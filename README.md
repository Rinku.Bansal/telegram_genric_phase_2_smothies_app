# Telegram_Genric_Phase_2_Smothies_App

[Telegram](https://telegram.org) is a messaging app with a focus on speed and security. It’s superfast, simple and free.
This repo contains the official source code for [Telegram App for iOS](https://apps.apple.com/app/telegram-messenger/id686449807).

## Creating your Telegram Application

We welcome all developers to use our API and source code to create applications on our platform.
There are several things we require from **all developers** for the moment.

1. [**Obtain your own api_id**](https://core.telegram.org/api/obtaining_api_id) for your application.
2. Please **do not** use the name Telegram for your app — or make sure your users understand that it is unofficial.
3. Kindly **do not** use our standard logo (white paper plane in a blue circle) as your app's logo.
3. Please study our [**security guidelines**](https://core.telegram.org/mtproto/security_guidelines) and take good care of your users' data and privacy.
4. Please remember to publish **your** code too in order to comply with the licences.

### API, Protocol documentation

Telegram API manuals: https://core.telegram.org/api

MTproto protocol manuals: https://core.telegram.org/mtproto

### Compilation Guide

1. Install the brew package manager, if you haven’t already.
2. Install the packages pkg-config, yasm:
brew install pkg-config yasm
3. Clone the project from GitHub:
git clone --recursive https://github.com/peter-iakovlev/Telegram-iOS.git
4. Open Telegram-iOS.workspace.
5. Open the Telegram-iOS-Fork scheme.
6. Start the compilation process.
7. To run the app on your device, you will need to set the correct values for the signature, .entitlements files and package IDs in accordance with your developer account values.


//--------------------------------Custom start here-------------------------------------------

// Repository 
Genric App: https://gitlab.com/Rinku.Bansal/chatbot_genric_telegram_ios.git
EZ Smoothie: https://gitlab.com/Rinku.Bansal/telegram_genric_phase_2_smothies_app.git

// generic app link
https://projects.invisionapp.com/share/8RHQGXTHWJQ#/screens/290923758

// EZ Smoothie version 1
https://projects.invisionapp.com/share/KBOY11FRNJ6#/screens/329480918

// EZ Smoothie version 2
https://projects.invisionapp.com/share/BRQK7AYF3VW#/screens/347343190

Amazon ads product (pending) url for EZ Smoothie
https://docs.google.com/spreadsheets/d/1vCk-HNSLHoAhcRe8tzxFo5chqKTOAjffGUWe5SUj5uA/edit#gid=0

// Postman Api Collection
https://www.getpostman.com/collections/b9bc97a2bc69dacb9d49

// Our Server Use URL
http://n4.iworklab.com:5000/

// credentials

1. Mix Panel
username: yaron.finkel@gmail.com
password: Finkel_mixpanel100

2. Apps flyer
username: yaron.finkel@gmail.com
password: Finkel_appsflyer100

3. Apple Developer account
username : amit.singh@valuecoders.com 
password : Amit@123!@#45

4. Telegram credentials:
client provided Telegram hash and token.

// code changes

1. Launch Screen: ( RMIntroViewController )
General api's call "fetching admin bots and admin channels details from server".

2. Login: ( TGLoginViewController )
After login and OTP verified, fetch admin and user details/ channels/ bots from server.

3. Dialog List (Home screen) ( TGDialogListViewController )
20 seconds repititive call for fetching user list and group list from server
fetching admin bots and channels, search them on basis of their username and add them to user.
Search User, Bot and Channel As it is clicked , add them to users unblocked list

4. Chatting Screen ( TGGenericModernConversationCompanion )
disabled app-app calling feature. 
As it is opened, add them to users unblocked list

5. ChatScreen Handle Keyboard ( TGModernConversationInputTextPanel )
Keyboard for bot changed here
disable bot keyboard opening button
if isAdmin bot, disable all bottom views as per comment

6. Notifications Controller ( TGInterfaceManager )
check every message object to compare with user's saved list of bots/channels/users to enable/disable notification.
check in checkId.

7. Locally saved Data ( TGSavedPrefrence ) 
contains list of chatbots/channels/userslist/savedchannel/savedbots.
check for telegram message in processUpdates to block it.

8. ActionBar
disable view background, back imageview and color.

9. TGColor
change bakcground color

10. TGArticleWebpageFooterModel
check if url is from our server to change name for instant view to full recipe

11. Create group ( TGCreateGroupController )
As soon as group is created, place a call to server to update group id with users list for group unblocking.

12. Profile ( TGEditProfileController )
disable all addViews
display user pic and name in updateUserData()
rest code is useless.

13. InviteContacts ( TGContactsController )
add custom add member button
Use link generator to generate Appsflyer invite friends link.
share url generated.

14. TGMixPanelEventsConstants
All Mix panel events here.

15. TGGenericModernConversationCompanion
check if url is from our server to open view screen otherwise default telegram screen open.

Rest activities are in activities package with usage specified in name
Rest fragment are in fragments package with usage specified in name


