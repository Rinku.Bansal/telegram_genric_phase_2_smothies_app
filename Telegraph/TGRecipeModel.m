//
//  TGRecipeModel.m
//  Telegraph
//
//  Created by vinove on 12/19/18.
//

#define imageUrl "http://app.ezsmoothie.recipes/ing/"//"http://beta.shakensmoothie.recipes/ing/"

#import "TGRecipeModel.h"

@implementation TGRecipeModel

+ (NSArray *)getIngredientsDetails:(NSArray *)arr {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < arr.count; i++) {
        TGRecipeModel *model = [[TGRecipeModel alloc] init];
        NSDictionary *dic = [arr objectAtIndex:i];
        model.name = [NSString stringWithFormat:@"%@", [dic objectForKey:@"nm"]];
        model.title = [NSString stringWithFormat:@"%@", [dic objectForKey:@"title"]];
        model.url = [self stringWithUrl:[dic objectForKey:@"nm"]];
        model.is_Selected = false;
        model.titleArr = [[NSMutableArray alloc] init];
        [arrToReturn addObject:model];
    }
    return (NSArray *)arrToReturn;
}

+ (NSString *)stringWithUrl:(NSString *)name {
    NSString *returnStr = [[NSString alloc] init];
    NSString* removeSpace = [[name stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
    returnStr = [NSString stringWithFormat:@"%s%@.png", imageUrl,  removeSpace];
    return returnStr;
}

+ (NSMutableArray *)getIngredientsDetailsScrolling:(NSArray *)arr withDic:(NSDictionary *)dic {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    TGRecipeModel *model1 = [[TGRecipeModel alloc] init];
    model1.name = @"";
    model1.title = @"";
    model1.url = [NSString stringWithFormat:@"%@", [dic objectForKey:@"img_recipe"]];
    
    [arrToReturn addObject:model1];
    
    for (NSUInteger i = 0; i < arr.count; i++) {
        TGRecipeModel *model = [[TGRecipeModel alloc] init];
        NSDictionary *dic = [arr objectAtIndex:i];
        model.name = [NSString stringWithFormat:@"%@", [dic objectForKey:@"nm"]];
        model.title = [NSString stringWithFormat:@"%@", [dic objectForKey:@"title"]];
        model.url = [self stringWithUrl:[dic objectForKey:@"nm"]];
        NSMutableArray *titleArray = [[NSMutableArray alloc] init];
        NSArray *titleStrArr = [[model.title lowercaseString] componentsSeparatedByString:[model.name lowercaseString]];
        BOOL isadd = false;
        for (NSUInteger j = 0; j < titleStrArr.count; j++) {
            if ([[NSString stringWithFormat:@"%@",[titleStrArr objectAtIndex:j]] isEqualToString:@""]) {
                [titleArray addObject:[model.name lowercaseString]];
                isadd = true;
            }else {
                [titleArray addObject:[NSString stringWithFormat:@"%@",[titleStrArr objectAtIndex:j]]];
            }
        }
        if (!isadd) {
            [titleArray insertObject:[model.name lowercaseString] atIndex:1];
        }
        model.titleArr = [[NSMutableArray alloc] init];
        model.titleArr = titleArray;
        [arrToReturn addObject:model];
    }
    TGRecipeModel *model2 = [[TGRecipeModel alloc] init];
    model2.name = @"";
    model2.title = @"";
    model2.url = @"ingredients_blender.png";
    [arrToReturn addObject:model2];

    return (NSArray *)arrToReturn;
}


@end
