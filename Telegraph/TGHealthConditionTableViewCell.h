//
//  TGHealthConditionTableViewCell.h
//  Telegraph
//
//  Created by vinove on 1/8/19.
//

#import <UIKit/UIKit.h>
@protocol SelectMultipleHealthConditionsProtocol <NSObject>
@optional

- (void)selectionClickedAtIndex:(NSInteger)section withIndex:(NSInteger)index;

@end

@interface TGHealthConditionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) NSInteger cellIndex;
@property (nonatomic, assign) id <SelectMultipleHealthConditionsProtocol> selectDelegate;
@end
