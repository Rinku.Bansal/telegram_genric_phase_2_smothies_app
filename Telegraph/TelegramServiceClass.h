//
//  TelegramServiceClass.h
//  Telegraph
//
//  Created by vinove on 19/07/18.
//

#import <Foundation/Foundation.h>

@interface TelegramServiceClass : NSObject <NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate>

+ (TelegramServiceClass *)shareInstance;

// ******************************************* Genric API *******************************************

- (void)getAppIdFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)AddNewUserOwnServer:(NSString *)firstName last:(NSString *)lastName phoneNo:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getAllTextThemeFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getAllChannelsDetailsFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getAllChatBotsDetailsFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getUserStatusBlocked:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getUserChannelsFromServer:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getUserBotsFromServer:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)addUserBotsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName 							:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)addUserChannelsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)removeUserChannelsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)removeUserBotsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getUserListSaveFromServer:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)addUserFromServer:(NSString *)userName witTelegramId:(NSString *)telegramUserId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)removeUserFromOwnServer:(NSString *)userName witTelegramId:(NSString *)telegramUserId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)addGroupFromServer:(NSString *)groupId withUsersArray:(NSArray *)userArr :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getGroupDetailsFromServer:(NSString *)userId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

// ******************************************* Smoothies API *******************************************

- (void)getFullRecipeDetailsFromServer:(NSString *)recipeId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)getIngradiantsDetailsFromServer:(NSString *)ingradiantName :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (NSString *)NutritionFactImageDownloadWithUrl:(NSString *)recipeId;

- (void)getAllGoalsPrefrencesIngredientsHealthConditionInformationFromServerphnNumber:(NSString *)phnNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:(NSString *)userData type:(NSString *)typeData like:(NSString *)likeStr dislike:(NSString *)dislike phnNumber:(NSString *)phnNumber with:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)saveUserAllPrefrencestoTheSmoothiesServerchatUserId:(NSString *)chatId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;

- (void)saveContactUsInformationFromServer:(NSString *)userName name:(NSString *)name emailId:(NSString *)emailId message:(NSString *)message with:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block;


@end
