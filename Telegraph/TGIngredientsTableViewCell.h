//
//  TGIngredientsTableViewCell.h
//  Telegraph
//
//  Created by vinove on 1/9/19.
//

#import <UIKit/UIKit.h>
#import "StepSlider.h"

@protocol changeMultipleIngredientsStateProtocol <NSObject>
@optional

- (void)selectionClickedAtIndex:(NSInteger)index stepperIndex:(NSInteger)stepperIndex;
- (void)likeSelectionAtIndex:(NSInteger)index;
- (void)dislikeSelectionAtIndex:(NSInteger)index;

@end

@interface TGIngredientsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ingredientsImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblIngredientsName;
@property (strong, nonatomic) IBOutlet StepSlider *stepperSlider;
@property (nonatomic, assign) NSInteger cellIndex;
@property (nonatomic, assign) id <changeMultipleIngredientsStateProtocol> selectDelegate;
@property (weak, nonatomic) IBOutlet UIView *likeDislikeView;
@property (weak, nonatomic) IBOutlet UIButton *btnDislike;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;

@end
