//
//  TGIngredientsDetailsViewController.h
//  Telegraph
//
//  Created by vinove on 1/10/19.
//

#import <UIKit/UIKit.h>

@interface TGIngredientsDetailsViewController : UIViewController
@property (nonatomic, strong) NSString *ingredientsName;
@end
