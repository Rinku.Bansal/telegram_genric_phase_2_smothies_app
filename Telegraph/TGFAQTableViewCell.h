//
//  TGFAQTableViewCell.h
//  Telegraph
//
//  Created by Rinku on 28/01/19.
//

#import <UIKit/UIKit.h>

@interface TGFAQTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblAnswer;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@end
