//
//  TGSavedPrefrence.h
//  Telegraph
//
//  Created by vinove on 12/12/18.
//

#import <Foundation/Foundation.h>

@interface TGSavedPrefrence : NSObject

// Save Admin channels and Admin bots getting from our server
+ (void)saveAdminChannels:(NSArray *)arr;
+ (void)saveAdminBots:(NSArray *)arr;

// Save User Channels and User Bots when user Add
+ (void)saveUserChannels:(NSMutableArray *)arr;
+ (void)saveUserBots:(NSMutableArray *)arr;

// Save User List
+ (void)saveUserList:(NSMutableArray *)arr;

// Telegram user information
+ (void)saveFirstName:(NSString *)firstName;
+ (void)saveLastName:(NSString *)lastName;
+ (void)saveMobile:(NSString *)mobileNumber;

// Save all application Text from
+ (void)saveAllApplicationText:(NSDictionary *)dic;

// Save is Genric Bot or not
+ (void)saveIsGenricBot:(bool)isGenricBot;

// add channels and bots add first not change screen
+ (void)saveControllerSelectedType:(bool)selectedType;

// User search channels and add channels and save channel info
+ (void)saveSearchAddChannelsId:(NSString *)channelId;
+ (void)saveSearchAddChannelsName:(NSString *)channelName;

// User search Bots and save bot info
+ (void)saveSearchAddBotId:(NSString *)botId;
+ (void)saveSearchAddBotName:(NSString *)botName;

// User delete
+ (void)saveDeleteUser:(NSString *)userId;

// User delete channel info save
+ (void)saveDeleteChannelId:(NSString *)channelId;
+ (void)saveDeleteChannelName:(NSString *)channelName;

// User delete Bot info save
+ (void)saveDeleteBotId:(NSString *)botId;
+ (void)saveDeleteBotName:(NSString *)botName;

// save Genric app id getting from our server
+ (void)saveGenricAppId:(NSString *)appId;

// save telegram uid from telegram
+ (void)saveTelegramUIDInfo:(int)telegramUid;

// Save user block status
+ (void)saveUserBlockedStatus:(bool)blockStatus;

// Save Chatting User, Channel, Bot Name
+ (void)saveChatUserName:(NSString *)chatUser;

// Save Days install count
+ (void)saveDaysInstallCount:(int)count;

// Save Daily App launch count
+ (void)saveDailyAppLaunchCount:(int)count;

// Save Current Date
+ (void)saveCurrentDate:(NSString *)date;

// Save Referral Name
+ (void)saveRefferalNameGettingAppsflyer:(NSString *)name;

// Save selected goals before login
+ (void)saveSelectedGoalsName:(NSString *)goalsStr;

// Save selected prefrences before login
+ (void)saveSelectedPrefrencesName:(NSString *)prefrenceStr;

// Save Seleted Ingredients List
+ (void)saveSelectedIngredientsList:(NSString *)ingredientsLikeStr dislike:(NSString *)ingredientsDislikeStr;

// Save selected Health Condition
+ (void)saveSeletedHealthCondition:(NSString *)healthStr;

// Save Favorate Recipe Id Saves
+ (void)saveFavorateRecipesIdFromArray:(NSMutableArray *)favorateArr;

// Save Channel Share Recipe Id
+ (void)saveRecipeIdShareToChannelFromArray:(NSMutableArray *)channelRecipeIdArr;

// Save Unread count
+ (void)saveUnreadCountOnlyShowCells:(int)unreadCount;

// Save User Create Favorate Smoothies Channel
+ (void)saveUserCreateFavorateSmoothiesChannel:(NSString *)channelId;

// Save User Create User Name Channel
+ (void)saveUserCreateUserNameChannel:(NSString *)channelId;

// Save User First Time Login Call ViewWillAppear Method On dialogList
+ (void)saveLoginFirstTime:(bool)isFirstTime;

// Save Smoothies Changes Like ingredients, health Condition, Goals, Prefrences
+ (void)saveChangeInSmoothiesBot:(bool)isChange;

// Save After login Hit Api Only First Time
+ (void)saveAfterLoginFirstTimeHitAllApi:(bool)isFirst;

// Save Campaign name in Appsflyer
+ (void)saveCampaignName:(NSString *)name;

// Save Group List
+ (void)saveGroupIdList:(NSArray *)arr;

@end
