//
//  TGIngredientsModel.m
//  Telegraph
//
//  Created by vinove on 1/9/19.
//
#define imageUrl "http://app.ezsmoothie.recipes/ing/"//"http://beta.shakensmoothie.recipes/ing/"

#import "TGIngredientsModel.h"

@implementation TGIngredientsModel

+ (NSMutableArray *)getIngredientsDetails {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    NSArray *ingredientsArr = [self ingredientsList];
    for (NSUInteger i = 0; i < ingredientsArr.count; i++) {
        TGIngredientsModel *model = [[TGIngredientsModel alloc] init];
        model.name = [NSString stringWithFormat:@"%@", [ingredientsArr objectAtIndex:i]];
        model.state = 1;
        model.url = [self stringWithUrl:[NSString stringWithFormat:@"%@", [ingredientsArr objectAtIndex:i]]];
        [arrToReturn addObject:model];
    }
    return arrToReturn;
}

+ (NSArray *)ingredientsList {
    NSArray *dataArr = [[NSArray alloc] init];
    dataArr = @[@"Water", @"Ice", @"Almond milk", @"Milk", @"Protein Powder", @"Mango", @"Banana", @"Apple", @"Ginger", @"Melon", @"Watermelon", @"Blueberries", @"Blackberries", @"Strawberries", @"Goji Berries", @"Chia", @"Flaxseeds", @"Almonds", @"Cashew", @"Kale", @"Spinach", @"Cucumber", @"Carrot", @"Beetroot", @"Yogurt", @"Cacao", @"Oats", @"Honey", @"Coconut Milk", @"Coconut Water", @"Green tea", @"Rice milk", @"Spirulina Powder", @"Avocado", @"Acai Powder", @"Chamomile Tea", @"Pecan", @"Walnuts", @"Pineapple", @"Kiwi", @"Cinnamon Powder", @"Matcha Powder", @"Nectarine", @"Peach", @"Sweet Potato", @"Dates", @"Orange", @"Grapefruit", @"Grapes", @"Pear", @"Tomatoes", @"Broccoli", @"Celery", @"Raspberry", @"Pomegranate", @"Passion fruit", @"Dragonfruit", @"Carambola", @"Squash", @"Zucchini", @"Cauliflower", @"Green Peas", @"Wheatgrass", @"Soy Beans"];
    return dataArr;
}

+ (NSString *)stringWithUrl:(NSString *)name {
    NSString *returnStr = [[NSString alloc] init];
    NSString* removeSpace = [[name stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
    returnStr = [NSString stringWithFormat:@"%s%@.png", imageUrl,  removeSpace];
    return returnStr;
}

@end

