//
//  TGSmoothiesGoalsCollectionViewCell.m
//  Telegraph
//
//  Created by vinove on 12/17/18.
//

#import "TGSmoothiesGoalsCollectionViewCell.h"

@implementation TGSmoothiesGoalsCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil)
    {
        _btnGoals = [[UIButton alloc] initWithFrame:CGRectMake(40.0f, 10.0f, 80.0f, 80.0f)];
        _btnGoals.layer.cornerRadius = _btnGoals.frame.size.width/2;
        _btnGoals.layer.masksToBounds = true;
        [_btnGoals addTarget:self action:@selector(btnSelectClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnGoals];

        _lblGoals = [[UILabel alloc] initWithFrame:CGRectMake(0.0, _btnGoals.frame.origin.y+ _btnGoals.frame.size.height + 10, self.frame.size.width, 30)];
        _lblGoals.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0f];
        _lblGoals.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_lblGoals];

    }
    return self;
}

- (void)btnSelectClick {
    if(_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(selectionClickedAtIndex:)])
        [_selectDelegate selectionClickedAtIndex:_cellIndex];
}

@end
