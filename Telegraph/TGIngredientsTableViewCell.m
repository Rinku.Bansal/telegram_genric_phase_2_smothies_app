//
//  TGIngredientsTableViewCell.m
//  Telegraph
//
//  Created by vinove on 1/9/19.
//

#import "TGIngredientsTableViewCell.h"

@implementation TGIngredientsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _stepperSlider.sliderCircleImage = [UIImage imageNamed:@"ingredient_handle.png"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)stepperSliderClick:(StepSlider *)sender {
    NSLog(@"Stepper index ====>>>%lu",(unsigned long)sender.index);
    if(_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(selectionClickedAtIndex:stepperIndex:)])
        [_selectDelegate selectionClickedAtIndex:_cellIndex stepperIndex:(unsigned long)sender.index];
}

- (IBAction)btnDislike:(id)sender {
    if (_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(dislikeSelectionAtIndex:)]) {
        [_selectDelegate dislikeSelectionAtIndex:_cellIndex];
    }
}

- (IBAction)btnLike:(id)sender {
    if (_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(likeSelectionAtIndex:)]) {
        [_selectDelegate likeSelectionAtIndex:_cellIndex];
    }
}

@end
