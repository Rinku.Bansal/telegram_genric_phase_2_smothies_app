//
//  TGHealthConditionModel.m
//  Telegraph
//
//  Created by vinove on 1/8/19.
//

#import "TGHealthConditionModel.h"

@implementation TGHealthConditionModel

+ (NSArray *)healthConditionArray {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    NSArray *charArr = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"O", @"P", @"R", @"S", @"T", @"U"];
    for (NSUInteger i = 0; i < charArr.count; i++) {
        TGHealthConditionModel *model = [[TGHealthConditionModel alloc] init];
        model.name = [NSString stringWithFormat:@"%@",[charArr objectAtIndex:i]];
        model.healthArr = [self healthArrWith:[NSString stringWithFormat:@"%@",[charArr objectAtIndex:i]]];
        [arrToReturn addObject:model];
    }
    
    return (NSArray *)arrToReturn;
}

+ (NSArray *)healthArrWith:(NSString *)name {
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    NSMutableArray *dicArr = [[self healthConditionDic] objectForKey:name];
    for (NSUInteger i = 0; i<dicArr.count; i++) {
        TGHealthModel *model = [[TGHealthModel alloc] init];
        model.isSelected = false;
        model.name = [NSString stringWithFormat:@"%@",[dicArr objectAtIndex:i]];
        [dataArr addObject:model];
    }
    return (NSArray *)dataArr;
}

+ (NSDictionary *)healthConditionDic {
    NSDictionary *dataDic = @{@"A" : @[@"Acne", @"Alcoholism", @"Allergies", @"Anemia", @"Arthritis", @"Asthma", @"Atherosclerosis"],
                              @"B" : @[@"Blood clots", @"Blood sugar disorders", @"Breast cancer", @"Bronchitis", @"Burns"],
                              @"C" : @[@"Cancer", @"Cardiovascular disease", @"Colitis", @"Colon cancer", @"Constipation", @"Coughs", @"Cystitis"],
                              @"D" : @[@"Dermatitis", @"Diabetes", @"Diarrhea", @"Digestive disorders", @"Dry cough", @"Dyspepsia"],
                              @"E" : @[@"Eczema", @"Edema", @"Excessive thirst"],
                              @"F" : @[@"Fever", @"Fluid retention", @"Free radicals"],
                              @"G" : @[@"Gallstones", @"Gout"],
                              @"H" : @[@"Heart disease", @"High blood pressure", @"High cholesterol", @"Hypertension"],
                              @"I" : @[@"IBS", @"Immune system deficiency", @"Impotence", @"Indigestion", @"Inflammation", @"Insomnia"],
                              @"J" : @[@"Jaundice"],
                              @"K" : @[@"Kidney stones"],
                              @"L" : @[@"Liver disease", @"Lung cancer"],
                              @"M" : @[@"Macular degeneration", @"Mental depression", @"Mental fatigue"],
                              @"O" : @[@"Osteoporosis"],
                              @"P" : @[@"Pancreatic cancer", @"Peptic ulcers"],
                              @"R" : @[@"Rheumatoid arthritis"],
                              @"S" : @[@"Skin disorders", @"Sore throat", @"Stomach cancer", @"Stomach ulcers"],
                              @"T" : @[@"Type 2 diabetes"],
                              @"U" : @[@"Urinary tract infections"]};
    return dataDic;
}

@end


@implementation TGHealthModel

@end

