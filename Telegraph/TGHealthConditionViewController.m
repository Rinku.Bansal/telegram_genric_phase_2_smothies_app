//
//  TGHealthConditionViewController.m
//  Telegraph
//
//  Created by vinove on 1/8/19.
//

#import "TGHealthConditionViewController.h"
#import "TGHealthConditionTableViewCell.h"
#import "TGHealthConditionModel.h"
#import "TelegramServiceClass.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGSavedPrefrence.h"
#import "TGUser+Telegraph.h"
#import "TGDatabase.h"
#import "TGMixPanelEventsConstants.h"

@interface TGHealthConditionViewController ()<UITableViewDelegate, UITableViewDataSource, SelectMultipleHealthConditionsProtocol, UISearchBarDelegate> {
    
    __weak IBOutlet UILabel *lblRecommendation;
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UITableView *healthConditionTableView;
    UIView *containerView;
    NSMutableArray *healthConditionArray;
    NSMutableArray *searchArray;
}

@end

@implementation TGHealthConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGHealthConditionViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    
    self.titleText = TGLocalized(@"Health_Condition_Header_Title");//@"Smoothies Nutrition";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    [TGMixPanelEventsConstants trackMixpanelEvents:HEALTH_CONDITION properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:HEALTH_CONDITION]];


    lblRecommendation.text = TGLocalized(@"Health_Condition_Not_Medical_Recommendation");
    healthConditionArray = [[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    healthConditionArray = (NSMutableArray *)[TGHealthConditionModel healthConditionArray];
    searchArray = (NSMutableArray *)[TGHealthConditionModel healthConditionArray];
    [self displaySaveHealthCondition];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)displaySaveHealthCondition {
    NSArray *healthArr = [TGFetchSavedPrefrenceInfoGenric getSelectedHealthConditionList];
    for (NSUInteger i = 0; i < healthArr.count; i++) {
        NSString *dataStr = [NSString stringWithFormat:@"%@", [healthArr objectAtIndex:i]];
        for (NSUInteger j = 0; j<healthConditionArray.count; j++) {
            TGHealthConditionModel *model = [healthConditionArray objectAtIndex:j];
            for (NSUInteger k = 0; k < model.healthArr.count;k++) {
                TGHealthModel *healthModel = [model.healthArr objectAtIndex:k];
                if ([healthModel.name isEqualToString:dataStr]) {
                    healthModel.isSelected = true;
                }
            }
 
        }
    }
    [healthConditionTableView reloadData];
}

- (void)ButtonBackClicked {
    NSMutableArray *dataArr = [[NSMutableArray alloc] init];
    for (NSUInteger j = 0; j<healthConditionArray.count; j++) {
        TGHealthConditionModel *model = [healthConditionArray objectAtIndex:j];
        for (NSUInteger k = 0; k < model.healthArr.count;k++) {
            TGHealthModel *healthModel = [model.healthArr objectAtIndex:k];
            if (healthModel.isSelected) {
                [dataArr addObject:healthModel.name];
            }
        }
        
    }
    NSString *seprateStr = [dataArr componentsJoinedByString:@","];
    [TGSavedPrefrence saveSeletedHealthCondition:seprateStr];
    [TGMixPanelEventsConstants trackMixpanelEvents:HEALTH_CONDITION properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:HEALTH_CONDITION]];
    [self.navigationController popViewControllerAnimated:true];
    [[TelegramServiceClass shareInstance] saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:seprateStr type:@"health_condition" like:@"" dislike:@"" phnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] with:^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"data dic is ====>>>%@",dic);
        }else {
            NSLog(@"error is ====>>>%@",error);
        }
    }];
    
   // [self saveAllPrefrencesListFromSmoothiesServer];
}

- (void)saveAllPrefrencesListFromSmoothiesServer {
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    [[TelegramServiceClass shareInstance] saveUserAllPrefrencestoTheSmoothiesServerchatUserId:chatUserId :^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"prefrences dic is ====>>>>>%@", dic);
        }else {
            NSLog(@"error is ====>>>>%@",error);
        }
    }];
}

// UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [healthConditionArray count];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    TGHealthConditionModel *model = [healthConditionArray objectAtIndex:index];
    
    return [model.healthArr indexOfObject:title];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    TGHealthConditionModel *model = [healthConditionArray objectAtIndex:section];
    return model.name;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    TGHealthConditionModel *model = [healthConditionArray objectAtIndex:section];
    return [model.healthArr count];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = TGThemeColor();
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TGHealthConditionTableViewCell *cell = (TGHealthConditionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGHealthConditionTableViewCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGHealthConditionTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    TGHealthConditionModel *model = [healthConditionArray objectAtIndex:indexPath.section];
    TGHealthModel *healthModel = [model.healthArr objectAtIndex:indexPath.row];
    cell.lblName.text = healthModel.name;
    if (healthModel.isSelected) {
        [cell.btnCheck setImage:[UIImage imageNamed:@"checkHealthCondition.png"] forState:UIControlStateNormal];
    }else {
        [cell.btnCheck setImage:[UIImage imageNamed:@"uncheckHealthCondition.png"] forState:UIControlStateNormal];
    }
    cell.section = indexPath.section;
    cell.cellIndex = indexPath.row;
    cell.selectDelegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

// Cell delegate method
- (void)selectionClickedAtIndex:(NSInteger)section withIndex:(NSInteger)index {
    [TGSavedPrefrence saveChangeInSmoothiesBot:YES];
    TGHealthConditionModel *model = [healthConditionArray objectAtIndex:section];
    TGHealthModel *healthModel = [model.healthArr objectAtIndex:index];
    if (healthModel.isSelected) {
        healthModel.isSelected = false;
    }else {
        healthModel.isSelected = true;
    }
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:section];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexpath, nil];
    [healthConditionTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}

#pragma Mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] == 0) {
        healthConditionArray = [[NSMutableArray alloc]init];
        healthConditionArray = [searchArray mutableCopy];
        [healthConditionTableView reloadData];
        [searchBar resignFirstResponder];
        return;
    }
    healthConditionArray = [[NSMutableArray alloc]init];
    for (NSUInteger i=0; i<searchArray.count; i++) {
        TGHealthConditionModel *model = [searchArray objectAtIndex:i];
        for (NSUInteger j = 0; j < model.healthArr.count; j++) {
            TGHealthModel *healthModel = [model.healthArr objectAtIndex:j];
            NSComparisonResult result = [healthModel.name compare:searchText options:NSCaseInsensitiveSearch range:NSMakeRange(0, searchText.length) locale:nil];
            if (result == NSOrderedSame) {
                [healthConditionArray addObject:model];
            }
        }
 
    }
    [healthConditionTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

@end
