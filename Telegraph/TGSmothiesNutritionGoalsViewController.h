//
//  TGSmothiesNutritionGoalsViewController.h
//  Telegraph
//
//  Created by vinove on 12/17/18.
//

#import <UIKit/UIKit.h>
#import <LegacyComponents/TGViewController.h>

typedef enum {
    InchS35 = 0,
    InchS4 = 1,
    InchS47 = 2,
    InchS55 = 3,
    iPadS = 4,
    iPadProS = 5
} DeviceScreenSize;

@interface TGSmothiesNutritionGoalsViewController : TGViewController
{
    UIButton *_startButton;

}
@property (nonatomic, assign) BOOL is_Login;

@end
