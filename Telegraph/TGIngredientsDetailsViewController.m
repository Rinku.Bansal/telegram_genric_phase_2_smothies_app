//
//  TGIngredientsDetailsViewController.m
//  Telegraph
//
//  Created by vinove on 1/10/19.
//
#define imageUrl "http://app.ezsmoothie.recipes/ing/"//"http://beta.shakensmoothie.recipes/ing/"
#define nutritionFactsImageUrl "http://app.ezsmoothie.recipes/image_iv6.php?ing="//"http://beta.shakensmoothie.recipes/image_iv6.php?ing="

#import "TGIngredientsDetailsViewController.h"
#import "TelegramServiceClass.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "TGRecipiesCollectionViewCell.h"
#import <LegacyComponents/TGProgressWindow.h>
#import "TGRecipeNutritionFactsZoomImageView.h"
#import "UIImageView+WebCache.h"
#import "TGIngredientsDiscriptionTableViewCell.h"
#import "TGMixPanelEventsConstants.h"

@interface TGIngredientsDetailsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegateLeftAlignedLayout>{
    
    __weak IBOutlet NSLayoutConstraint *ingredientsTableViewHeightConstant;
    __weak IBOutlet NSLayoutConstraint *bodySystemHeightConstant;
    __weak IBOutlet NSLayoutConstraint *nutritionProgramHeightConstant;
    __weak IBOutlet NSLayoutConstraint *bodySystemLabelConstant;
    __weak IBOutlet NSLayoutConstraint *nutritionProgramLabelConstant;
    __weak IBOutlet NSLayoutConstraint *nutritionFactsTopConstants;
    __weak IBOutlet NSLayoutConstraint *moreLessHeightConstant;
    __weak IBOutlet NSLayoutConstraint *downImageHeightConstant;
    
    __weak IBOutlet NSLayoutConstraint *moreLessTopConstant;
    __weak IBOutlet UIView *topViw;
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UIImageView *ingredientsImageView;
    __weak IBOutlet UITableView *ingredientsDiscritionTableView;
    __weak IBOutlet UIButton *btnMore;
    __weak IBOutlet UICollectionView *bodySystemCollectionView;
    __weak IBOutlet UICollectionView *nutritionProgramCollectionView;
    __weak IBOutlet UIImageView *nutritionFactsImage;
    __weak IBOutlet UILabel *lblMoreLess;
    __weak IBOutlet UIImageView *downUpImageView;
    __weak IBOutlet UICollectionViewLeftAlignedLayout *bodySystemLayout;
    __weak IBOutlet UICollectionViewLeftAlignedLayout *nutritionProgramLayout;
    TGRecipeNutritionFactsZoomImageView *zoomImageView;

    UIView *containerView;
    NSArray *bodySystemArr;
    NSArray *nutritionProgramArr;
    NSArray *discriptionArr;
    BOOL isMore;
}
@property (nonatomic, strong) TGProgressWindow *progressWindow;

@end

@implementation TGIngredientsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isMore = false;
    lblMoreLess.text = TGLocalized(@"Ingredients_Detail_More");
    self.navigationController.navigationBarHidden = true;
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGIngredientsDetailsViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    [TGMixPanelEventsConstants trackMixpanelEvents:INGREDEINTS_DETAILS properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:INGREDEINTS_DETAILS]];

    [self initialiseTheView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)initialiseTheView {
    
    NSString* removeSpace = [[_ingredientsName stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
    NSString *returnStr = [NSString stringWithFormat:@"%s%@.png", imageUrl,  removeSpace];
    [ingredientsImageView sd_setImageWithURL:[NSURL URLWithString:returnStr] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSString *nutritionFactsImageStr = [NSString stringWithFormat:@"%s%@",nutritionFactsImageUrl,_ingredientsName];
    NSString *urlString = [nutritionFactsImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [nutritionFactsImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    lblHeader.text = [NSString stringWithFormat:@"%@", _ingredientsName];
    _progressWindow = [[TGProgressWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [bodySystemCollectionView registerNib:[UINib nibWithNibName:@"TGRecipiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell"];
    
    [nutritionProgramCollectionView registerNib:[UINib nibWithNibName:@"TGRecipiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell"];
    [self getIngredientsDetailsFromServer];
}

- (void)getIngredientsDetailsFromServer {
    bodySystemArr = [[NSArray alloc] init];
    nutritionProgramArr = [[NSArray alloc] init];
    discriptionArr = [[NSArray alloc] init];
    [_progressWindow show:true];
    [[TelegramServiceClass shareInstance] getIngradiantsDetailsFromServer:_ingredientsName :^(BOOL success, NSError *error, NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_progressWindow dismiss:true];
            if (success) {
                NSDictionary *dataDic = [dic objectForKey:@"ingredient"];
                NSArray *disArr = [dataDic objectForKey:@"did_you_know"];
                NSString *dataStr = [NSString stringWithFormat:@"%@", disArr];
                bodySystemArr = [dataDic objectForKey:@"body_sys"];
                nutritionProgramArr = [dataDic objectForKey:@"health_cond"];
                if (![dataStr isEqualToString:@"<null>"] && ![dataStr isEqualToString:@"(null)"]) {
                    if (disArr.count > 0) {
                        discriptionArr = [dataDic objectForKey:@"did_you_know"];
                        [ingredientsDiscritionTableView reloadData];
                    }else {
                        discriptionArr = [[NSArray alloc] init];
                    }
                }else {
                    discriptionArr = [[NSArray alloc] init];
                }
                [self setEstimatedSizeIfNeeded];
                [bodySystemCollectionView reloadData];
                [nutritionProgramCollectionView reloadData];
                [self setCollectionViewHeight];
                NSLog(@"ingradiant dic is =====<>>>>>%@", dic);
            }else {
                NSLog(@"error is ===>>>%@", error);
            }
        });
    }] ;
}

- (void)setEstimatedSizeIfNeeded {
    bodySystemLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    nutritionProgramLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
}

- (void)setCollectionViewHeight {
    [self.view layoutIfNeeded];
    if (discriptionArr.count <= 1) {
        moreLessHeightConstant.constant = 0;
        moreLessTopConstant.constant = 0;
        downImageHeightConstant.constant = 0;
    }
    
    CGFloat bodySystemHeight = bodySystemCollectionView.collectionViewLayout.collectionViewContentSize.height;
    CGFloat nutritionProgramHeight = nutritionProgramCollectionView.collectionViewLayout.collectionViewContentSize.height;
    
    if (bodySystemArr.count > 0) {
        if (bodySystemHeight == 30) {
            bodySystemHeightConstant.constant = 30;
        }else {
            bodySystemHeightConstant.constant = bodySystemHeight;
        }
    }else {
        bodySystemHeightConstant.constant = 0;
        bodySystemLabelConstant.constant = 0;
    }
    
    if (nutritionProgramArr.count > 0) {
        if (nutritionProgramHeight == 30) {
            nutritionProgramHeightConstant.constant = 30;
        }else {
            nutritionProgramHeightConstant.constant = nutritionProgramHeight;
            
        }   
    }else {
        nutritionProgramHeightConstant.constant = 0;
        nutritionProgramLabelConstant.constant = 0;
    }
    
    if (discriptionArr.count > 0 ) {
        ingredientsTableViewHeightConstant.constant = 90;
    }else {
        ingredientsTableViewHeightConstant.constant = 0;
    }
    
    if (bodySystemArr.count == 0 && nutritionProgramArr.count == 0) {
        nutritionFactsTopConstants.constant = 0;
    }
}

- (void)openZoomImageView:(NSString *)url {
    int imageConstant = 0;
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        imageConstant = 50;
    }else {
        imageConstant = 23;
    }
    zoomImageView = [[[NSBundle mainBundle] loadNibNamed:@"TGRecipeNutritionFactsZoomImageView" owner:self options:nil] objectAtIndex:0];
    zoomImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    zoomImageView.nutritionFactsImageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth );
    [zoomImageView.nutritionFactsImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    zoomImageView.buttonTopConstraints.constant = imageConstant;
    zoomImageView.scrollerView.contentSize = CGSizeMake(zoomImageView.nutritionFactsImageView.frame.size.width, zoomImageView.nutritionFactsImageView.frame.size.height);
    zoomImageView.scrollerView.clipsToBounds = YES;
    zoomImageView.scrollerView.delegate = self;
    zoomImageView.doubleTap.delegate = self;
    zoomImageView.nutritionFactsImageView.userInteractionEnabled = YES;
    
    zoomImageView.scrollerView.minimumZoomScale = 1.0;
    zoomImageView.scrollerView.maximumZoomScale = 5.0;
    
    // add gesture recognizers to the image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:singleTap];
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:doubleTap];
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:twoFingerTap];
    [self.view addSubview:zoomImageView];
}

- (IBAction)btnClose:(id)sender {
    [TGMixPanelEventsConstants trackMixpanelEvents:INGREDEINTS_DETAILS properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:INGREDEINTS_DETAILS]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAmezon:(id)sender {
    
}

- (IBAction)btnMore:(id)sender {
    if (!isMore) {
        isMore = true;
        if (discriptionArr.count > 0 ) {
            ingredientsTableViewHeightConstant.constant = 90*discriptionArr.count;
        }else {
            ingredientsTableViewHeightConstant.constant = 0;
        }
        lblMoreLess.text = TGLocalized(@"Ingredients_Detail_Less");
        downUpImageView.image = [UIImage imageNamed:@"upArrow.png"];
        [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:INGREDEINTS_DETAILS back:MORE] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:INGREDEINTS_DETAILS]];
    }else {
        isMore = false;
        if (discriptionArr.count > 0 ) {
            ingredientsTableViewHeightConstant.constant = 90;
        }else {
            ingredientsTableViewHeightConstant.constant = 0;
        }
        lblMoreLess.text = TGLocalized(@"Ingredients_Detail_More");
        downUpImageView.image = [UIImage imageNamed:@"downarrow.png"];
        [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:INGREDEINTS_DETAILS back:LESS] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:INGREDEINTS_DETAILS]];
    }
    [ingredientsDiscritionTableView reloadData];
}

- (IBAction)btnIngredientsImageClick:(id)sender {
    NSString* removeSpace = [[_ingredientsName stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
    NSString *returnStr = [NSString stringWithFormat:@"%s%@.png", imageUrl,  removeSpace];
    [self openZoomImageView:returnStr];
}


- (IBAction)nutritionFactImageClick:(id)sender {
    NSString *nutritionFactsImageStr = [NSString stringWithFormat:@"%s%@",nutritionFactsImageUrl,_ingredientsName];
    NSString *urlString = [nutritionFactsImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [self openZoomImageView:urlString];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return zoomImageView.nutritionFactsImageView;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}
#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // zoom in
    float newScale = [zoomImageView.scrollerView zoomScale] * 1.5;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [zoomImageView.scrollerView zoomToRect:zoomRect animated:YES];
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [zoomImageView.scrollerView zoomScale] / 1.5;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [zoomImageView.scrollerView zoomToRect:zoomRect animated:YES];
}

#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [zoomImageView.scrollerView frame].size.height / scale;
    zoomRect.size.width  = [zoomImageView.scrollerView frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}


#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (bool)collectionView:(UICollectionView *)__unused collectionView canMoveItemAtIndexPath:(NSIndexPath *)__unused indexPath {
    return false;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == bodySystemCollectionView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    if (collectionView == nutritionProgramCollectionView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == bodySystemCollectionView) {
        CGFloat randomWidth = (arc4random() % 120) + 30;
        return CGSizeMake(randomWidth+16, 30);
    }
    if (collectionView == nutritionProgramCollectionView) {
        CGFloat randomWidth = (arc4random() % 120) + 30;
        return CGSizeMake(randomWidth+16, 30);
    }
    return CGSizeZero;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == bodySystemCollectionView) {
        return bodySystemArr.count;
    }else {
        return nutritionProgramArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TGRecipiesCollectionViewCell *cell = (TGRecipiesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell" forIndexPath:indexPath];
    if (collectionView == bodySystemCollectionView) {
        cell.lblText.text = [NSString stringWithFormat:@"%@",[bodySystemArr objectAtIndex:indexPath.row]];
        cell.bgView.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:97.0/255.0 blue:156.0/255.0 alpha:1];
        cell.bgView.layer.cornerRadius = 14.0;
        cell.bgView.layer.masksToBounds = true;
        return cell;
    }else {
        cell.lblText.text = [NSString stringWithFormat:@"%@",[nutritionProgramArr objectAtIndex:indexPath.row]];
        cell.bgView.backgroundColor = [UIColor colorWithRed:19.0/255.0 green:54.0/255.0 blue:86.0/255.0 alpha:1];
        cell.bgView.layer.cornerRadius = 14.0;
        cell.bgView.layer.masksToBounds = true;
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return discriptionArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TGIngredientsDiscriptionTableViewCell *cell = (TGIngredientsDiscriptionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGIngredientsDiscriptionTableViewCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGIngredientsDiscriptionTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblDiscription.text = [NSString stringWithFormat:@"%@",[discriptionArr objectAtIndex:indexPath.row]];
    cell.backgroundColor = TGThemeColor();
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

@end
