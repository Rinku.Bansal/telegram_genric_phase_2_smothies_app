//
//  TGRecipeModel.h
//  Telegraph
//
//  Created by vinove on 12/19/18.
//

#import <Foundation/Foundation.h>

@interface TGRecipeModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) BOOL is_Selected;
@property (nonatomic, strong) NSMutableArray *titleArr;

+ (NSArray *)getIngredientsDetails:(NSArray *)arr;
+ (NSMutableArray *)getIngredientsDetailsScrolling:(NSArray *)arr withDic:(NSDictionary *)dic;
@end
