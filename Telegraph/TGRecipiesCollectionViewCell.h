//
//  TGRecipiesCollectionViewCell.h
//  Telegraph
//
//  Created by vinove on 12/27/18.
//

#import <UIKit/UIKit.h>

@interface TGRecipiesCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
