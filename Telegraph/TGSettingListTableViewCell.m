//
//  TGSettingListTableViewCell.m
//  Telegraph
//
//  Created by vinove on 13/08/18.
//

#import "TGSettingListTableViewCell.h"

@implementation TGSettingListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    int max = (int)[[UIScreen mainScreen] bounds].size.width;
    CGFloat width = 0.0;
    if (max == 320) {
        width = 320;
    }else if (max == 375) {
        width = 375;
    }else {
        width = 414;
    }
    
    self.contentView.frame = CGRectMake(0.0, 0.0, width, 50);
    _contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 40, 30)];
   // _contentImageView.backgroundColor = [UIColor redColor];
    _contentImageView.layer.cornerRadius = 10;//_contentImageView.frame.size.height/2;
    _contentImageView.layer.masksToBounds = true;
    [self.contentView addSubview:_contentImageView];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, width-80, 30)];
    _nameLabel.textColor = [UIColor blackColor];
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont fontWithName:@"Helvetica Medium" size:16];
    [self.contentView addSubview:_nameLabel];
    
    _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(width - 22, 18, 10, 13)];
    _arrowImageView.image = [UIImage imageNamed:@"back-arrow Copy.png"];
    [self.contentView addSubview:_arrowImageView];
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(70, self.contentView.frame.size.height-1, width-70, 0.5)];
    _lineView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:_lineView];
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
