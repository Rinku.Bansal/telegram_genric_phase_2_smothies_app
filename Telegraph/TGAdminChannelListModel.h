//
//  TGAdminChannelListModel.h
//  Telegraph
//
//  Created by Rinku on 13/01/19.
//

#import <Foundation/Foundation.h>

@interface TGAdminChannelListModel : NSObject

@property (nonatomic, strong) NSString *channelName;
@property (nonatomic, strong) NSString *channelId;

+ (NSMutableArray *)getAdminChannelList;

@end


@interface TGAdminBotsListModel : NSObject

@property (nonatomic, strong) NSString *botName;
@property (nonatomic, strong) NSString *botId;

+ (NSMutableArray *)getAdminBotList;

@end

@interface TGGroupCreationList : NSObject

@property (nonatomic, strong) NSString *telegramId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *appId;
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic, strong) NSString *createdDate;

+ (NSMutableArray *)getGroupCreationList:(NSArray *)arr;
@end
