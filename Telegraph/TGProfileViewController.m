//
//  TGProfileViewController.m
//  Telegraph
//
//  Created by vinove on 07/08/18.
//

#import "TGProfileViewController.h"
#import <LegacyComponents/TGModernBarButton.h>
#import "TGDatabase.h"
#import "TGSettingViewController.h"
#import <LegacyComponents/TGRemoteImageView.h>
#import <LegacyComponents/TGImageManager.h>
#import <LegacyComponents/TGLetteredAvatarView.h>
#import "TGModernLetteredAvatarViewModel.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"
#import "TGProfileTableViewCell.h"
#import "TGSmoothiesNutritionPrefrenceViewController.h"
#import "TGSmothiesNutritionGoalsViewController.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGHealthConditionViewController.h"
#import "TGIngredientsViewController.h"
#import "TelegramServiceClass.h"
#import "TGSavedPrefrence.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGInterfaceManager.h"

@interface TGProfileViewController ()<UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    __weak IBOutlet UIScrollView *scrollerView;
    __weak IBOutlet UIView *profileContainerView;
    __weak IBOutlet UIImageView *userImageView;
    __weak IBOutlet UILabel *lblUserName;
    __weak IBOutlet UILabel *lblUserFullName;
    __weak IBOutlet UITableView *personalizedNutritionTableView;
    __weak IBOutlet UITableView *yourSmoothiesTableView;
    __weak IBOutlet UILabel *lblPersonalizeNutrition;
    __weak IBOutlet UILabel *lblOptimize;
    __weak IBOutlet UILabel *lblYourSmoothies;
    
    NSArray *nutritionArr;
    NSArray *smoothiesArr;
    TGCache *_cache;
    UIView *containerView;
    UIView *bottomView;
}
@end

@implementation TGProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TGThemeColor();
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGProfileViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    [TGMixPanelEventsConstants trackMixpanelEvents:PROFILE properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:PROFILE]];

    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"profile" withInfoParam:@"title" withDefaultStr:TGLocalized(@"Profile_Header_Title")];//@"Profile";
    [TGSavedPrefrence saveChangeInSmoothiesBot:false];
    [self setLeftBarButtonItem:[self controllerLeftBarButtonItem]];
    [self setRightBarButtonItem:[self controllerRightBarButtonItem]];
    [self getAllIngredientsInformationFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initialiseView];
    [self showIngredientsDetails];
}

- (void)initialiseView {
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-40, self.view.frame.size.height, 40)];
        bottomView.backgroundColor = TGThemeColor();
        [self.view addSubview:bottomView];
    }
    
    lblPersonalizeNutrition.text = TGLocalized(@"Profile_Personalized_Nutrition");
    lblOptimize.text = TGLocalized(@"Profile_Optimize_Your_Goals_Prefrences_Needs");
    lblYourSmoothies.text = TGLocalized(@"Profile_Your_Smoothies");
    profileContainerView.layer.cornerRadius = profileContainerView.frame.size.width/2;
    profileContainerView.layer.masksToBounds = true;
    TGUser *user = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    if ([user.photoUrlSmall isEqualToString:@""] || user.photoUrlSmall == nil) {
        NSString *firstNameChar = @"";
        NSString *lastNameChar = @"";
        if (![user.firstName isEqualToString:@""]) {
            NSString *firstName = [NSString stringWithFormat:@"%@",user.firstName];
            if (![firstName isEqualToString:@"(null)"]) {
                firstNameChar = [NSString stringWithFormat:@"%@",[user.firstName substringToIndex:1]];
            }
        }
        if (![user.lastName isEqualToString:@""]) {
            NSString *lastName = [NSString stringWithFormat:@"%@",user.lastName];
            if (![lastName isEqualToString:@"(null)"]) {
                lastNameChar = [NSString stringWithFormat:@"%@",[user.lastName substringToIndex:1]];
            }
        }
        if ([firstNameChar isEqualToString:@""] && [lastNameChar isEqualToString:@""]){
            UIImage *defaultImage = [UIImage imageNamed:@"Profile icon.png"];
            profileContainerView.backgroundColor = [UIColor clearColor];
            userImageView.image = defaultImage;
        }else {
            [userImageView setHidden:YES];
            lblUserName.text = [NSString stringWithFormat:@"%@%@",firstNameChar,lastNameChar];
        }
    }else {
        NSString *filter = @"circle:64x64";
        static UIImage *placeholder = nil;
        static dispatch_once_t onceToken2;
        dispatch_once(&onceToken2, ^
                      {
                          UIGraphicsBeginImageContextWithOptions(CGSizeMake(40.0f, 40.0f), false, 0.0f);
                          CGContextRef context = UIGraphicsGetCurrentContext();
                          
                          //!placeholder
                          CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
                          CGContextFillEllipseInRect(context, CGRectMake(0.0f, 0.0f, 40.0f, 40.0f));
                          CGContextSetStrokeColorWithColor(context, UIColorRGB(0xd9d9d9).CGColor);
                          CGContextSetLineWidth(context, 1.0f);
                          CGContextStrokeEllipseInRect(context, CGRectMake(0.5f, 0.5f, 39.0f, 39.0f));
                          
                          placeholder = UIGraphicsGetImageFromCurrentImageContext();
                          UIGraphicsEndImageContext();
                      });
        // _cache = @"cache";
        TGCache *cache = _cache != nil ? _cache : [TGRemoteImageView sharedCache];
        
        NSString *cacheUrl = filter == nil ? user.photoUrlSmall : [[NSString alloc] initWithFormat:@"{filter:%@}%@", filter, user.photoUrlSmall];
        
        UIImage *image = [cache cachedImage:cacheUrl availability:TGCacheMemory];
        
        if (image == nil)
            image = [[TGImageManager instance] loadImageSyncWithUri:user.photoUrlSmall canWait:false decode:true acceptPartialData:false asyncTaskId:NULL progress:nil partialCompletion:nil completion:nil];
        
        if (image == nil && (4 & TGRemoteImageContentHintLoadFromDiskSynchronously))
        {
            UIImage *managerImage = [[TGImageManager instance] loadImageSyncWithUri:user.photoUrlSmall canWait:true decode:filter == nil acceptPartialData:false asyncTaskId:NULL progress:nil partialCompletion:nil completion:nil];
            if (managerImage == nil)
                managerImage = [cache cachedImage:user.photoUrlSmall availability:TGCacheDisk];
            
            if (managerImage != nil)
            {
                if (filter != nil)
                {
                    TGImageProcessor procesor = [TGRemoteImageView imageProcessorForName:filter];
                    if (procesor != nil)
                        image = procesor(managerImage);
                }else {
                    image = managerImage;
                }
                [lblUserName setHidden:YES];
                userImageView.image = image;
            }else {
                NSString *firstNameChar = @"";
                NSString *lastNameChar = @"";
                if (![user.firstName isEqualToString:@""]) {
                    NSString *firstName = [NSString stringWithFormat:@"%@",user.firstName];
                    if (![firstName isEqualToString:@"(null)"]) {
                        firstNameChar = [NSString stringWithFormat:@"%@",[user.firstName substringToIndex:1]];
                    }
                }
                if (![user.lastName isEqualToString:@""]) {
                    NSString *lastName = [NSString stringWithFormat:@"%@",user.lastName];
                    if (![lastName isEqualToString:@"(null)"]) {
                        lastNameChar = [NSString stringWithFormat:@"%@",[user.lastName substringToIndex:1]];
                    }
                }
                if ([firstNameChar isEqualToString:@""] && [lastNameChar isEqualToString:@""]){
                    UIImage *defaultImage = [UIImage imageNamed:@"Profile icon.png"];
                    profileContainerView.backgroundColor = [UIColor clearColor];
                    userImageView.image = defaultImage;
                }else {
                    [userImageView setHidden:YES];
                    lblUserName.text = [NSString stringWithFormat:@"%@%@",firstNameChar,lastNameChar];
                }
            }
        }else {
            [lblUserName setHidden:YES];
            userImageView.image = image;
        }
    }
    NSString *firstName = @"";
    NSString *lastName = @"";
    
    NSString *first = [NSString stringWithFormat:@"%@", user.firstName];
    if (![first isEqualToString:@"(null)"]) {
        firstName = user.firstName;
    }
    
    NSString *last = [NSString stringWithFormat:@"%@", user.lastName];
    if (![last isEqualToString:@"(null)"]) {
        lastName = user.lastName;
    }
    lblUserFullName.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    nutritionArr = [[NSArray alloc] init];
    smoothiesArr = [[NSArray alloc] init];
    
   // NSString *userChannelName = [NSString stringWithFormat:@"%@%@", firstName,TGLocalized(@"Profile_My_Channel")];
    smoothiesArr = @[ @{@"Name": TGLocalized(@"Profile_Favorate_Smoothies"), @"image": @"mark-as-favourite-star.png"}];
                      //@{@"Name": userChannelName, @"image": @"channelChirag.png"}];
    [yourSmoothiesTableView reloadData];
    
    [scrollerView setContentSize:CGSizeMake(self.view.frame.size.width, yourSmoothiesTableView.frame.origin.y + yourSmoothiesTableView.frame.size.height + 10)];

}

- (UIBarButtonItem *)controllerLeftBarButtonItem {
        UIImage* image3 = [UIImage imageNamed:@"slice.png"];
        CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(settingButtonPressed)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setShowsTouchWhenHighlighted:YES];
        UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        return mailbutton;
}

- (UIBarButtonItem *)controllerRightBarButtonItem {
    TGModernBarButton *composeButton = [[TGModernBarButton alloc] initWithImage:[UIImage imageNamed:@"slice 2.png"]];//@"ModernNavigationComposeButtonIcon.png"]];
    composeButton.portraitAdjustment = CGPointMake(0, 0);//CGPointMake(-7, -5);
    composeButton.landscapeAdjustment = CGPointMake(0, 0);//CGPointMake(-7, -4);
    [composeButton addTarget:self action:@selector(composeMessageButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *unreadCount = @"0";//[NSString stringWithFormat:@"%d",[TGFetchSavedPrefrenceInfoGenric getUnreadCountDisplayedCells]];
    if (![unreadCount isEqualToString:@"0"]) {
        UIView *badgeView = [[UIView alloc] initWithFrame:CGRectMake(composeButton.frame.origin.x+composeButton.frame.size.width-40, 0.0, 20, 20)];
        badgeView.backgroundColor = [UIColor redColor];
        badgeView.layer.cornerRadius = badgeView.frame.size.height/2;
        badgeView.layer.masksToBounds = true;
        [composeButton addSubview:badgeView];
        
        UILabel *unreadCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, badgeView.frame.size.width, badgeView.frame.size.height)];
        unreadCountLabel.text = unreadCount;
        unreadCountLabel.textColor = [UIColor whiteColor];
        unreadCountLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        unreadCountLabel.textAlignment = NSTextAlignmentCenter;
        [badgeView addSubview:unreadCountLabel];
    }
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"UnReadCountMessageGenric"]];
//    UIImage *img = [UIImage imageNamed:@"jb.png"];
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(45-self.view.frame.size.width/2, 30, img.size.width, img.size.height)];
//    [imgView setImage:img];
//    [imgView setContentMode:UIViewContentModeScaleAspectFit];
//    [composeButton addSubview:imgView];
    
    return [[UIBarButtonItem alloc] initWithCustomView:composeButton];
}

- (void)getAllIngredientsInformationFromServer {
    [[TelegramServiceClass shareInstance] getAllGoalsPrefrencesIngredientsHealthConditionInformationFromServerphnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] :^(BOOL success, NSError *error, NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                [TGSavedPrefrence saveSelectedGoalsName:[dic objectForKey:@"goal"]];
                [TGSavedPrefrence saveSelectedPrefrencesName:[dic objectForKey:@"preference"]];
                [TGSavedPrefrence saveSelectedIngredientsList:[dic objectForKey:@"indigrants_like"] dislike:[dic objectForKey:@"indigrants_dislike"]];
                [TGSavedPrefrence saveSeletedHealthCondition:[dic objectForKey:@"health_conditions"]];
                [self showIngredientsDetails];
                NSLog(@"data dic is ====>>>%@",dic);
                
            }else {
                NSLog(@"error is ===>>>%@", error);
            }
        });
    }];
}

- (void)showIngredientsDetails {
    NSString *health = @"";
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedHealthConditionList].count > 0) {
        health = [NSString stringWithFormat:@"%lu %@",(unsigned long)[TGFetchSavedPrefrenceInfoGenric getSelectedHealthConditionList].count, TGLocalized(@"Profile_Health_Condition")];
    } else {
        health = [NSString stringWithFormat:@"0 %@", TGLocalized(@"Profile_Health_Condition")];//@"Health Condition";
    }
    
    NSString *prefrence = @"";
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved].count > 0) {
        if ([TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved].count > 1) {
            prefrence = [NSString stringWithFormat:@"%lu %@", (unsigned long)[TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved].count,TGLocalized(@"Profile_Prefrences")];
        }else {
            prefrence = [NSString stringWithFormat:@"%lu %@", (unsigned long)[TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved].count,TGLocalized(@"Profile_Prefrence")];
        }
    }else {
        prefrence = [NSString stringWithFormat:@"0 %@", TGLocalized(@"Profile_Prefrence")];//@"Preferences";
    }
    
    NSString *goals = @"";
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved].count > 0) {
        if ([TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved].count > 1) {
            goals = [NSString stringWithFormat:@"%lu %@", (unsigned long)[TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved].count,TGLocalized(@"Profile_Goals")];
        }else {
            goals = [NSString stringWithFormat:@"%lu %@", (unsigned long)[TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved].count,TGLocalized(@"Profile_Goal")];
        }
    }else {
        goals = [NSString stringWithFormat:@"0 %@", TGLocalized(@"Profile_Goal")];//@"Goals";
    }
    
    NSUInteger like = 0;
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsLikeList].count > 0) {
        like = [TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsLikeList].count;
    }else {
        like = 0;
    }
    
    NSUInteger dislike = 0;
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsDisLikeList].count > 0) {
        dislike = [TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsDisLikeList].count;
    }else {
        dislike = 0;
    }
    
    NSInteger likeDislike = like+dislike;
    NSString *likeDislikeStr = [NSString stringWithFormat:@"%ld %@",(long)likeDislike, TGLocalized(@"Profile_Ingredients_Like_Avoid")];
    
    nutritionArr = @[ @{@"Name": goals, @"image": @"goals.png"},
                      @{@"Name": prefrence, @"image": @"prefrences.png"},
                      @{@"Name": likeDislikeStr, @"image": @"ingredients.png"},
                      @{@"Name": health, @"image": @"healthCondition.png"}];
    [personalizedNutritionTableView reloadData];
}

- (void)settingButtonPressed {
    [TGMixPanelEventsConstants trackMixpanelEvents:PROFILE properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:SETTING element:BUTTON screenName:PROFILE]];
      TGSettingViewController *controller = [[TGSettingViewController alloc] init];
      [self.navigationController pushViewController:controller animated:false];
}

- (void)composeMessageButtonPressed {
    [TGMixPanelEventsConstants trackMixpanelEvents:PROFILE properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:MESSAGE element:BUTTON screenName:PROFILE]];
    if ([TGFetchSavedPrefrenceInfoGenric isSmoothiesChanges]) {
        [self saveAllPrefrencesListFromSmoothiesServer];
    }
    [self.navigationController popViewControllerAnimated:false];
}

- (void)saveAllPrefrencesListFromSmoothiesServer {
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    [[TelegramServiceClass shareInstance] saveUserAllPrefrencestoTheSmoothiesServerchatUserId:chatUserId :^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"prefrences dic is ====>>>>>%@", dic);
        }else {
            NSLog(@"error is ====>>>>%@",error);
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == personalizedNutritionTableView) {
        return nutritionArr.count;
    }else {
        return smoothiesArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == personalizedNutritionTableView) {
        TGProfileTableViewCell *cell = (TGProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGProfileTableViewCell"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGProfileTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.lblContent.text = [NSString stringWithFormat:@"%@",[[nutritionArr objectAtIndex:indexPath.row] objectForKey:@"Name"]];
        cell.contentImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[nutritionArr objectAtIndex:indexPath.row] objectForKey:@"image"]]];
        cell.backgroundColor = TGThemeColor();
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else {
        TGProfileTableViewCell *cell = (TGProfileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGProfileTableViewCell"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGProfileTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.lblContent.text = [NSString stringWithFormat:@"%@",[[smoothiesArr objectAtIndex:indexPath.row] objectForKey:@"Name"]];
        cell.contentImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[smoothiesArr objectAtIndex:indexPath.row] objectForKey:@"image"]]];
        cell.backgroundColor = TGThemeColor();
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == personalizedNutritionTableView) {
        if (indexPath.row == 0) {
            TGSmothiesNutritionGoalsViewController *controller = [[TGSmothiesNutritionGoalsViewController alloc] init];
            controller.is_Login = false;
            [self.navigationController pushViewController:controller animated:true];
        }else if (indexPath.row == 1) {
            TGSmoothiesNutritionPrefrenceViewController *controller = [[TGSmoothiesNutritionPrefrenceViewController alloc] init];
            controller.is_Login = false;
            [self.navigationController pushViewController:controller animated:true];
        }else if (indexPath.row == 2) {
            TGIngredientsViewController *controller = [[TGIngredientsViewController alloc] initWithNibName:@"TGIngredientsViewController" bundle:nil];
            [self.navigationController pushViewController:controller animated:true];
        }else {
            TGHealthConditionViewController *controller = [[TGHealthConditionViewController alloc] initWithNibName:@"TGHealthConditionViewController" bundle:nil];
            [self.navigationController pushViewController:controller animated:true];
        }
    }else {
        [TGSavedPrefrence saveControllerSelectedType:false];
        //if (indexPath.row == 0) {
        int64_t conversationId = [[TGFetchSavedPrefrenceInfoGenric getFavorateSmoothiesChannelId] longLongValue];
        int64_t converID = conversationId - ((int64_t)INT32_MIN) * 2;
        NSString *encrypthId = [NSString stringWithFormat:@"-%lld",converID];
        if (conversationId != 0) {
            [[TGInterfaceManager instance] navigateToConversationWithId:[encrypthId longLongValue] conversation:nil];
        }
//        }else {
//            int64_t conversationId =  [[TGFetchSavedPrefrenceInfoGenric getUserNameCreatedChannelId] longLongValue];
//            if (conversationId != 0) {
//                [[TGInterfaceManager instance] navigateToConversationWithId:conversationId conversation:nil];
//            }
//        }
    }
}

@end
