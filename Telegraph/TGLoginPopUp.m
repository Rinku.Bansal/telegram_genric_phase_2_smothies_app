//
//  TGLoginPopUp.m
//  Telegraph
//
//  Created by Rinku on 19/02/19.
//

#import "TGLoginPopUp.h"

@implementation TGLoginPopUp

- (IBAction)btnOkClicked:(id)sender {
    [self removeFromSuperview];
    if (_delegate != nil) {
        [_delegate clickOkButton];
    }
}

- (IBAction)tapGuestureClicked:(id)sender {
    if (_delegate != nil) {
        [_delegate dismissKeyboardWhenTap];
    }
}

@end
