//
//  TGSmothiesNutritionGoalsViewController.m
//  Telegraph
//
//  Created by vinove on 12/17/18.
//

#import "TGSmothiesNutritionGoalsViewController.h"
#import <LegacyComponents/TGAnimationUtils.h>
#import "RMIntroViewController.h"
#import "TGNutritionGoalsModel.h"
#import "TGSmoothiesGoalsCollectionViewCell.h"
#import "TGSmoothiesNutritionPrefrenceViewController.h"
#import "TGSavedPrefrence.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TelegramServiceClass.h"
#import "TGUser+Telegraph.h"
#import "TGDatabase.h"
#import "TGMixPanelEventsConstants.h"

@interface TGSmothiesNutritionGoalsViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SelectMultipleGoalsProtocol>{
    NSMutableArray *goalsArr;
    UIView *containerView;
    UILabel *lblYourGoals;
    UILabel *lblSelectGoalsCount;
    UICollectionView *goalsCollectionView;
    UILabel *lblGoalsCount;
    
}

@end

@implementation TGSmothiesNutritionGoalsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleText = TGLocalized(@"Goals_Prefrences_Header_Title");//@"Smoothies Nutrition";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    goalsArr = [[NSMutableArray alloc] init];
    goalsArr = [TGNutritionGoalsModel getNutritionGoalsDetails:[self goalsAllArr]];
    [TGMixPanelEventsConstants trackMixpanelEvents:GOALS properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:GOALS]];
    [self initialiseTheView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = TGThemeColor();
    [self getSelectedGoals];
}

// Mark :- Private Methods
- (void)initialiseTheView {
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    containerView.backgroundColor = TGThemeColor();
    [self.view addSubview:containerView];
    
    _startButton = [[UIButton alloc] init];
    if (_is_Login) {
        [_startButton setTitle:TGLocalized(@"Common_Next") forState:UIControlStateNormal];
    }else {
        [_startButton setTitle:TGLocalized(@"Common_Save") forState:UIControlStateNormal];
    }
    [_startButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:20.0f]];
    [_startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(48.0f, 48.0f), false, 0.0f);
        CGContextRef contextRef = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(contextRef, [UIColor colorWithRed:89.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1].CGColor);
        CGContextFillEllipseInRect(contextRef, CGRectMake(0.0f, 0.0f, 48.0f, 48.0f));
        UIImage *startButtonImage = [UIGraphicsGetImageFromCurrentImageContext() stretchableImageWithLeftCapWidth:24 topCapHeight:24];
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(48.0f, 48.0f), false, 0.0f);
        contextRef = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(contextRef, [UIColor colorWithRed:89.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1].CGColor);
        CGContextFillEllipseInRect(contextRef, CGRectMake(0.0f, 0.0f, 48.0f, 48.0f));
        UIImage *startButtonHighlightedImage = [UIGraphicsGetImageFromCurrentImageContext() stretchableImageWithLeftCapWidth:24 topCapHeight:24];
        UIGraphicsEndImageContext();
        
        [_startButton setBackgroundImage:startButtonImage forState:UIControlStateNormal];
        [_startButton setBackgroundImage:startButtonHighlightedImage forState:UIControlStateHighlighted];
        [_startButton setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 20.0f, 0.0f, 20.0f)];
    }
    _startButton.titleLabel.clipsToBounds = false;
    _startButton.backgroundColor = [UIColor colorWithRed:89.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1];
    int cornerRadius = 20;
    int screenSize = [self deviceScreen];
    if (screenSize == 0) {
        cornerRadius = 20;
    }else if (screenSize == 1) {
        cornerRadius = 23;

    }else if (screenSize == 2) {
        cornerRadius = 25;

    }else {
        cornerRadius = 25;
    }
    _startButton.layer.cornerRadius = cornerRadius;
    _startButton.layer.masksToBounds = true;
    [_startButton sizeToFit];
    [_startButton addTarget:self action:@selector(nextButtonPress) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:_startButton];
    
    lblYourGoals = [[UILabel alloc] init];
    lblYourGoals.text = TGLocalized(@"Goals_What_are_Your_Goals");
    lblYourGoals.textAlignment = NSTextAlignmentCenter;
    lblYourGoals.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    [containerView addSubview:lblYourGoals];
    
    lblSelectGoalsCount = [[UILabel alloc] init];
    lblSelectGoalsCount.text = TGLocalized(@"Goals_Step_1_out_of_2");//@"step 1 out of 2";
    lblSelectGoalsCount.textAlignment = NSTextAlignmentCenter;
    lblSelectGoalsCount.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
    [containerView addSubview:lblSelectGoalsCount];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(170, 170);
    goalsCollectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [goalsCollectionView setDataSource:self];
    [goalsCollectionView setDelegate:self];
    goalsCollectionView.showsHorizontalScrollIndicator = false;
    goalsCollectionView.showsVerticalScrollIndicator = false;
    goalsCollectionView.alwaysBounceVertical = true;
    [goalsCollectionView registerClass:[TGSmoothiesGoalsCollectionViewCell class] forCellWithReuseIdentifier:@"TGSmoothiesGoalsCollectionViewCell"];
    [goalsCollectionView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:goalsCollectionView];
    
    lblGoalsCount = [[UILabel alloc] init];
    lblGoalsCount.text = @"";
    lblGoalsCount.textAlignment = NSTextAlignmentCenter;
    lblGoalsCount.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
    [containerView addSubview:lblGoalsCount];
}

// Text Arr Method
- (NSArray *)goalsArr {
    NSArray *goalArr = [[NSArray alloc] init];
    goalArr = [TGLocalized(@"Goals_List") componentsSeparatedByString:@","];
    return goalArr;
}

// Image Arr Method
- (NSArray *)imageArr {
    NSArray *imageArr = [[NSArray alloc] init];
    imageArr = @[@"UnselectedMuscleBuilding.png",@"unselectedWeightLoss.png",@"shapingGoal.png",@"healthImrpovement.png",@"healthyskingoal.png",@"bettersleepgoal.png",@"Unselected_Better-Vision.png"];
    return imageArr;
}

- (NSArray *)goalsAllArr {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < [self goalsArr].count; i++) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:[NSString stringWithFormat:@"%@",[[self goalsArr] objectAtIndex:i]] forKey:@"Name"];
        [dic setObject:[NSString stringWithFormat:@"%@",[[self imageArr] objectAtIndex:i]] forKey:@"Image"];
        [arrToReturn addObject:dic];
    }
    return (NSArray *)arrToReturn;
}

// Get Selected Goals
- (void)getSelectedGoals {
    NSArray *selectedGoalsArr = [TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved];
    for (NSUInteger i = 0; i<selectedGoalsArr.count; i++) {
        NSString *goalsStr = [NSString stringWithFormat:@"%@",[selectedGoalsArr objectAtIndex:i]].capitalizedString;
        for (NSUInteger j = 0; j<goalsArr.count; j++) {
            TGNutritionGoalsModel *model = [goalsArr objectAtIndex:j];
            if ([model.name.capitalizedString isEqualToString:goalsStr]) {
                model.is_Selected = true;
            }
        }
    }
    if (selectedGoalsArr.count > 0) {
        if (selectedGoalsArr.count == 1) {
            lblGoalsCount.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)selectedGoalsArr.count, TGLocalized(@"Goal_Selected")];
            
        }else {
            lblGoalsCount.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)selectedGoalsArr.count, TGLocalized(@"Goals_Selected")];
            
        }
        [_startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _startButton.userInteractionEnabled = true;
    }else {
        [_startButton setTitleColor:[UIColor colorWithRed:166.0/255.0 green:200.0/255.0 blue:199.0/255.0 alpha:1] forState:UIControlStateNormal];
        _startButton.userInteractionEnabled = false;
    }
    [goalsCollectionView reloadData];
}

// Mark :- viewWillLayoutSubviews Set Frame
- (void)viewWillLayoutSubviews
{
    CGFloat statusBarHeight = (iosMajorVersion() >= 7) ? 0 : 20;
    CGFloat startButtonY = 0;
    int originY = 0;
    DeviceScreenSize deviceScreen = [self deviceScreen];
    switch (deviceScreen)
    {
        case iPadS:
            startButtonY = 120;
            break;
            
        case iPadProS:
            startButtonY = 120;
            break;
            
        case InchS35:
            originY = 40;
            startButtonY = 75;
            break;
            
        case InchS4:
            originY = 70;
            startButtonY = 75;
            break;
            
        case InchS47:
            originY = 60;
            startButtonY = 75 + 5;
            break;
            
        case InchS55:
            originY = 70;
            startButtonY = 75;
            break;
            
        default:
            break;
    }
    [_startButton sizeToFit];
    _startButton.frame = CGRectMake(CGFloor((self.view.bounds.size.width - _startButton.frame.size.width -40) / 2.0f), self.view.bounds.size.height - startButtonY - statusBarHeight, _startButton.frame.size.width+40, 48.0f);
    
    lblGoalsCount.frame = CGRectMake(10, self.view.bounds.size.height - startButtonY - statusBarHeight - 23, containerView.frame.size.width-20, 20);
    
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        lblYourGoals.frame = CGRectMake(10, originY+30, containerView.frame.size.width-20, 25);
    }else {
        lblYourGoals.frame = CGRectMake(10, originY, containerView.frame.size.width-20, 25);
    }
    lblSelectGoalsCount.frame = CGRectMake(10, lblYourGoals.frame.origin.y + lblYourGoals.frame.size.height + 5, containerView.frame.size.width-20, 25);
    
    goalsCollectionView.frame = CGRectMake(0.0, lblSelectGoalsCount.frame.origin.y + lblSelectGoalsCount.frame.size.height + 20, self.view.frame.size.width, _startButton.frame.origin.y - lblSelectGoalsCount.frame.origin.y + lblSelectGoalsCount.frame.size.height - 20 - _startButton.frame.size.height - lblGoalsCount.frame.size.height- 10);
    

}

// Change screen Size
- (DeviceScreenSize)deviceScreen
{
    CGSize viewSize = self.view.frame.size;
    int max = (int)MAX(viewSize.width, viewSize.height);
    
    DeviceScreenSize deviceScreen = InchS55;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        switch (max)
        {
            case 1366:
                deviceScreen = iPadProS;
                break;
                
            default:
                deviceScreen = iPadS;
                break;
        }
    }
    else
    {
        switch (max)
        {
            case 480:
                deviceScreen = InchS35;
                break;
            case 568:
                deviceScreen = InchS4;
                break;
            case 667:
                deviceScreen = InchS47;
                break;
            default:
                deviceScreen = InchS55;
                break;
        }
    }
    
    return deviceScreen;
}

// Mark :- UIButton
- (void)nextButtonPress {
    NSMutableArray *saveGoalsArr = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < goalsArr.count; i++) {
        TGNutritionGoalsModel *model = [goalsArr objectAtIndex:i];
        if (model.is_Selected) {
            [saveGoalsArr addObject:model.name.capitalizedString];
        }
    }
//    if (saveGoalsArr.count == 0) {
//        [self showMessage:@"Please select at least one."];return;
//    }
    
    NSString *seprateStr = [saveGoalsArr componentsJoinedByString:@","];
    [TGSavedPrefrence saveSelectedGoalsName:seprateStr];
    if (_is_Login) {
        TGSmoothiesNutritionPrefrenceViewController *controller = [[TGSmoothiesNutritionPrefrenceViewController alloc] init];
        controller.is_Login = _is_Login;
        [self.navigationController pushViewController:controller animated:true];
        [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:GOALS back:NEXT] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:GOALS]];
    }else {
        [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:GOALS back:SAVE] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:GOALS]];
        [self.navigationController popViewControllerAnimated:true];
        [[TelegramServiceClass shareInstance] saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:seprateStr type:@"goals" like:@"" dislike:@"" phnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] with:^(BOOL success, NSError *error, NSDictionary *dic) {
            if (success) {
                NSLog(@"data dic is ====>>>%@",dic);
            }else {
                NSLog(@"error is ====>>>%@",error);
            }
        }];
      //  [self saveAllPrefrencesListFromSmoothiesServer];
    }
}

- (void)saveAllPrefrencesListFromSmoothiesServer {
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    [[TelegramServiceClass shareInstance] saveUserAllPrefrencestoTheSmoothiesServerchatUserId:chatUserId :^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"prefrences dic is ====>>>>>%@", dic);
        }else {
            NSLog(@"error is ====>>>>%@",error);
        }
    }];
}

// To show alert without any delegate
- (void)showMessage: (NSString *)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:GOALS properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:GOALS]];
    [self.navigationController popViewControllerAnimated:true];
}

// Mark :- UICollectionViewDelegate, UICollectionViewDataSource

- (bool)collectionView:(UICollectionView *)__unused collectionView canMoveItemAtIndexPath:(NSIndexPath *)__unused indexPath
{
    return false;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)__unused collectionView layout:(UICollectionViewLayout*)__unused collectionViewLayout insetForSectionAtIndex:(NSInteger)__unused section
{
  

    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return goalsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TGSmoothiesGoalsCollectionViewCell *cell = (TGSmoothiesGoalsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGSmoothiesGoalsCollectionViewCell" forIndexPath:indexPath];
    TGNutritionGoalsModel *model = [goalsArr objectAtIndex:indexPath.row];
    cell.lblGoals.text = model.name;
    cell.cellIndex = indexPath.row;
    cell.selectDelegate = self;
    [cell.btnGoals setImage:[UIImage imageNamed:model.image] forState:UIControlStateNormal];
    if (model.is_Selected) {
        [cell.btnGoals setBackgroundColor:[UIColor colorWithRed:241.0/255.0 green:149.0/255.0 blue:28.0/255.0 alpha:1]];
    }else {
        [cell.btnGoals setBackgroundColor:[UIColor colorWithRed:89.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1]];
    }
    return cell;
}

- (void)selectionClickedAtIndex:(NSInteger)index {
    [TGSavedPrefrence saveChangeInSmoothiesBot:YES];
    TGNutritionGoalsModel *model = [goalsArr objectAtIndex:index];
    if (model.is_Selected) {
        model.is_Selected = false;
    }else {
        model.is_Selected = true;
    }
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    [goalsCollectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexpath]];
    
    int k = 0;
    for (NSUInteger i = 0; i < goalsArr.count; i++) {
        TGNutritionGoalsModel *model = [goalsArr objectAtIndex:i];
        if (model.is_Selected) {
            k = k+1;
        }
    }
    if (k == 0) {
        lblGoalsCount.text = @"";
        [_startButton setTitleColor:[UIColor colorWithRed:166.0/255.0 green:200.0/255.0 blue:199.0/255.0 alpha:1] forState:UIControlStateNormal];
        _startButton.userInteractionEnabled = false;
    }else {
        if (k == 1) {
            lblGoalsCount.text = [NSString stringWithFormat:@"%d %@", k, TGLocalized(@"Goal_Selected")];
        }else {
            lblGoalsCount.text = [NSString stringWithFormat:@"%d %@", k, TGLocalized(@"Goals_Selected")];
        }
        [_startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _startButton.userInteractionEnabled = true;
    }
}

@end
