//
//  TGHealthConditionModel.h
//  Telegraph
//
//  Created by vinove on 1/8/19.
//

#import <Foundation/Foundation.h>

@interface TGHealthConditionModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *healthArr;

+ (NSArray *)healthConditionArray;
@end


@interface TGHealthModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) BOOL isSelected;

@end
