//
//  TGProfileTableViewCell.h
//  Telegraph
//
//  Created by vinove on 1/3/19.
//

#import <UIKit/UIKit.h>

@interface TGProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end
