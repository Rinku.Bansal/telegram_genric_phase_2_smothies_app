//
//  TGIngradiantsShowCollectionViewCell.m
//  Telegraph
//
//  Created by Rinku on 31/12/18.
//

#import "TGIngradiantsShowCollectionViewCell.h"

@implementation TGIngradiantsShowCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (IBAction)btnAmezonClick:(id)sender {
    
}

- (IBAction)ingredientsImageViewClick:(id)sender {
    if(_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(selectionClickedAtIndex:)])
        [_selectDelegate selectionClickedAtIndex:_cellIndex];
}

@end
