//
//  TGAboutUsViewController.m
//  Telegraph
//
//  Created by vinove on 06/09/18.
//

#import "TGAboutUsViewController.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"

@interface TGAboutUsViewController ()<UITextViewDelegate>
{
    UITextView *faqTextView;
}

@end

@implementation TGAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [TGMixPanelEventsConstants trackMixpanelEvents:ABOUT_US properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:ABOUT_US]];

    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"about" withInfoParam:@"title" withDefaultStr:TGLocalized(@"About_Header_Title")];//@"About Us";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    [self initialiseTheView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = TGThemeColor();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:ABOUT_US properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:ABOUT_US]];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)initialiseTheView {
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    int navigationBarHeight = 0;
    
    if (max == 812) {
        navigationBarHeight = 88;
    }else {
        navigationBarHeight = 64;
    }
    faqTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, navigationBarHeight+20, self.view.frame.size.width-20, self.view.frame.size.height-navigationBarHeight-30)];
    faqTextView.delegate = self;
    faqTextView.backgroundColor = [UIColor clearColor];
    faqTextView.textColor = [UIColor blackColor];
    faqTextView.text = TGLocalized(@"About_Us_Discription_Text");
    [faqTextView setFont:[UIFont systemFontOfSize:16.0]];
    [self.view addSubview:faqTextView];
}

@end
