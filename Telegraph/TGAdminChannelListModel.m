//
//  TGAdminChannelListModel.m
//  Telegraph
//
//  Created by Rinku on 13/01/19.
//

#import "TGAdminChannelListModel.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"

@implementation TGAdminChannelListModel

+ (NSMutableArray *)getAdminChannelList {
    NSArray *arr = [TGFetchSavedPrefrenceInfoGenric getAdminChannels];
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i=0; i < arr.count; i++) {
        NSDictionary *productDic = [arr objectAtIndex:i];
        TGAdminChannelListModel *model = [[TGAdminChannelListModel alloc] init];
        model.channelName = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"channel_name"]];
        model.channelId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"channel_telegram_id"]];
        [arrToReturn addObject:model];
    }
    return arrToReturn;
}
@end

@implementation TGAdminBotsListModel

+ (NSMutableArray *)getAdminBotList {
    NSArray *arr = [TGFetchSavedPrefrenceInfoGenric getAdminBots];
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i=0; i < arr.count; i++) {
        NSDictionary *productDic = [arr objectAtIndex:i];
        TGAdminBotsListModel *model = [[TGAdminBotsListModel alloc] init];
        model.botName = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"chatbot_name"]];
        model.botId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"chatbot_telegram_id"]];
        [arrToReturn addObject:model];
    }
    return arrToReturn;
}

@end

@implementation TGGroupCreationList

+ (NSMutableArray *)getGroupCreationList:(NSArray *)arr {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i=0; i < arr.count; i++) {
        NSDictionary *productDic = [arr objectAtIndex:i];
        TGGroupCreationList *model = [[TGGroupCreationList alloc] init];
        model.telegramId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"id"]];
        model.groupId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"group_id"]];
        model.userId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"telegram_user_id"]];
        model.createdDate = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"created_at"]];
        model.appId = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"app_id"]];
        [arrToReturn addObject:model];
    }
    return arrToReturn;
}

@end
