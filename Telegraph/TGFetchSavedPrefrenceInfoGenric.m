//
//  TGFetchSavedPrefrenceInfoGenric.m
//  Telegraph
//
//  Created by vinove on 12/12/18.
//

#import "TGFetchSavedPrefrenceInfoGenric.h"

#define ADMIN_CHANNEL_LIST @"ADMIN_CHANNEL_LIST"
#define ADMIN_BOT_LIST @"ADMIN_BOT_LIST"

#define USER_CHANNEL_LIST @"USER_CHANNEL_LIST"
#define USER_BOT_LIST @"USER_BOT_LIST"

#define USER_LIST_SAVE @"USER_LIST_SAVE"

#define USER_FIRST_NAME @"USER_FIRST_NAME"
#define USER_LAST_NAME @"USER_LAST_NAME"
#define USER_MOBILE_NUMBER @"USER_MOBILE_NUMBER"

#define USER_BLOCK_STATUS @"USER_BLOCK_STATUS"

#define ALL_APPLICATION_TEXT_INFO @"ALL_APPLICATION_TEXT_INFO"

#define IS_GENRIC_BOT_ON_ADMIN @"IS_GENRIC_BOT_ON_ADMIN"

#define IS_CONTROLLER_SELECTED_TYPE @"IS_CONTROLLER_SELECTED_TYPE"

#define SEARCH_ADD_JOIN_CHANNEL_ID @"SEARCH_ADD_JOIN_CHANNEL_ID"
#define SEARCH_ADD_JOIN_CHANNEL_NAME @"SEARCH_ADD_JOIN_CHANNEL_NAME"

#define SEARCH_ADD_BOT_ID @"SEARCH_ADD_BOT_ID"
#define SEARCH_ADD_BOT_USER_NAME @"SEARCH_ADD_BOT_USER_NAME"

#define DELETE_CHANNEL_ID @"DELETE_CHANNEL_ID"
#define DELETE_CHANNEL_NAME @"DELETE_CHANNEL_NAME"

#define DELETE_BOT_ID @"DELETE_BOT_ID"
#define DELETE_BOT_USER_NAME @"DELETE_BOT_USER_NAME"

#define DELETE_USER_ID @"DELETE_USER_ID"

#define GENRIC_APP_ID @"GENRIC_APP_ID"

#define TELEGRAM_UID_TELEGRAM_SERVER @"TELEGRAM_UID_TELEGRAM_SERVER"

#define CHAT_USER_NAME @"CHAT_USER_NAME"

#define DAYS_INSATLL_COUNT @"DAYS_INSATLL_COUNT"

#define DAYS_APP_LAUNCH_COUNT @"DAYS_APP_LAUNCH_COUNT"

#define CURRENT_DATE_SAVE @"CURRENT_DATE_SAVE"

#define REFFRAL_NAME @"REFFRAL_NAME"



#define SAVE_SELECTED_GOALS_BEFORE_LOGIN @"SAVE_SELECTED_GOALS_BEFORE_LOGIN"

#define SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN @"SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN"

#define SAVE_FAVORATE_RECIPE_ID @"SAVE_FAVORATE_RECIPE_ID"

#define SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS @"SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS"

#define SAVE_SELECTED_INGREDIENTS_LIST_LIKE @"SAVE_SELECTED_INGREDIENTS_LIST_LIKE"

#define SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE @"SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE"

#define SAVE_SELECTED_HEALTH_CONDITION @"SAVE_SELECTED_HEALTH_CONDITION"

#define SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL @"SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL"

#define SAVE_USER_CREATE_USER_NAME_CHANNEL @"SAVE_USER_CREATE_USER_NAME_CHANNEL"

#define SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE @"SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE"

#define SAVE_USER_FIRST_TIME_LOGIN @"SAVE_USER_FIRST_TIME_LOGIN"

#define SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS @"SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS"

#define SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API @"SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API"

#define SAVE_CAMPAIGN_NAME_APPSFLYER @"SAVE_CAMPAIGN_NAME_APPSFLYER"

#define SAVE_GROUP_CREATION_LIST @"SAVE_GROUP_CREATION_LIST"

@implementation TGFetchSavedPrefrenceInfoGenric


// Get Admin channels and Admin bots getting from our server
+ (NSArray *)getAdminChannels {
    NSArray *arr = [[NSArray alloc] init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:ADMIN_CHANNEL_LIST];
    return arr;
}

+ (NSArray *)getAdminBots {
    NSArray *arr = [[NSArray alloc] init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:ADMIN_BOT_LIST];
    return arr;
}

// Get User Channels and User Bots when user Add
+ (NSMutableArray *)getUserChannels {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:USER_CHANNEL_LIST];
    return arr;
}

+ (NSMutableArray *)getUserBots {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:USER_BOT_LIST];
    return arr;
}

// Get saved user list details
+ (NSMutableArray *)getUserList {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:USER_LIST_SAVE];
    return arr;
}

// Get Telegram user information
+ (NSString *)getFirstName {
    NSString *firstName = [[NSString alloc] init];
    firstName = [[NSUserDefaults standardUserDefaults] objectForKey:USER_FIRST_NAME];
    if ([firstName isEqualToString:@"(null)"] && [firstName isEqualToString:@"<nil>"] && [firstName isEqualToString:@""]) {
        return @" ";
    }
    return firstName;
}

+ (NSString *)getLastName {
    NSString *lastName = [[NSString alloc] init];
    lastName = [[NSUserDefaults standardUserDefaults] objectForKey:USER_LAST_NAME];
    if ([lastName isEqualToString:@"(null)"] && [lastName isEqualToString:@"<nil>"] && [lastName isEqualToString:@""]) {
        return @" ";
    }
    return lastName;
}

+ (NSString *)getMobileNumber {
    NSString *mobileNumber = [[NSString alloc] init];
    mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:USER_MOBILE_NUMBER];
    if ([mobileNumber isEqualToString:@"(null)"] && [mobileNumber isEqualToString:@"<nil>"] && [mobileNumber isEqualToString:@""]) {
        return @" ";
    }
    return mobileNumber;
}

// Get all application Text from saved prefrence
+ (NSDictionary *)getAllApplicationSavedText {
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [[NSUserDefaults standardUserDefaults] objectForKey:ALL_APPLICATION_TEXT_INFO];
    return dic;
}

// is Genric Bot or not
+ (bool)isGenricBot {
    bool isGenric = [[NSUserDefaults standardUserDefaults] boolForKey:IS_GENRIC_BOT_ON_ADMIN];
    return isGenric;
}

// is controller selected tableview selection or not
+ (bool)isSelectedType {
    bool isSelected = [[NSUserDefaults standardUserDefaults] boolForKey:IS_CONTROLLER_SELECTED_TYPE];
    return isSelected;
}

// Get user search channels and add channels and save channel info
+ (NSString *)getAddedChannelId {
    NSString *channelId = [[NSString alloc] init];
    channelId = [[NSUserDefaults standardUserDefaults] objectForKey:SEARCH_ADD_JOIN_CHANNEL_ID];
    return channelId;
}

+ (NSString *)getAddedChannelName {
    NSString *channelName = [[NSString alloc] init];
    channelName = [[NSUserDefaults standardUserDefaults] objectForKey:SEARCH_ADD_JOIN_CHANNEL_NAME];
    return channelName;
}

// Get search Bots and add save bot info
+ (NSString *)getAddedBotId {
    NSString *botId = [[NSString alloc] init];
    botId = [[NSUserDefaults standardUserDefaults] objectForKey:SEARCH_ADD_BOT_ID];
    return botId;
}

+ (NSString *)getAddedBotName {
    NSString *botName = [[NSString alloc] init];
    botName = [[NSUserDefaults standardUserDefaults] objectForKey:SEARCH_ADD_BOT_USER_NAME];
    return botName;
}

// Get delete channel info save
+ (NSString *)getDeleteChannelId {
    NSString *channelId = [[NSString alloc] init];
    channelId = [[NSUserDefaults standardUserDefaults] objectForKey:DELETE_CHANNEL_ID];
    return channelId;
}

+ (NSString *)getDeletechannelName {
    NSString *channelName = [[NSString alloc] init];
    channelName = [[NSUserDefaults standardUserDefaults] objectForKey:DELETE_CHANNEL_NAME];
    return channelName;
}

// Get delete Bot info save
+ (NSString *)getDeletebotId {
    NSString *botId = [[NSString alloc] init];
    botId = [[NSUserDefaults standardUserDefaults] objectForKey:DELETE_BOT_ID];
    return botId;
}

+ (NSString *)getDeletebotName {
    NSString *botName = [[NSString alloc] init];
    botName = [[NSUserDefaults standardUserDefaults] objectForKey:DELETE_BOT_USER_NAME];
    return botName;
}

// Get delete User
+ (NSString *)getDeleteUserId {
    NSString *userId = [[NSString alloc] init];
    userId = [[NSUserDefaults standardUserDefaults] objectForKey:DELETE_USER_ID];
    return userId;
}

// Get Genric app id getting from our server
+ (NSString *)getGenricApplicationappId {
    NSString *appId = [[NSString alloc] init];
    appId = [[NSUserDefaults standardUserDefaults] objectForKey:GENRIC_APP_ID];
    return appId;
}

// Get telegram uid from telegram
+ (NSInteger)getTelegramUid {
    NSInteger uid = [[NSUserDefaults standardUserDefaults] integerForKey:TELEGRAM_UID_TELEGRAM_SERVER];
    return uid;
}

// Get user block status
+ (bool)isUserBlocked {
    bool isBlocked = [[NSUserDefaults standardUserDefaults] boolForKey:USER_BLOCK_STATUS];
    return isBlocked;
}

// ************************************************************* Change *******************************************************

// Get Chatting User Name
+ (NSString *)getUserChatterName {
    NSString *name = [[NSString alloc] init];
    name = [[NSUserDefaults standardUserDefaults] objectForKey:CHAT_USER_NAME];
    return name;
}

// Get Days from install count
+ (int)getDaysFromIntallCount {
    int count = [[NSUserDefaults standardUserDefaults] integerForKey:DAYS_INSATLL_COUNT];
    return count;
}

// Get Daily app launch count
+ (int)getDailyAppLaunchCount {
    int count = [[NSUserDefaults standardUserDefaults] integerForKey:DAYS_APP_LAUNCH_COUNT];
    return count;
}

// Get Current Date
+ (NSString *)getCurrentDate {
    NSString *currentDate = [[NSString alloc] init];
    currentDate = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_DATE_SAVE];
    return currentDate;
}

// Get Appsflyer Refferal name
+ (NSString *)getRefferalNameFromAppsFlyer {
    NSString *refferalName = [[NSString alloc] init];
    refferalName = [[NSUserDefaults standardUserDefaults] objectForKey:REFFRAL_NAME];
    return refferalName;
}

// Get Favorate Recipe id
+ (NSArray *)getFavorateRecipeIdArr {
    NSArray *arrToReturn = [[NSArray alloc] init];
    arrToReturn = [[NSUserDefaults standardUserDefaults] objectForKey:SAVE_FAVORATE_RECIPE_ID];
    return arrToReturn;
}

// Get unread count only displayed cells
+ (int)getUnreadCountDisplayedCells {
    int unreadCount = [[NSUserDefaults standardUserDefaults] integerForKey:SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS];
    return unreadCount;
}

// Get Goals Arr Save before login
+ (NSArray *)getSelectedGoalsArrayBeforeLoginSaved {
    NSArray *arrToReturn = [[NSArray alloc] init];
    NSString *goals = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_SELECTED_GOALS_BEFORE_LOGIN]];
    if (![goals isEqualToString:@""] && ![goals isEqualToString:@"(null)"]) {
        arrToReturn = [goals componentsSeparatedByString:@","];
        return arrToReturn;
    }
    return arrToReturn;
}

// Get Prefrence Arr Save before login
+ (NSArray *)getSelectedPrefrenceArrayBeforeLoginSaved {
    NSArray *arrToReturn = [[NSArray alloc] init];
    NSString *prefrences = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN]];
    if (![prefrences isEqualToString:@""] && ![prefrences isEqualToString:@"(null)"]) {
        arrToReturn = [prefrences componentsSeparatedByString:@","];
        return arrToReturn;
    }
    return arrToReturn;
}

// Get Selected ingredients Like list
+ (NSArray *)getSelectedIngredientsLikeList {
    NSArray *returnArr = [[NSArray alloc] init];
    NSString *ingredients = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_SELECTED_INGREDIENTS_LIST_LIKE]];
    if (![ingredients isEqualToString:@""] && ![ingredients isEqualToString:@"(null)"]) {
        returnArr = [ingredients componentsSeparatedByString:@","];
        return returnArr;
    }
    return returnArr;
}

// Get Selected ingredients DisLike list
+ (NSArray *)getSelectedIngredientsDisLikeList {
    NSArray *returnArr = [[NSArray alloc] init];
    NSString *ingredients = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE]];
    if (![ingredients isEqualToString:@""] && ![ingredients isEqualToString:@"(null)"]) {
        returnArr = [ingredients componentsSeparatedByString:@","];
        return returnArr;
    }
    return returnArr;
}


// Get Selected Health Condition
+ (NSArray *)getSelectedHealthConditionList {
    NSArray *returnArr = [[NSArray alloc] init];
    NSString *health = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_SELECTED_HEALTH_CONDITION]];
    if (![health isEqualToString:@""] && ![health isEqualToString:@"(null)"]) {
        returnArr = [health componentsSeparatedByString:@","];
        return returnArr;
    }
    return returnArr;
}

// Get User created channel favorate smoothies
+ (NSString *)getFavorateSmoothiesChannelId {
    NSString *channelId = @"";
    channelId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL]];
    if ([channelId isEqualToString:@"(null)"]) {
        channelId = @"";
    }
    if ([channelId isEqualToString:@"nil"]) {
        channelId = @"";
    }
    if ([channelId isEqualToString:@"null"]) {
        channelId = @"";
    }
    return channelId;
}

// Get User created User Name Channel
+ (NSString *)getUserNameCreatedChannelId {
    NSString *channelId = @"";
    channelId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_USER_CREATE_USER_NAME_CHANNEL]];
    if ([channelId isEqualToString:@"(null)"]) {
        channelId = @"";
    }
    if ([channelId isEqualToString:@"nil"]) {
        channelId = @"";
    }
    if ([channelId isEqualToString:@"null"]) {
        channelId = @"";
    }
    return channelId;
}

// Get recipe id share in user channel
+ (NSArray *)getRecipeIdShareChannnel {
    NSArray *arrToReturn = [[NSArray alloc] init];
    arrToReturn = [[NSUserDefaults standardUserDefaults] objectForKey:SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE];
    return arrToReturn;
}

// Get User First Time Login Call ViewWillAppear Method On dialogList
+ (bool)isFirstTime {
    bool isFirst = [[NSUserDefaults standardUserDefaults] boolForKey:SAVE_USER_FIRST_TIME_LOGIN];
    return isFirst;
}

// Get Smoothies Changes Like ingredients, health Condition, Goals, Prefrences
+ (bool)isSmoothiesChanges {
    bool isChange = [[NSUserDefaults standardUserDefaults] boolForKey:SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS];
    return isChange;
}

// Only First time hit all api
+ (bool)isHitAllApi {
    bool isFirst = [[NSUserDefaults standardUserDefaults] boolForKey:SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API];
    return isFirst;
}

// Get Campaign name Appsflyer
+ (NSString *)getCampaignName {
    NSString *campaignName = [[NSString alloc] init];
    campaignName = [[NSUserDefaults standardUserDefaults] objectForKey:SAVE_CAMPAIGN_NAME_APPSFLYER];
    return campaignName;
}

// Get group created List from server
+ (NSArray *)getGroupList {
    NSArray *arrToReturn = [[NSArray alloc] init];
    arrToReturn = [[NSUserDefaults standardUserDefaults] objectForKey:SAVE_GROUP_CREATION_LIST];
    return arrToReturn;
}
@end
