//
//  TGSettingListTableViewCell.h
//  Telegraph
//
//  Created by vinove on 13/08/18.
//

#import <UIKit/UIKit.h>

@interface TGSettingListTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIView *lineView;

@end
