//
//  TGMixPanelEventsConstants.m
//  Telegraph
//
//  Created by vinove on 12/20/18.
//

#import "TGMixPanelEventsConstants.h"
#import <Mixpanel/Mixpanel.h>
#import "TGDatabase.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"

@implementation TGMixPanelEventsConstants

NSString* const SPLASH_1_3_BUTTON = @"Splash 1-3 -> Button";
NSString* const SPLASH_1_3_SCREEN = @"Splash 1-3 -> Screen";
NSString* const REGISTRATION_PHONE_FIELD = @"Registration - Phone -> Field";
NSString* const REGISTRATION_CODE_FIELD = @"Registration - Code -> Field";
NSString* const CHATSCHANNELS_CHAT = @"Chats and Channels -> Chat";
NSString* const CHATSCHANNELS_NEW_CHAT = @"Chats and Channels -> New Chat";
NSString* const CHATSCHANNELS_PROFILE_ICON = @"Chats and Channels -> Profile Icon";

NSString* const INVITESCREEN_INVITE_FRIEND = @"Invite Friends -> Invite Friend";
NSString* const INVITESCREEN_SUCCESSFULL_INVITE = @"Invite Friends -> Successful invite";

NSString* const PROFILE_ENTRY = @"Profile -> Entry";

NSString* const SETTING_ENTRY = @"Settings -> Entry";

NSString* const CHAT_SCREEN_SEND_BUTTON = @"Chat Screen -> Send button";

NSString* const BACK_BUTTON = @"Back Button";

NSString* const SPASH = @"Splash";
NSString* const REGISTRATION = @"Registration";
NSString* const COUNTRY_LIST = @"Select Country";
NSString* const OTP = @"OTP";
NSString* const REGISTER_PROFILE = @"Register Profile";
NSString* const CHATTING_SCREEN = @"Chat Screen";
NSString* const PROFILE = @"Profile";
NSString* const SETTING = @"Settings";
NSString* const ACCOUNT = @"Account";
NSString* const FAQ = @"FAQ";
NSString* const FAQ_DETAILS = @"FAQ Details";

NSString* const ABOUT_US = @"About";
NSString* const CONTACT_US = @"Contact Us";
NSString* const NOTIFICATIONS = @"Notifications";
NSString* const INVITE_FRIENDS = @"Invite Friends";
NSString* const CHATTING = @"Chats And Channels";
NSString* const CREATE_GROUP = @"Create Group";
NSString* const CHATTING_INFO = @"Chatting Info";
NSString* const CREATE_CONTACT = @"Create Contact";

NSString* const INGREDEINTS = @"Ingredients";
NSString* const INGREDEINTS_DETAILS = @"Ingredient Detail";
NSString* const FULL_RECIPE = @"Full Recipe";
NSString* const HEALTH_CONDITION = @"Health Conditions";
NSString* const GOALS = @"Goals";
NSString* const PREFRENCE = @"Preferences";
NSString* const TERMS_CONDITION = @"Terms & Conditions";


NSString* const CHANNELS_JOIN = @"Channels -> Join";
NSString* const CHANNELS_MUTE = @"Channels -> Mute";
NSString* const CHANNELS_LEAVE = @"Channels -> Leave";

// Common Events
NSString* const DONE = @"Done";
NSString* const CLICK = @"Click";
NSString* const CLICKED = @"Clicked";
NSString* const RIGHT = @"Right";
NSString* const LEFT = @"Left";
NSString* const SWEEP = @"Sweep";
NSString* const PHONE_NUMBER = @"Phone Number";
NSString* const EXISTINGYES = @"Existing/YES";
NSString* const EXISTINGNO = @"Existing/NO";
NSString* const COUNTRY = @"Country";
NSString* const SUCCESS = @"Success";
NSString* const FAIL = @"Fail";
NSString* const JOIN = @"Join";
NSString* const MUTE = @"Mute";
NSString* const LEAVE = @"Leave";
NSString* const MESSAGE = @"Message";
NSString* const MORE = @"More";
NSString* const LESS = @"Less";
NSString* const NEXT = @"Next";
NSString* const SHARE = @"Share";
NSString* const CHANNEL = @"Channel";
NSString* const FAVORITE = @"Favorite";

NSString* const IMAGE = @"Image";
NSString* const VIDEO = @"Video";
NSString* const AUDIO = @"Audio";
NSString* const TEXT = @"Text";
NSString* const DOC = @"Doc";
NSString* const LOCATION = @"Location";
NSString* const ADMIN_BOT = @"Admin Bot";
NSString* const BOT_ANSWER = @"Bot Answer";
NSString* const CONTACT = @"Contact";
NSString* const STICKER = @"Sticker";

NSString* const DAYSFROMINSTALL = @"Days from install";
NSString* const DAILYINITCOUNT = @"Daily init count";

NSString* const USERNAME = @"User Name";
NSString* const USERID = @"User ID";

NSString* const DAYSINSTALL = @"Days Install";
NSString* const DAILYAPPLAUNCH = @"Daily App Launch";

NSString* const ENTRY  = @"Entry";
NSString* const SCREEN_NAME = @"Screen Name";
NSString* const SCREEN = @"Screen";
NSString* const INVITE_FRIEND = @"Invite Friends";
NSString* const REGISTRATION_TERMS_CONDITION = @"Registration - Phone -> Terms & Condition";
NSString* const SEND_BUTTON = @"Send Button";
NSString* const NEW_CHAT = @"New Chat";
NSString* const SAVE = @"Save";
NSString* const INGREDIENT_LIST = @"Ingredients List";
NSString* const AVOID = @"Avoid";
NSString* const SELECT = @"Select";
NSString* const NA = @"NA";
NSString* const INGREDIENT = @"Ingredient";


// Element Name
NSString* const BUTTON = @"Button";
NSString* const NEW_CHAT_SUCCESSFULLY_CREATED = @"<new chat successfully created>";
NSString* const CAMPAIGN = @"Campaign";

+ (void)trackMixpanelEvents:(NSString *)eventName properties:(NSMutableDictionary *)properties {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    if (properties != nil) {
        [mixpanel track:eventName properties:properties];
    }else {
        [mixpanel track:eventName];
    }
}

+ (NSMutableDictionary *)mixpanelProperties:(NSString *)label withAction:(NSString *)action value:(NSString *)value element:(NSString *)element screenName:(NSString *)screenName{
    NSMutableDictionary *mixPanelDic = [[NSMutableDictionary alloc] init];
    NSLog(@"Mixpanel label is ====>>>%@", label);
    NSLog(@"Mixpanel action is ====>>>%@", action);
    NSLog(@"Mixpanel value is ====>>>%@", value);
    NSString *first = [NSString stringWithFormat:@"%@",label];
    NSString *second = [NSString stringWithFormat:@"%@",action];
    NSString *third = [NSString stringWithFormat:@"%@",value];
    
    TGUser *user = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    if (user != nil) {
        NSString *firstName = @"";
        NSString *lastName = @"";
        
        NSString *userFirstName = [NSString stringWithFormat:@"%@", user.firstName];
        if (![userFirstName isEqualToString:@"(null)"]) {
            firstName = user.firstName;
        }
        
        NSString *userLastName = [NSString stringWithFormat:@"%@", user.lastName];
        if (![userLastName isEqualToString:@"(null)"]) {
            lastName = user.lastName;
        }
        
        NSString *userName = @"";
        if ([userLastName isEqualToString:@"(null)"] && [userFirstName isEqualToString:@"(null)"]) {
            userName = [NSString stringWithFormat:@"%@",user.userName];
        }else {
            userName = [NSString stringWithFormat:@"%@%@",firstName,lastName];
        }
        NSString *userId = [NSString stringWithFormat:@"%d",user.uid];
        [mixPanelDic setObject:userName forKey:USERNAME];
        [mixPanelDic setObject:userId forKey:USERID];
    }
    
    [mixPanelDic setObject:screenName forKey:SCREEN_NAME];
    NSString *dailyLaunchCount = [NSString stringWithFormat:@"%d",[TGFetchSavedPrefrenceInfoGenric getDailyAppLaunchCount]];
    int daysInstallCount = [TGFetchSavedPrefrenceInfoGenric getDaysFromIntallCount];
    NSString *daysCount = [NSString stringWithFormat:@"%d",daysInstallCount];
    [mixPanelDic setObject:dailyLaunchCount forKey:DAILYINITCOUNT];
    [mixPanelDic setObject:daysCount forKey:DAYSFROMINSTALL];
    
    NSString *campaignName = [NSString stringWithFormat:@"%@",[TGFetchSavedPrefrenceInfoGenric getCampaignName]];
    if (![campaignName isEqualToString:@""] && ![campaignName isEqualToString:@"nil"] && ![campaignName isEqualToString:@"(null)"] && ![campaignName isEqualToString:@"<nil>"]) {
        [mixPanelDic setObject:campaignName forKey:@"Campaign"];
    }else {
        [mixPanelDic setObject:@"" forKey:@"Campaign"];
    }
    
    if (![first isEqualToString:@""] && ![first isEqualToString:@"nil"] && ![first isEqualToString:@"(null)"] && ![first isEqualToString:@"<nil>"]) {
        [mixPanelDic setObject:label forKey:@"Label"];
    }
    if (![second isEqualToString:@""] && ![second isEqualToString:@"nil"] && ![second isEqualToString:@"(null)"] && ![second isEqualToString:@"<nil>"]) {
        [mixPanelDic setObject:action forKey:@"Action"];
    }
    if (![third isEqualToString:@""] && ![third isEqualToString:@"nil"] && ![third isEqualToString:@"(null)"] && ![third isEqualToString:@"<nil>"]) {
        [mixPanelDic setObject:value forKey:@"Value"];
    }
    if (![element isEqualToString:@""]) {
        [mixPanelDic setObject:element forKey:@"Element"];
    }
    
    
    return mixPanelDic;
}

+ (NSString *)screenNameWith:(NSString *)screenName back:(NSString *)back {
    NSString *strToReturen = [NSString stringWithFormat:@"%@ -> %@", screenName, back];
    return strToReturen;
}

@end

