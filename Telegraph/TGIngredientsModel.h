//
//  TGIngredientsModel.h
//  Telegraph
//
//  Created by vinove on 1/9/19.
//

#import <Foundation/Foundation.h>

@interface TGIngredientsModel : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger state;
@property (nonatomic, strong) NSString *url;

+ (NSMutableArray *)getIngredientsDetails;
@end
