//
//  TGNutritionGoalsModel.m
//  Telegraph
//
//  Created by vinove on 12/18/18.
//

#import "TGNutritionGoalsModel.h"

@implementation TGNutritionGoalsModel

+ (NSMutableArray *)getNutritionGoalsDetails:(NSArray *)dataArr {
    NSMutableArray *arrToReturn = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i<dataArr.count; i++) {
        TGNutritionGoalsModel *model = [[TGNutritionGoalsModel alloc] init];
        NSDictionary *dic = [dataArr objectAtIndex:i];
        model.name = [dic objectForKey:@"Name"];
        model.image = [dic objectForKey:@"Image"];
        model.is_Selected = false;
        [arrToReturn addObject:model];
    }
    return arrToReturn;
}

@end
