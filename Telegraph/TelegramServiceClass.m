//
//  TelegramServiceClass.m
//  Telegraph
//
//  Created by vinove on 19/07/18.

#define Base_Url_Smoothies "http://app.ezsmoothie.recipes/"//"http://beta.shakensmoothie.recipes/"
#define Salt "​15Gl[b6819qmmm_a858%37065580}s7d8f9a&0sdf​"
#define NutritionFactImageDownload "image_iv5.php?r"

#import "TelegramServiceClass.h"
#import <CommonCrypto/CommonDigest.h>
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGDatabase.h"

@implementation TelegramServiceClass 

// Mark:- Shared instanc
+ (TelegramServiceClass *)shareInstance{
    // Creating shared instance
    static TelegramServiceClass * TGServiceClass = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        TGServiceClass = [[self alloc] init];
    });
    return TGServiceClass;
}

#pragma mark NSURLConnection Delegates -

// ******************************************* Genric API *******************************************


// get app id from server
- (void)getAppIdFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
   // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    NSString *stringURL = @"http://n4.iworklab.com:5000/getAppId?appname=Smoothies";
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// Login and registration api
- (void)AddNewUserOwnServer:(NSString *)firstName last:(NSString *)lastName phoneNo:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/addUser"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    
    NSString *first = @" ";
    NSString *last = @" "; 
    if ([firstName isEqualToString:@"(null)"]) {
        first = @" ";
    }else {
        first = firstName;
    }
    if ([lastName isEqualToString:@"(null)"]) {
        last = @" ";
    }else {
        last = lastName;
    }
    
    NSString *referredBy = [NSString stringWithFormat:@"%@",[TGFetchSavedPrefrenceInfoGenric getRefferalNameFromAppsFlyer]];
    if ([referredBy isEqualToString:@"(null)"]) {
        referredBy = @" ";
    }
    
    NSString *param = [NSString stringWithFormat:@"first_name=%@&last_name=%@&is_activated=%@&app_id=%@&phone=%@&refered_by=%@" , first, last, @"1", appID, phoneNumber, referredBy];
    NSData *postData =[param dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSLog(@"Status code is ====>>>>%@", response);
            NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            block(true, nil, jsonDic);
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

// get all text and theme from server
- (void)getAllTextThemeFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/getPages/%@", appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// get all text and theme from server
- (void)getAllChannelsDetailsFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/getappchannel?appid=%@", appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// get all chatbot details from server
- (void)getAllChatBotsDetailsFromServer:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/getChatbots/%@", appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// GET USER status is user blocked by admin or not.
- (void)getUserStatusBlocked:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/getUserStatus"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *param = [NSString stringWithFormat:@"phone=%@&appid=%@" , phoneNumber, appID];
    NSData *postData =[param dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSLog(@"Status code is ====>>>>%@", response);
            NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            block(true, nil, jsonDic);
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

// GET particular single user channels from server
- (void)getUserChannelsFromServer:(NSString *)phoneNumber :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/userchannel?username=%@&appid=%@", phoneNumber,appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// GET particular single user bots from server
- (void)getUserBotsFromServer:(NSString *)phoneNumber :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/userbot?username=%@&appid=%@", phoneNumber,appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// add channels from particular single user Number
- (void)addUserChannelsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/userchannel"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: phoneNumber, @"username",telegramId, @"telegram_id",appID, @"appid", userName, @"telegram_username", nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

// add bots from particular single user Number
- (void)addUserBotsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/userbot"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: phoneNumber, @"username",telegramId, @"telegram_id",appID, @"appid", userName, @"telegram_username", nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

// remove channels from particular single user Number
- (void)removeUserChannelsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/userchannel?username=%@&telegram_id=%@&appid=%@&userName=%@", phoneNumber, telegramId, appID, userName];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"DELETE";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// remove bots from particular single user Number
- (void)removeUserBotsFromServer:(NSString *)phoneNumber tgId:(NSString *)telegramId telegramUserName:(NSString *)userName :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/userbot?username=%@&telegram_id=%@&appid=%@&userName=%@", phoneNumber, telegramId, appID, userName];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"DELETE";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

- (void)getUserListSaveFromServer:(NSString *)phoneNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/user_unblock?username=%@&appid=%@", phoneNumber,appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

- (void)addUserFromServer:(NSString *)userName witTelegramId:(NSString *)telegramUserId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/user_unblock"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: userName, @"username",telegramUserId, @"telegram_user_id",appID, @"appid",chatUserId, @"other_telegram_id", nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

- (void)removeUserFromOwnServer:(NSString *)userName witTelegramId:(NSString *)telegramUserId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/user_unblock?username=%@&telegram_user_id=%@&appid=%@", userName, telegramUserId, appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"DELETE";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

// ******************************************* String Convert to MD5 *******************************************
- (NSString*)md5HexDigest:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}


// ******************************************* Smoothies API *******************************************

- (void)getFullRecipeDetailsFromServer:(NSString *)recipeId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-hh"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    NSString *token = [[self md5HexDigest:[NSString stringWithFormat:@"%@%s", dateStr,Salt]] stringByAppendingFormat:@"-%@",dateStr];
    

    NSString *param = [NSString stringWithFormat:@"recipe?token=%@&recipe_id=%@", token, recipeId];
    NSString *stringURL = [NSString stringWithFormat:@"%s%@", Base_Url_Smoothies, param];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];


        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

- (void)getIngradiantsDetailsFromServer:(NSString *)ingradiantName :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-hh"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    NSString *token = [[self md5HexDigest:[NSString stringWithFormat:@"%@%s", dateStr,Salt]] stringByAppendingFormat:@"-%@",dateStr];
    
    
    NSString *param = [NSString stringWithFormat:@"ingredient?token=%@&ingredient=%@", token, ingradiantName];
    NSString *stringURL = [NSString stringWithFormat:@"%s%@", Base_Url_Smoothies, param];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

- (NSString *)NutritionFactImageDownloadWithUrl:(NSString *)recipeId {
    NSString *url = [[NSString alloc] init];
    url = [NSString stringWithFormat:@"%s%s=%@",Base_Url_Smoothies,NutritionFactImageDownload,recipeId];
    return url;
}

- (void)getAllGoalsPrefrencesIngredientsHealthConditionInformationFromServerphnNumber:(NSString *)phnNumber :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/user_data_get?username=%@&appid=%@", phnNumber,appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

- (void)saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:(NSString *)userData type:(NSString *)typeData like:(NSString *)likeStr dislike:(NSString *)dislike phnNumber:(NSString *)phnNumber with:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/user_data_get"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSDictionary *mapData;
    if ([typeData isEqualToString:@"ingredients"]) {
        mapData = [[NSDictionary alloc] initWithObjectsAndKeys: phnNumber, @"username",appID, @"appid",userData, @"user_data",typeData, @"type_data",likeStr, @"like",dislike, @"dislike", nil];
    }else {
        mapData = [[NSDictionary alloc] initWithObjectsAndKeys: phnNumber, @"username",appID, @"appid",userData, @"user_data",typeData, @"type_data", nil];
    }
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

- (void)saveUserAllPrefrencestoTheSmoothiesServerchatUserId:(NSString *)chatId :(void (^)(BOOL, NSError *, NSDictionary *))block {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *date = [NSDate date];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-hh"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    NSString *token = [[self md5HexDigest:[NSString stringWithFormat:@"%@%s", dateStr,Salt]] stringByAppendingFormat:@"-%@",dateStr];
    
    
    NSTimeZone* deviceTimeZone_ = [NSTimeZone systemTimeZone];
    CGFloat offset = [deviceTimeZone_ secondsFromGMTForDate:date] / 3600.0;
    NSLog(@"device time offset%f", offset);
    NSString *offsetStr = [NSString stringWithFormat:@"%f",offset];
    
    NSString *healthStr = @"";
    NSString *likeStr = @"";
    NSString *disLikeStr = @"";
    NSString *goals = @"";
    NSString *prefrence = @"";
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsDisLikeList].count > 0) {
        disLikeStr = [[TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsDisLikeList] componentsJoinedByString:@","];
    }
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsLikeList].count > 0) {
        likeStr = [[TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsLikeList] componentsJoinedByString:@","];
    }
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedHealthConditionList].count > 0) {
        healthStr = [[TGFetchSavedPrefrenceInfoGenric getSelectedHealthConditionList] componentsJoinedByString:@","];
    }
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved].count > 0) {
        prefrence = [[TGFetchSavedPrefrenceInfoGenric getSelectedPrefrenceArrayBeforeLoginSaved] componentsJoinedByString:@","];
    }
    if ([TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved].count > 0) {
        goals = [[TGFetchSavedPrefrenceInfoGenric getSelectedGoalsArrayBeforeLoginSaved] componentsJoinedByString:@","];
    }
    
    NSString *goalsPrefrence = @"";
    if ([goals isEqualToString:@""]) {
        goalsPrefrence = [NSString stringWithFormat:@"%@",prefrence];
    }
    if ([prefrence isEqualToString:@""]) {
        goalsPrefrence = [NSString stringWithFormat:@"%@",goals];
    }
    if (![goals isEqualToString:@""] && ![prefrence isEqualToString:@""]) {
        goalsPrefrence = @"";
        goalsPrefrence = [NSString stringWithFormat:@"%@,%@",goals,prefrence];
    }
    
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"body_sys", @"", @"chat_id", chatId, @"chat_user_id", disLikeStr, @"dislike_ingredients",healthStr, @"health_cond",likeStr, @"like_ingredients", @"*", @"meter_energy", @"*", @"meter_fats",@"*", @"meter_gl_Index",@"*", @"meter_micr",@"*", @"meter_protein",@"*", @"meter_salt",goalsPrefrence, @"tags",offsetStr, @"time_offset",token, @"token", nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%spreferences?",Base_Url_Smoothies]];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSLog(@"Status code is ====>>>>%@", response);
            NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            block(true, nil, jsonDic);
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

// Save contact us information to server
- (void)saveContactUsInformationFromServer:(NSString *)userName name:(NSString *)name emailId:(NSString *)emailId message:(NSString *)message with:(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/user_contact_us"];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: userName, @"username",name, @"name",emailId, @"email",message, @"message", nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];

    [postDataTask resume];
}

- (void)addGroupFromServer:(NSString *)groupId withUsersArray:(NSArray *)userArr :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSError *error;
    NSURL *url = [NSURL URLWithString:@"http://n4.iworklab.com:5000/usergroupunblock"];
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: groupId, @"channel_id",appID, @"appid",userArr, @"telegram_id", nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else if (httpResp.statusCode == 409){
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                block(true, nil, json);
            }else {
                block(false, nil, nil);
            }
        }else {
            block(false, nil, nil);
        }
    }];
    
    [postDataTask resume];
}

- (void)getGroupDetailsFromServer:(NSString *)userId :(void (^)(BOOL success, NSError *error, NSDictionary *dic))block {
    NSString *appID = [TGFetchSavedPrefrenceInfoGenric getGenricApplicationappId];
    NSString *stringURL = [NSString stringWithFormat:@"http://n4.iworklab.com:5000/usergroupunblock?telegram_user_id=%@&appid=%@", userId,appID];
    NSString* webStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"Basic YWRtaW46Y2hhdGJvdA==" forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSError *error = nil;
    if (!error) {
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                if (httpResp.statusCode == 200) {
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    block(true, nil, json);
                }else {
                    block(false, nil, nil);
                }
            }else {
                block(false, nil, nil);
            }
        }];
        
        
        [downloadTask resume];
    }else {
        NSLog(@"No data found");
    }
}

@end
