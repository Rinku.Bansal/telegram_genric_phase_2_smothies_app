//
//  TGSmoothiesGoalsCollectionViewCell.h
//  Telegraph
//
//  Created by vinove on 12/17/18.
//

#import <UIKit/UIKit.h>
@protocol SelectMultipleGoalsProtocol <NSObject>
@optional

- (void)selectionClickedAtIndex:(NSInteger)index;

@end

@interface TGSmoothiesGoalsCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *btnGoals;
@property (nonatomic, strong) UILabel *lblGoals;
@property (nonatomic, assign) NSInteger cellIndex;
@property (nonatomic, assign) id <SelectMultipleGoalsProtocol> selectDelegate;

@end
