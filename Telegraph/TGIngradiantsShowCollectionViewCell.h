//
//  TGIngradiantsShowCollectionViewCell.h
//  Telegraph
//
//  Created by Rinku on 31/12/18.
//

#import <UIKit/UIKit.h>
@protocol selectIngredientsImageProtocol <NSObject>
@optional

- (void)selectionClickedAtIndex:(NSUInteger)index;

@end

@interface TGIngradiantsShowCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ingradiantImageView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscription;
@property (nonatomic, assign) NSUInteger cellIndex;
@property (nonatomic, assign) id <selectIngredientsImageProtocol> selectDelegate;
@property (weak, nonatomic) IBOutlet UILabel *lblFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblThird;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upperViewWidthConstraints;

- (IBAction)btnAmezonClick:(id)sender;
@end
