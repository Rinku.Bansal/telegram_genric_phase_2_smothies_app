//
//  TGMixPanelEventsConstants.h
//  Telegraph
//
//  Created by vinove on 12/20/18.
//

#import <Foundation/Foundation.h>

@interface TGMixPanelEventsConstants : NSObject

/// Mixpanel Events
extern NSString* const SPLASH_1_3_BUTTON;
extern NSString* const SPLASH_1_3_SCREEN;
extern NSString* const REGISTRATION_PHONE_FIELD;
extern NSString* const REGISTRATION_CODE_FIELD;
extern NSString* const CHATSCHANNELS_CHAT;
extern NSString* const CHATSCHANNELS_NEW_CHAT;
extern NSString* const CHATSCHANNELS_PROFILE_ICON;

extern NSString* const INVITESCREEN_INVITE_FRIEND;
extern NSString* const INVITESCREEN_SUCCESSFULL_INVITE;

extern NSString* const PROFILE_ENTRY;

extern NSString* const SETTING_ENTRY;

extern NSString* const CHAT_SCREEN_SEND_BUTTON;

extern NSString* const BACK_BUTTON;

extern NSString* const SPASH;
extern NSString* const REGISTRATION;
extern NSString* const COUNTRY_LIST;
extern NSString* const OTP;
extern NSString* const REGISTER_PROFILE;
extern NSString* const CHATTING_SCREEN;
extern NSString* const PROFILE;
extern NSString* const SETTING;
extern NSString* const ACCOUNT;
extern NSString* const FAQ;
extern NSString* const FAQ_DETAILS;

extern NSString* const ABOUT_US;
extern NSString* const CONTACT_US;
extern NSString* const NOTIFICATIONS;
extern NSString* const INVITE_FRIENDS;
extern NSString* const CHATTING;
extern NSString* const CREATE_GROUP;
extern NSString* const CHATTING_INFO;
extern NSString* const CREATE_CONTACT;

extern NSString* const INGREDEINTS;
extern NSString* const INGREDEINTS_DETAILS;
extern NSString* const FULL_RECIPE;
extern NSString* const HEALTH_CONDITION;
extern NSString* const GOALS;
extern NSString* const PREFRENCE;
extern NSString* const TERMS_CONDITION;

// Mixpanel Properties
extern NSString* const CHANNELS_JOIN;
extern NSString* const CHANNELS_MUTE;
extern NSString* const CHANNELS_LEAVE;

// Common Events
extern NSString* const DONE;
extern NSString* const CLICK;
extern NSString* const CLICKED;
extern NSString* const RIGHT;
extern NSString* const LEFT;
extern NSString* const SWEEP;
extern NSString* const PHONE_NUMBER;
extern NSString* const EXISTINGYES;
extern NSString* const EXISTINGNO;
extern NSString* const COUNTRY;
extern NSString* const SUCCESS;
extern NSString* const FAIL;
extern NSString* const JOIN;
extern NSString* const MUTE;
extern NSString* const LEAVE;
extern NSString* const MESSAGE;
extern NSString* const MORE;
extern NSString* const LESS;
extern NSString* const NEXT;
extern NSString* const SHARE;
extern NSString* const CHANNEL;
extern NSString* const FAVORITE;

extern NSString* const IMAGE;
extern NSString* const VIDEO;
extern NSString* const AUDIO;
extern NSString* const TEXT;
extern NSString* const DOC;
extern NSString* const LOCATION;
extern NSString* const ADMIN_BOT;
extern NSString* const BOT_ANSWER;
extern NSString* const CONTACT;
extern NSString* const STICKER;
extern NSString* const USERNAME;
extern NSString* const USERID;

extern NSString* const DAYSFROMINSTALL;
extern NSString* const DAILYINITCOUNT;

extern NSString* const DAYSINSTALL;
extern NSString* const DAILYAPPLAUNCH;

// Element Name
extern NSString* const BUTTON;
extern NSString* const NEW_CHAT_SUCCESSFULLY_CREATED;

extern NSString* const CAMPAIGN;
extern NSString* const ENTRY;
extern NSString* const SCREEN_NAME;
extern NSString* const SCREEN;
extern NSString* const INVITE_FRIEND;
extern NSString* const REGISTRATION_TERMS_CONDITION;
extern NSString* const SEND_BUTTON;
extern NSString* const NEW_CHAT;
extern NSString* const SAVE;
extern NSString* const INGREDIENT_LIST;
extern NSString* const AVOID;
extern NSString* const SELECT;
extern NSString* const NA;
extern NSString* const INGREDIENT;



+ (void)trackMixpanelEvents:(NSString *)eventName properties:(NSMutableDictionary *)properties;
+ (NSMutableDictionary *)mixpanelProperties:(NSString *)label withAction:(NSString *)action value:(NSString *)value element:(NSString *)element screenName:(NSString *)screenName;
+ (NSString *)screenNameWith:(NSString *)screenName back:(NSString *)back;


@end

