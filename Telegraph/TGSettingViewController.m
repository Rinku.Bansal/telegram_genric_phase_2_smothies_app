//
//  TGSettingViewController.m
//  Telegraph
//
//  Created by vinove on 13/08/18.
//

#import "TGSettingViewController.h"
#import "TGSettingListTableViewCell.h"
#import "TGAccountSettingsController.h"
#import "TGContactsController.h"
#import "TGNotificationSettingsController.h"
#import "TGContactUsViewController.h"
#import "TGAboutUsViewController.h"
#import "TGFAQViewController.h"
#import "TGEditProfileController.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"

@interface TGSettingViewController ()<UITableViewDelegate, UITableViewDataSource>{
    UITableView *settingTableView;
    NSArray *listArr;
    
}

@end

@implementation TGSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [TGMixPanelEventsConstants trackMixpanelEvents:SETTING properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:SETTING]];

    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"title" withDefaultStr:TGLocalized(@"Setting_Header_Title")];//@"Settings";
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    settingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 40, self.view.frame.size.width, self.view.frame.size.height-40)];
    settingTableView.delegate = self;
    settingTableView.dataSource = self;
    settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    settingTableView.backgroundColor = TGThemeColor();
    settingTableView.rowHeight = 50;
    [self.view addSubview:settingTableView];
    
    self.view.backgroundColor = TGThemeColor();
    listArr = [[NSArray alloc] initWithObjects:[[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_account_text" withDefaultStr:TGLocalized(@"Account_Header_Title")],[[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_notifications_text"withDefaultStr:TGLocalized(@"Notification_Header_Title")], @"", [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_invite_friends_text" withDefaultStr:TGLocalized(@"Invite_Friends_Header_Title")], @"", [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_about_text" withDefaultStr:TGLocalized(@"About_Header_Title")],[[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_contact_us_text" withDefaultStr:TGLocalized(@"Contact_Us_Header_Title")],[[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"settings" withInfoParam:@"button_faq_text" withDefaultStr:TGLocalized(@"FAQ_Header_Title")], nil];
    [settingTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:SETTING properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:SETTING]];
    [self.navigationController popViewControllerAnimated:true];
}

- (NSArray *)getColorArrToShowOnTable{
    UIColor *color1 = [UIColor colorWithRed:241.0/255.0 green:154.0/255.0 blue:55.0/255.0 alpha:1.0];
    UIColor *color2 = [UIColor colorWithRed:174.0/255.0 green:42.0/255.0 blue:33.0/255.0 alpha:1.0];
    UIColor *color3 = TGThemeColor();
    UIColor *color4 = [UIColor colorWithRed:52.0/255.0 green:120.0/255.0 blue:246.0/255.0 alpha:1.0];
    UIColor *color5 = TGThemeColor();
    UIColor *color6 = [UIColor colorWithRed:118.0/255.0 green:214.0/255.0 blue:114.0/255.0 alpha:1.0];
    UIColor *color7 = [UIColor colorWithRed:241.0/255.0 green:154.0/255.0 blue:55.0/255.0 alpha:1.0];
    UIColor *color8 = [UIColor colorWithRed:247.0/255.0 green:206.0/255.0 blue:70.0/255.0 alpha:1.0];

    NSArray *arrToReturn = [[NSArray alloc] initWithObjects:color1, color2, color3, color4, color5, color6, color7, color8, nil];
    return arrToReturn;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *settingIdentifier = @"TGSettingListTableViewCell";
    TGSettingListTableViewCell *cell = (TGSettingListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:settingIdentifier];
    if (cell == nil) {
        cell = [[TGSettingListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:settingIdentifier];
        NSString *nameLabel = [NSString stringWithFormat:@"%@",[listArr objectAtIndex:indexPath.row]];
        cell.nameLabel.text = nameLabel;
        if ([nameLabel isEqualToString:@""]) {
            [cell.contentImageView setHidden:true];
            [cell.arrowImageView setHidden:true];
            [cell.lineView setHidden:true];
        }else{
            [cell.contentImageView setHidden:false];
            [cell.arrowImageView setHidden:false];
            [cell.lineView setHidden:false];
        }
    }
    int colorIndex = (int)(indexPath.row%[self getColorArrToShowOnTable].count);
    cell.contentImageView.backgroundColor = (UIColor *)[[self getColorArrToShowOnTable] objectAtIndex:colorIndex];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = TGThemeColor();
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"Account" forKey:@"TypeController"];
        TGEditProfileController *controller = [[TGEditProfileController alloc] init];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:ACCOUNT element:CLICK screenName:SETTING]];
        [self.navigationController pushViewController:controller animated:true];
        
    }else if (indexPath.row == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:@"Notification" forKey:@"TypeController"];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:NOTIFICATIONS element:@"" screenName:SETTING]];
        [self.navigationController pushViewController:[[TGNotificationSettingsController alloc] init] animated:true];
        
    }else if (indexPath.row == 3) {
        [[NSUserDefaults standardUserDefaults] setObject:@"Invite Friends" forKey:@"TypeController"];
        TGContactsController *controller = [[TGContactsController alloc] initWithContactsMode:TGContactsModeModalInvite | TGContactsModeSearchDisabled | TGContactsModeRegistered | TGContactsModeSelectModal | TGContactsModeShare];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:INVITE_FRIENDS element:@"" screenName:SETTING]];
        controller.customTitle = @"Invite Friends";
        [self.navigationController pushViewController:controller animated:true];
    }else if (indexPath.row == 5) {
        [self.navigationController pushViewController:[[TGAboutUsViewController alloc] init] animated:true];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:ABOUT_US element:@"" screenName:SETTING]];
        
        
    }else if (indexPath.row == 6) {
        TGContactUsViewController *controller = [[TGContactUsViewController alloc] initWithNibName:@"TGContactUsViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:true];
        //[self.navigationController pushViewController:[[TGContactUsViewController alloc] init] animated:true];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:CONTACT_US element:@"" screenName:SETTING]];
        
        
    }else if (indexPath.row == 7) {
        TGFAQViewController *controller = [[TGFAQViewController alloc] initWithNibName:@"TGFAQViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:true];
        [TGMixPanelEventsConstants trackMixpanelEvents:SETTING_ENTRY properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:FAQ element:@"" screenName:SETTING]];
        
        
        //        NSString *faqUrl = TGLocalized(@"Settings.FAQ_URL");
        //        if (faqUrl.length == 0)
        //            faqUrl = @"http://telegram.org/faq#general";
        //
        //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:faqUrl]];
    }else {
        
    }
}

@end
