//
//  TGSmoothiesNutritionPrefrenceViewController.h
//  Telegraph
//
//  Created by vinove on 12/17/18.
//

#import <UIKit/UIKit.h>
#import <LegacyComponents/TGViewController.h>
#import <LegacyComponents/TGAnimationUtils.h>
#import "RMIntroViewController.h"
#import "TGNutritionGoalsModel.h"
#import "TGSmoothiesGoalsCollectionViewCell.h"

typedef enum {
    InchSP35 = 0,
    InchSP4 = 1,
    InchSP47 = 2,
    InchSP55 = 3,
    iPadSP = 4,
    iPadProSP = 5
} DeviceScreenSizePrefrence;

@interface TGSmoothiesNutritionPrefrenceViewController : TGViewController
{
    UIButton *_startButton;
    
}
@property (nonatomic, assign) BOOL is_Login;
@end
