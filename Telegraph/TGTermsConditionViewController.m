//
//  TGTermsConditionViewController.m
//  Telegraph
//
//  Created by vinove on 2/13/19.
//

#import "TGTermsConditionViewController.h"
#import <LegacyComponents/TGProgressWindow.h>
#import "TGMixPanelEventsConstants.h"

@interface TGTermsConditionViewController (){
    
    __weak IBOutlet UIWebView *termsConditionWebView;
    UIView *containerView;

}
@property (nonatomic, strong) TGProgressWindow *progressWindow;

@end

@implementation TGTermsConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGTermsConditionViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    
    self.titleText = TGLocalized(@"Login_Terms_and_Condition");//@"Ingredients";
    [TGMixPanelEventsConstants trackMixpanelEvents:TERMS_CONDITION properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:TERMS_CONDITION]];

    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    
    NSURL *url = [NSURL URLWithString:@"http://app.ezsmoothie.recipes/Terms.html"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [termsConditionWebView setScalesPageToFit:YES];
    [termsConditionWebView loadRequest:request];
    [_progressWindow show:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:TERMS_CONDITION properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:TERMS_CONDITION]];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_progressWindow show:false];
}


@end
