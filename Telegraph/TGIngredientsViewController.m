//
//  TGIngredientsViewController.m
//  Telegraph
//
//  Created by vinove on 1/9/19.
//

#import "TGIngredientsViewController.h"
#import "TGIngredientsModel.h"
#import "TGIngredientsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "TGIngredientsDetailsViewController.h"
#import "TelegramServiceClass.h"
#import "TGSavedPrefrence.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGUser+Telegraph.h"
#import "TGDatabase.h"
#import "TGMixPanelEventsConstants.h"

@interface TGIngredientsViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, changeMultipleIngredientsStateProtocol>{
    
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UITableView *ingredientsTableView;
    
    UIView *containerView;
    NSMutableArray *ingredientsArr;
    NSMutableArray *searchArray;
}

@end

@implementation TGIngredientsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGIngredientsViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    
    self.titleText = TGLocalized(@"Ingredients_Header_Title");//@"Ingredients";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    [self.navigationController.interactivePopGestureRecognizer setEnabled:false];
    [TGMixPanelEventsConstants trackMixpanelEvents:INGREDIENT_LIST properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:INGREDIENT_LIST]];
    searchBar.placeholder = TGLocalized(@"Ingredients_Search_For_Ingredients");
    ingredientsArr = [[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    ingredientsArr = [TGIngredientsModel getIngredientsDetails];
    searchArray = [TGIngredientsModel getIngredientsDetails];
    [self displaySaveIngredients];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)displaySaveIngredients {
    NSArray *likeArr = [TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsLikeList];
    for (NSUInteger i = 0; i < likeArr.count; i++) {
        NSString *dataStr = [NSString stringWithFormat:@"%@", [likeArr objectAtIndex:i]];
        for (NSUInteger j = 0; j<ingredientsArr.count; j++) {
            TGIngredientsModel *model = [ingredientsArr objectAtIndex:j];
            if ([model.name isEqualToString:dataStr]) {
                model.state = 2;
            }
        }
    }
    NSArray *disLikeArr = [TGFetchSavedPrefrenceInfoGenric getSelectedIngredientsDisLikeList];
    for (NSUInteger i = 0; i < disLikeArr.count; i++) {
        NSString *dataStr = [NSString stringWithFormat:@"%@", [disLikeArr objectAtIndex:i]];
        for (NSUInteger j = 0; j<ingredientsArr.count; j++) {
            TGIngredientsModel *model = [ingredientsArr objectAtIndex:j];
            if ([model.name isEqualToString:dataStr]) {
                model.state = 0;
            }
        }
    }
    
    [ingredientsTableView reloadData];
}

- (void)ButtonBackClicked {
    NSMutableArray *likeArr = [[NSMutableArray alloc] init];
    NSMutableArray *disLikeArr = [[NSMutableArray alloc] init];
    for (NSUInteger j = 0; j<ingredientsArr.count; j++) {
        TGIngredientsModel *model = [ingredientsArr objectAtIndex:j];
        if (model.state == 0) {
            [disLikeArr addObject:model.name];
        }
        if (model.state == 2) {
            [likeArr addObject:model.name];
        }
    }
    NSString *likeStr = [likeArr componentsJoinedByString:@","];
    NSString *unLikeStr = [disLikeArr componentsJoinedByString:@","];
    [TGSavedPrefrence saveSelectedIngredientsList:likeStr dislike:unLikeStr];
    [self.navigationController popViewControllerAnimated:true];
    [[TelegramServiceClass shareInstance] saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:@"" type:@"ingredients" like:likeStr dislike:unLikeStr phnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] with:^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"data dic is ====>>>%@",dic);
        }else {
            NSLog(@"error is ====>>>%@",error);
        }
    }];
    [TGMixPanelEventsConstants trackMixpanelEvents:INGREDEINTS properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:INGREDEINTS]];

}

- (void)saveAllPrefrencesListFromSmoothiesServer {
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    [[TelegramServiceClass shareInstance] saveUserAllPrefrencestoTheSmoothiesServerchatUserId:chatUserId :^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"prefrences dic is ====>>>>>%@", dic);
        }else {
            NSLog(@"error is ====>>>>%@",error);
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ingredientsArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TGIngredientsTableViewCell *cell = (TGIngredientsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGIngredientsTableViewCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGIngredientsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    TGIngredientsModel *model = [ingredientsArr objectAtIndex:indexPath.row];
    cell.lblIngredientsName.text = model.name;
    [cell.ingredientsImageView sd_setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.ingredientsImageView.layer.cornerRadius = cell.ingredientsImageView.frame.size.width/2;
    cell.ingredientsImageView.layer.masksToBounds = true;

    if (model.state == 0) {
        cell.stepperSlider.tintColor = [UIColor colorWithRed:190.0/255.0 green:29.0/255.0 blue:38.0/255.0 alpha:1];
        [cell.stepperSlider setIndex:0];
        cell.stepperSlider.trackColor = [UIColor colorWithRed:190.0/255.0 green:29.0/255.0 blue:38.0/255.0 alpha:1];
        [cell.likeDislikeView setHidden:true];
        [cell.btnLike setHidden:true];
        [cell.btnDislike setHidden:true];
    }
    if (model.state == 1) {
        cell.stepperSlider.tintColor = TGThemeColor();
        [cell.stepperSlider setIndex:1];
        [cell.likeDislikeView setHidden:false];
        [cell.btnLike setHidden:false];
        [cell.btnDislike setHidden:false];

    }
    if (model.state == 2) {
        cell.stepperSlider.tintColor = [UIColor colorWithRed:43.0/255.0 green:195.0/255.0 blue:42.0/255.0 alpha:1];
        [cell.stepperSlider setIndex:2];
        [cell.likeDislikeView setHidden:true];
        [cell.btnLike setHidden:true];
        [cell.btnDislike setHidden:true];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellIndex = indexPath.row;
    cell.selectDelegate = self;
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 568) {
        cell.stepperSlider.sliderCircleImage = [UIImage imageNamed:@"ingredient_handle568.png"];
    }else if (max == 667) {
        cell.stepperSlider.sliderCircleImage = [UIImage imageNamed:@"ingredient_handle568.png"];
    }else if (max == 750) {
        cell.stepperSlider.sliderCircleImage = [UIImage imageNamed:@"ingredient_handle568.png"];
    }else {
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TGIngredientsModel *model = [ingredientsArr objectAtIndex:indexPath.row];
    TGIngredientsDetailsViewController *controller = [[TGIngredientsDetailsViewController alloc] initWithNibName:@"TGIngredientsDetailsViewController" bundle:nil];
    controller.ingredientsName = model.name;
    [self.navigationController pushViewController:controller animated:true];
}

// tableview stepper delegate method
- (void)selectionClickedAtIndex:(NSInteger)index stepperIndex:(NSInteger)stepperIndex {
    [TGSavedPrefrence saveChangeInSmoothiesBot:YES];
    TGIngredientsModel *model = [ingredientsArr objectAtIndex:index];
    model.state = stepperIndex;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexpath, nil];
    [ingredientsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
    if (model.state == 0) {
        [self addMixpanelEvents:model.name action:AVOID];
    }else if (model.state == 1) {
        [self addMixpanelEvents:model.name action:NA];
    }else {
        [self addMixpanelEvents:model.name action:SELECT];
    }
}

- (void)likeSelectionAtIndex:(NSInteger)index {
    [TGSavedPrefrence saveChangeInSmoothiesBot:YES];
    TGIngredientsModel *model = [ingredientsArr objectAtIndex:index];
    model.state = 2;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexpath, nil];
    [ingredientsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self addMixpanelEvents:model.name action:SELECT];
}

- (void)dislikeSelectionAtIndex:(NSInteger)index {
    [TGSavedPrefrence saveChangeInSmoothiesBot:YES];
    TGIngredientsModel *model = [ingredientsArr objectAtIndex:index];
    model.state = 0;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexpath, nil];
    [ingredientsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self addMixpanelEvents:model.name action:AVOID];

}

- (void)addMixpanelEvents:(NSString *)label action:(NSString *)action {
    [TGMixPanelEventsConstants trackMixpanelEvents:INGREDIENT_LIST properties:[TGMixPanelEventsConstants mixpanelProperties:label withAction:action value:@"" element:INGREDIENT screenName:INGREDIENT_LIST]];
}

#pragma Mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] == 0) {
        ingredientsArr = [[NSMutableArray alloc]init];
        ingredientsArr = [searchArray mutableCopy];
        [ingredientsTableView reloadData];
        [searchBar resignFirstResponder];
        return;
    }
    ingredientsArr = [[NSMutableArray alloc]init];
    for (NSUInteger i=0; i<searchArray.count; i++) {
        TGIngredientsModel *model = [searchArray objectAtIndex:i];
        NSComparisonResult result = [model.name compare:searchText options:NSCaseInsensitiveSearch range:NSMakeRange(0, searchText.length) locale:nil];
        if (result == NSOrderedSame) {
            [ingredientsArr addObject:model];
        }
        
    }
    [ingredientsTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


@end
