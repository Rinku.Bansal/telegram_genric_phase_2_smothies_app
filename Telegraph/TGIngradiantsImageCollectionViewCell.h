//
//  TGIngradiantsImageCollectionViewCell.h
//  Telegraph
//
//  Created by Rinku on 31/12/18.
//

#import <UIKit/UIKit.h>

@interface TGIngradiantsImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ingradiantsImageView;

@end
