//
//  TGRecipiesViewController.h
//  Telegraph
//
//  Created by vinove on 12/19/18.
//

#import <UIKit/UIKit.h>

@interface TGRecipiesViewController : UIViewController
@property (nonatomic, strong)NSString *recipeId;
@property (nonatomic, strong)NSString *urlStr;
@end
