//
//  TGLoginPopUp.h
//  Telegraph
//
//  Created by Rinku on 19/02/19.
//

#import <UIKit/UIKit.h>

@protocol LoginPopUpProtocol
@optional
- (void)clickOkButton;
- (void)dismissKeyboardWhenTap;
@end
@interface TGLoginPopUp : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscription;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (nonatomic, weak) id <LoginPopUpProtocol> delegate;

@end
