#import "TGSelectContactController.h"

#import <LegacyComponents/LegacyComponents.h>

#import "TGAppDelegate.h"
#import "TGRootController.h"

#import "TGInterfaceManager.h"

#import <LegacyComponents/SGraphObjectNode.h>

#import "TGMessage+Telegraph.h"

#import "TGDatabase.h"
#import "TGTelegraph.h"

#import <LegacyComponents/TGProgressWindow.h>

#import "TGCreateGroupController.h"

#import "TGCustomAlertView.h"
#import "TGApplicationFeatures.h"

#import "TGChannelManagementSignals.h"

#import "TGTelegramNetworking.h"

#import "TGGroupInfoContactListCreateLinkCell.h"

#import "TGPresentation.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"

@interface TGSelectContactController ()
{
    UIView *_titleContainer;
    UILabel *_titleLabel;
    UILabel *_counterLabel;
    UIView *addMemberContainerView;

    int _displayUserCountLimit;
}

@property (nonatomic, strong) TGCreateGroupController *createGroupController;

@property (nonatomic) bool createEncrypted;
@property (nonatomic) bool createBroadcast;
@property (nonatomic) bool createChannel;
@property (nonatomic) bool inviteToChannel;
@property (nonatomic) bool call;

@property (nonatomic, strong) TGProgressWindow *progressWindow;

@property (nonatomic) TGUser *currentEncryptedUser;

@end

@implementation TGSelectContactController

- (id)initWithCreateGroup:(bool)createGroup createEncrypted:(bool)createEncrypted createBroadcast:(bool)createBroadcast createChannel:(bool)createChannel inviteToChannel:(bool)inviteToChannel showLink:(bool)showLink
{
    return [self initWithCreateGroup:createGroup createEncrypted:createEncrypted createBroadcast:createBroadcast createChannel:createChannel inviteToChannel:inviteToChannel showLink:showLink call:false];
}

- (id)initWithCreateGroup:(bool)createGroup createEncrypted:(bool)createEncrypted createBroadcast:(bool)createBroadcast createChannel:(bool)createChannel inviteToChannel:(bool)inviteToChannel showLink:(bool)showLink call:(bool)call
{
    int contactsMode = TGContactsModeRegistered;
    if (createEncrypted)
    {
        _createEncrypted = true;
    }
    else
    {
        _createBroadcast = createBroadcast;
        _createChannel = createChannel;
        _inviteToChannel = inviteToChannel;
        _call = call;
        
        if (createGroup)
            contactsMode |= TGContactsModeCompose | TGContactsModeSearchGlobal;
        else if (createChannel || inviteToChannel) {
            contactsMode |= (TGContactsModeCompose | TGContactsModeSearchGlobal);
            if (showLink) {
                contactsMode |= TGContactsModeCreateGroupLink | TGContactsModeManualFirstSection;
            }
        }
        else if (!call)
        {
            contactsMode |= TGContactsModeCreateGroupOption;
        }
        
        if (call)
            contactsMode |= TGContactsModeCalls;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            [self setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:TGLocalized(@"Common.Close") style:UIBarButtonItemStylePlain target:self action:@selector(closePressed)]];
        }
    }
    
    self = [super initWithContactsMode:contactsMode];
    if (self)
    {
        self.presentation = TGPresentation.current;
        if (createChannel || inviteToChannel) {
            self.usersSelectedLimit = 0;
            self.composePlaceholder = TGLocalized(@"Compose.ChannelTokenListPlaceholder");
        } else {
            self.usersSelectedLimit = 199;
            
            NSData *data = [TGDatabaseInstance() customProperty:@"maxChatParticipants"];
            if (data.length >= 4)
            {
                int32_t maxChatParticipants = 0;
                [data getBytes:&maxChatParticipants length:4];
                if (maxChatParticipants == 0)
                    self.usersSelectedLimit = 99;
                else
                    self.usersSelectedLimit = MAX(0, maxChatParticipants - 1);
            }
            
#if TARGET_IPHONE_SIMULATOR
            //self.usersSelectedLimit = 10;
#endif
            
            if (createEncrypted || call) {
                self.ignoreBots = true;
            }
            
            _displayUserCountLimit = self.usersSelectedLimit + 1;
            
            data = [TGDatabaseInstance() customProperty:@"maxChannelGroupMembers"];
            if (data.length >= 4)
            {
                int32_t maxChannelGroupMembers = 0;
                [data getBytes:&maxChannelGroupMembers length:4];
                if (maxChannelGroupMembers != 0) {
                    _displayUserCountLimit = MAX(_displayUserCountLimit, maxChannelGroupMembers);
                }
            }
            
            self.usersSelectedLimitAlert = TGLocalized(@"CreateGroup.SoftUserLimitAlert");
            
            self.composePlaceholder = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"create_group" withInfoParam:@"subtitle1" withDefaultStr:TGLocalized(@"Create_Group_Who_Would_You_Like_To_Message")];//@"Who would you like to message?";//TGLocalized(@"Compose.TokenListPlaceholder");
        }
    }
    return self;
}

- (void)closePressed
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [TGAppDelegateInstance.rootController clearContentControllers];
    }
}

- (void)backPressed
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)channelNextPressed {
    NSArray *users = [self selectedComposeUsers];
    if (users.count == 0) {
        [[TGInterfaceManager instance] navigateToConversationWithId:_channelConversation.conversationId conversation:_channelConversation];
    } else {
        TGProgressWindow *progressWindow = [[TGProgressWindow alloc] init];
        [progressWindow show:true];
        
        [[[[TGChannelManagementSignals inviteUsers:_channelConversation.conversationId accessHash:_channelConversation.accessHash users:users] deliverOn:[SQueue mainQueue]] onDispose:^{
            TGDispatchOnMainThread(^{
                [progressWindow dismiss:true];
                
                TGConversation *conversation = [TGDatabaseInstance() loadChannels:@[@(_channelConversation.conversationId)]][@(_channelConversation.conversationId)];
                
                [[TGInterfaceManager instance] navigateToConversationWithId:conversation.conversationId conversation:conversation];
            });
        }] startWithNext:nil completed:nil];
    }
}

- (void)cancelPressed {
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)inviteChannelNextPressed {
    NSArray *users = [self selectedComposeUsers];
    if (users.count == 0) {
        [self cancelPressed];
    } else {
        TGProgressWindow *progressWindow = [[TGProgressWindow alloc] init];
        [progressWindow show:true];

        TGConversation *conversation = _channelConversation;
        __weak TGSelectContactController *weakSelf = self;
        [[[[[[TGChannelManagementSignals inviteUsers:_channelConversation.conversationId accessHash:_channelConversation.accessHash users:users] onCompletion:^ {
            [TGDatabaseInstance() updateChannelCachedData:conversation.conversationId block:^TGCachedConversationData *(TGCachedConversationData *data) {
                if (data == nil) {
                    data = [[TGCachedConversationData alloc] init];
                }
                
                NSMutableArray *userIds = [[NSMutableArray alloc] init];
                for (TGUser *user in users) {
                    [userIds addObject:@(user.uid)];
                }
                
                return [data addMembers:userIds timestamp:(int32_t)[[TGTelegramNetworking instance] approximateRemoteTime]];
            }];
        }] deliverOn:[SQueue mainQueue]] onDispose:^{
            TGDispatchOnMainThread(^{
                [progressWindow dismiss:true];
            });
        }] onCompletion:^{
            __strong TGSelectContactController *strongSelf = weakSelf;
            if (strongSelf != nil) {
                if (strongSelf->_onChannelMembersInvited) {
                    strongSelf->_onChannelMembersInvited(users);
                }
                [strongSelf cancelPressed];
            }
        }] startWithNext:nil error:^(id error) {
            NSString *errorType = [[TGTelegramNetworking instance] extractNetworkErrorType:error];
            NSString *errorText = TGLocalized(@"Profile.CreateEncryptedChatError");
            if ([errorType isEqual:@"USER_BLOCKED"]) {
                errorText = _channelConversation.isChannelGroup ? TGLocalized(@"Group.ErrorAddBlocked") : TGLocalized(@"Group.ErrorAddBlocked");
            } else if ([errorType isEqual:@"USERS_TOO_MUCH"]) {
                if (_channelConversation.isChannelGroup) {
                    errorText = TGLocalized(@"ConversationProfile.UsersTooMuchError");
                } else {
                    errorText = TGLocalized(@"Channel.ErrorAddTooMuch");
                }
            } else if ([errorType isEqual:@"BOTS_TOO_MUCH"]) {
                errorText = TGLocalized(@"Group.ErrorAddTooMuchBots");
            } else if ([errorType isEqual:@"USER_NOT_MUTUAL_CONTACT"]) {
                errorText = TGLocalized(@"Group.ErrorNotMutualContact");
            } else if ([errorType isEqualToString:@"USER_PRIVACY_RESTRICTED"]) {
                if (users.count == 1) {
                    NSString *format = conversation.isChannelGroup ? TGLocalized(@"Privacy.GroupsAndChannels.InviteToGroupError") : TGLocalized(@"Privacy.GroupsAndChannels.InviteToChannelError");
                    TGUser *user = users.firstObject;
                    errorText = [[NSString alloc] initWithFormat:format, user.displayFirstName, user.displayFirstName];
                } else {
                    errorText = TGLocalized(@"Privacy.GroupsAndChannels.InviteToChannelMultipleError");
                }
            }
            
            [TGCustomAlertView presentAlertWithTitle:nil message:errorText cancelButtonTitle:TGLocalized(@"Common.OK") okButtonTitle:nil completionBlock:nil];
        } completed:nil];
    }
}

- (void)actionItemSelected
{
    __autoreleasing NSString *disabledMessage = nil;
    if (![TGApplicationFeatures isGroupCreationEnabled:&disabledMessage])
    {
        if ([self.tableView indexPathForSelectedRow])
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:true];
        [TGCustomAlertView presentAlertWithTitle:TGLocalized(@"FeatureDisabled.Oops") message:disabledMessage cancelButtonTitle:TGLocalized(@"Common.OK") okButtonTitle:nil completionBlock:nil];
        return;
    }
    
    TGSelectContactController *createGroupController = [[TGSelectContactController alloc] initWithCreateGroup:true createEncrypted:false createBroadcast:false createChannel:false inviteToChannel:false showLink:false];
    [self.navigationController pushViewController:createGroupController animated:true];
}

- (void)encryptionItemSelected
{
    TGSelectContactController *selectContactController = [[TGSelectContactController alloc] initWithCreateGroup:false createEncrypted:true createBroadcast:false createChannel:false inviteToChannel:false showLink:false];
    [self.navigationController pushViewController:selectContactController animated:true];
}

- (NSString *)baseTitle
{
    if (_createChannel || _inviteToChannel) {
        return TGLocalized(@"Compose.ChannelMembers");
    }
    return _createBroadcast ? TGLocalized(@"Compose.NewBroadcast") : [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"create_group" withInfoParam:@"title" withDefaultStr:TGLocalized(@"Create_Group_New_Group")];
}

- (void)loadView
{
    [super loadView];
    
    if ((self.contactsMode & TGContactsModeCompose) == TGContactsModeCompose || (self.contactsMode & TGContactsModeInvite) == TGContactsModeInvite)
    {
        if (!_createChannel && !_inviteToChannel) {
            [self setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle: [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"create_group" withInfoParam:@"button_next_text" withDefaultStr:TGLocalized(@"Common_Next")] style:UIBarButtonItemStylePlain target:self action:@selector(createButtonPressed:)]];
        } else if (_createChannel){
            [self setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:TGLocalized(@"Common.Next") style:UIBarButtonItemStyleDone target:self action:@selector(channelNextPressed)]];
        } else if (_inviteToChannel) {
            [self setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:TGLocalized(@"Common.Cancel") style:UIBarButtonItemStyleDone target:self action:@selector(cancelPressed)]];
            [self setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:TGLocalized(@"Common.Done") style:UIBarButtonItemStyleDone target:self action:@selector(inviteChannelNextPressed)]];
        }
        
        if (!self.createChannel) {
            self.navigationItem.rightBarButtonItem.enabled = [self selectedContactsCount] != 0;
        }
        
        self.titleText = [self baseTitle];
        
        _titleContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, 2)];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = self.presentation.pallete.navigationTitleColor;
        _titleLabel.font = TGBoldSystemFontOfSize(17.0f);
        _titleLabel.text = [self baseTitle];
        [_titleLabel sizeToFit];
        [_titleContainer addSubview:_titleLabel];
        
        _counterLabel = [[UILabel alloc] init];
        _counterLabel.backgroundColor = [UIColor clearColor];
        _counterLabel.textColor = self.presentation.pallete.navigationSubtitleColor;
        _counterLabel.font = TGSystemFontOfSize(15.0f);
        _counterLabel.text = [[NSString alloc] initWithFormat:@"0/%d", _displayUserCountLimit];
        if (!_createChannel && !_inviteToChannel) {
            [_titleContainer addSubview:_counterLabel];
        }
        [self setTitleView:_titleContainer];
        
        int max = (int)[[UIScreen mainScreen] bounds].size.height;
        if (max == 812) {
            addMemberContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 132, self.view.frame.size.width, 40)];
        }else {
            addMemberContainerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 108, self.view.frame.size.width, 40)];
        }
        addMemberContainerView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:addMemberContainerView];
        
        UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, addMemberContainerView.frame.size.width, 1.0)];
        topLineView.backgroundColor = [UIColor colorWithRed:208.0/255.0 green:219.0/255.0 blue:221.0/255.0 alpha:1];
        [addMemberContainerView addSubview:topLineView];
        
        UIButton *btnAddContact = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, addMemberContainerView.frame.size.width, addMemberContainerView.frame.size.height)];
        [btnAddContact addTarget:self action:@selector(ButtonAddContactClicked) forControlEvents:UIControlEventTouchUpInside];
        [addMemberContainerView addSubview:btnAddContact];
        
        UIImageView *addContactImageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 5, 25, 25)];
        addContactImageView.image = [UIImage imageNamed:@"add_user.png"];
        [addMemberContainerView addSubview:addContactImageView];
        
        UILabel *addContactLabel = [[UILabel alloc] initWithFrame:CGRectMake(addContactImageView.frame.origin.x+ addContactImageView.frame.size.width + 10, 5, self.view.frame.size.width-addContactImageView.frame.origin.x+ addContactImageView.frame.size.width, 30)];
        addContactLabel.text = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"create_group" withInfoParam:@"button_add_members_text" withDefaultStr:TGLocalized(@"Create_Group_Add_Member")];//@"Add Members";
        addContactLabel.textAlignment = NSTextAlignmentLeft;
        addContactLabel.font = [UIFont fontWithName:@"Helvetica Medium" size:16];
        addContactLabel.textColor = [UIColor colorWithRed:46.0/255.0 green:107.0/255.0 blue:232.0/255.0 alpha:1];
        [addMemberContainerView addSubview:addContactLabel];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 39, addMemberContainerView.frame.size.width, 1.0)];
        lineView.backgroundColor = [UIColor colorWithRed:208.0/255.0 green:219.0/255.0 blue:221.0/255.0 alpha:1];
        [addMemberContainerView addSubview:lineView];
        
        //int max = (int)[[UIScreen mainScreen] bounds].size.height;
        if ((self.contactsMode & TGContactsModeInvite) == TGContactsModeInvite) {
            if (max == 812) {
                self.tableView.frame = CGRectMake(0.0, 132, self.view.frame.size.width, self.view.frame.size.height-132);
            }else {
                self.tableView.frame = CGRectMake(0.0, 108, self.view.frame.size.width, self.view.frame.size.height-108);
            }
        }else {
            if (max == 812) {
                self.tableView.frame = CGRectMake(0.0, 172, self.view.frame.size.width, self.view.frame.size.height-172);
            }else {
                self.tableView.frame = CGRectMake(0.0, 148, self.view.frame.size.width, self.view.frame.size.height-148);
            }
        }

        
    }
    else if (_createEncrypted)
    {
        self.titleText = @"New Secret Chat";
    }
    else if (_call)
    {
        self.titleText = @"New Call";
    }
    else
    {
        self.titleText = @"New Message";
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TGLocalized(@"Common.Back") style:UIBarButtonItemStylePlain target:self action:@selector(backPressed)];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self _layoutTitleViews:toInterfaceOrientation];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = TGThemeColor();
    [[UINavigationBar appearance] setBarTintColor:TGThemeColor()];
    [self _layoutTitleViews:self.interfaceOrientation];
}

- (void)_layoutTitleViews:(UIInterfaceOrientation)orientation
{
    CGFloat portraitOffset = 0.0f;
    CGFloat landscapeOffset = 0.0f;
    CGFloat indicatorOffset = 0.0f;
    if (iosMajorVersion() >= 7)
    {
        portraitOffset = 1.0f;
        landscapeOffset = 0.0f;
        indicatorOffset = -1.0f;
    }
    else
    {
        portraitOffset = -1.0f;
        landscapeOffset = 1.0f;
        indicatorOffset = 0.0f;
    }
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        _counterLabel.font = TGSystemFontOfSize(13.0f);
    } else {
        _counterLabel.font = TGSystemFontOfSize(15.0f);
    }
    [_counterLabel sizeToFit];
    CGSize counterSize = _counterLabel.frame.size;
    
    CGRect titleLabelFrame = _titleLabel.frame;
    
    if (_createChannel || _inviteToChannel) {
        titleLabelFrame.origin = CGPointMake(CGFloor((_titleContainer.frame.size.width - titleLabelFrame.size.width) / 2.0f), CGFloor((_titleContainer.frame.size.height - titleLabelFrame.size.height) / 2.0f) + (UIInterfaceOrientationIsPortrait(orientation) ? portraitOffset : landscapeOffset));
        _titleLabel.frame = titleLabelFrame;
    } else {
        titleLabelFrame.origin = CGPointMake(CGFloor((_titleContainer.frame.size.width - titleLabelFrame.size.width) / 2.0f - (UIInterfaceOrientationIsPortrait(orientation) ? 0 : counterSize.width / 2.0f)), CGFloor((_titleContainer.frame.size.height - titleLabelFrame.size.height) / 2.0f) + (UIInterfaceOrientationIsPortrait(orientation) ? portraitOffset : landscapeOffset) - (UIInterfaceOrientationIsPortrait(orientation) ? 7.0f : 0.0f));
        _titleLabel.frame = titleLabelFrame;
        
        if (UIInterfaceOrientationIsPortrait(orientation)) {
            _counterLabel.frame = CGRectMake(CGFloor((_titleContainer.frame.size.width - counterSize.width) / 2.0f), CGRectGetMaxY(titleLabelFrame) - 2.0f, counterSize.width, counterSize.height);
        } else {
            _counterLabel.frame = CGRectMake(CGRectGetMaxX(titleLabelFrame) + 4, titleLabelFrame.origin.y + 2 - TGScreenPixel, counterSize.width, counterSize.height);
        }
    }
}

- (void)createButtonPressed:(id)__unused sender
{
    NSArray *contacts = [self selectedContactsList];
    if (contacts.count == 0)
        return;
    else
    {
        if (_createGroupController == nil)
        {
            _createGroupController = [[TGCreateGroupController alloc] initWithCreateChannel:false createChannelGroup:false];
        }
        
        NSMutableArray *userIds = [[NSMutableArray alloc] init];
        for (TGUser *user in [self selectedComposeUsers])
        {
            [userIds addObject:@(user.uid)];
        }
        [_createGroupController setUserIds:userIds];
        
        [self.navigationController pushViewController:_createGroupController animated:true];
    }
}

- (void)contactSelected:(TGUser *)user
{
    int count = [self selectedContactsCount];
    
    if (!_createChannel) {
        self.navigationItem.rightBarButtonItem.enabled = count != 0;
    }
    
    [super contactSelected:user];
    
    [self updateCount:count];
}

- (void)updateCount:(int)count
{
    _counterLabel.text = [[NSString alloc] initWithFormat:@"%d/%d", count, _displayUserCountLimit];
    [self _layoutTitleViews:self.interfaceOrientation];
}

- (void)singleUserSelected:(TGUser *)user
{
    if (_createEncrypted)
    {
        if ([self.tableView indexPathForSelectedRow] != nil)
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:true];
        
        _progressWindow = [[TGProgressWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_progressWindow show:true];
        
        _currentEncryptedUser = user;
        
        static int actionId = 0;
        [ActionStageInstance() requestActor:[[NSString alloc] initWithFormat:@"/tg/encrypted/createChat/(profile%d)", actionId++] options:@{@"uid": @(user.uid)} flags:0 watcher:self];
    }
    else if (_call)
    {
        if (self.onCall != nil)
            self.onCall(user);
    }
    else
    {
        [super singleUserSelected:user];
    }
}

- (void)contactDeselected:(TGUser *)user
{
    int count = [self selectedContactsCount];
    
    if (!_createChannel) {
        self.navigationItem.rightBarButtonItem.enabled = count != 0;
    }
    
    [super contactDeselected:user];
    
    [self updateCount:count];
}

#pragma mark -

- (void)actionStageActionRequested:(NSString *)action options:(NSDictionary *)options
{
    if ([action isEqualToString:@"chatCreated"])
    {
        _shouldBeRemovedFromNavigationAfterHiding = true;
    }
    
    if ([[self superclass] instancesRespondToSelector:@selector(actionStageActionRequested:options:)])
        [super actionStageActionRequested:action options:options];
}

- (void)actorCompleted:(int)status path:(NSString *)path result:(id)result
{
    if ([path hasPrefix:@"/tg/encrypted/createChat/"])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [_progressWindow dismiss:true];
            _progressWindow = nil;
            
            if (status == ASStatusSuccess)
            {
                TGConversation *conversation = result[@"conversation"];
                [[TGInterfaceManager instance] navigateToConversationWithId:conversation.conversationId conversation:conversation];
            }
            else
            {
                [TGCustomAlertView presentAlertWithTitle:nil message:status == -2 ? [[NSString alloc] initWithFormat:TGLocalized(@"Profile.CreateEncryptedChatOutdatedError"), _currentEncryptedUser.displayFirstName, _currentEncryptedUser.displayFirstName] : TGLocalized(@"Profile.CreateEncryptedChatError") cancelButtonTitle:TGLocalized(@"Common.OK") okButtonTitle:nil completionBlock:nil];
            }
        });
    }
    
    if ([[self superclass] instancesRespondToSelector:@selector(actorCompleted:path:result:)])
        [super actorCompleted:status path:path result:result];
}

- (UITableViewCell *)cellForRowInFirstSection:(NSInteger)__unused row
{
    TGGroupInfoContactListCreateLinkCell *cell = (TGGroupInfoContactListCreateLinkCell *)[self.tableView dequeueReusableCellWithIdentifier:@"TGGroupInfoContactListCreateLinkCell"];
    if (cell == nil)
    {
        cell = [[TGGroupInfoContactListCreateLinkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TGGroupInfoContactListCreateLinkCell"];
    }
    cell.presentation = self.presentation;
    return cell;
}

- (NSInteger)numberOfRowsInFirstSection
{
    if (self.contactsMode & TGContactsModeCreateGroupLink)
        return 1;
    return 0;
}

- (CGFloat)itemHeightForFirstSection
{
    return 48.0f;
}

- (void)didSelectRowInFirstSection:(NSInteger)__unused row
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:true];
    if (_onCreateLink) {
        _onCreateLink();
    }
}

@end
