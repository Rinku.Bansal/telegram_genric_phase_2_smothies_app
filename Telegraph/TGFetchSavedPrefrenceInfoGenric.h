//
//  TGFetchSavedPrefrenceInfoGenric.h
//  Telegraph
//
//  Created by vinove on 12/12/18.
//

#import <Foundation/Foundation.h>

@interface TGFetchSavedPrefrenceInfoGenric : NSObject

// Get Admin channels and Admin bots getting from our server
+ (NSArray *)getAdminChannels;
+ (NSArray *)getAdminBots;

// Get User Channels and User Bots when user Add
+ (NSMutableArray *)getUserChannels;
+ (NSMutableArray *)getUserBots;

// Get saved user list details
+ (NSMutableArray *)getUserList;

// Get Telegram user information
+ (NSString *)getFirstName;
+ (NSString *)getLastName;
+ (NSString *)getMobileNumber;

// Get all application Text from saved prefrence
+ (NSDictionary *)getAllApplicationSavedText;

// is Genric Bot or not
+ (bool)isGenricBot;

// is controller selected tableview selection or not
+ (bool)isSelectedType;

// Get user search channels and add channels and save channel info
+ (NSString *)getAddedChannelId;
+ (NSString *)getAddedChannelName;

// Get search Bots and add save bot info
+ (NSString *)getAddedBotId;
+ (NSString *)getAddedBotName;

// Get delete channel info save
+ (NSString *)getDeleteChannelId;
+ (NSString *)getDeletechannelName;

// Get delete Bot info save
+ (NSString *)getDeletebotId;
+ (NSString *)getDeletebotName;

// Get delete User
+ (NSString *)getDeleteUserId;

// Get Genric app id getting from our server
+ (NSString *)getGenricApplicationappId;

// Get telegram uid from telegram
+ (NSInteger)getTelegramUid;

// Get user block status
+ (bool)isUserBlocked;

// Get Chatting User Name
+ (NSString *)getUserChatterName;

// Get Days from install count
+ (int)getDaysFromIntallCount;

// Get Daily app launch count
+ (int)getDailyAppLaunchCount;

// Get Current Date
+ (NSString *)getCurrentDate;

// Get Appsflyer Refferal name
+ (NSString *)getRefferalNameFromAppsFlyer;

// Get Goals Arr Save before login
+ (NSArray *)getSelectedGoalsArrayBeforeLoginSaved;

// Get Prefrence Arr Save before login
+ (NSArray *)getSelectedPrefrenceArrayBeforeLoginSaved;

// Get Favorate Recipe id
+ (NSArray *)getFavorateRecipeIdArr;

// Get unread count only displayed cells
+ (int)getUnreadCountDisplayedCells;

// Get Selected ingredients Like list
+ (NSArray *)getSelectedIngredientsLikeList;

// Get Selected ingredients DisLike list
+ (NSArray *)getSelectedIngredientsDisLikeList;

// Get Selected Health Condition
+ (NSArray *)getSelectedHealthConditionList;

// Get User created channel favorate smoothies
+ (NSString *)getFavorateSmoothiesChannelId;

// Get User created User Name Channel
+ (NSString *)getUserNameCreatedChannelId;

// Get recipe id share in user channel
+ (NSArray *)getRecipeIdShareChannnel;

// Get User First Time Login Call ViewWillAppear Method On dialogList
+ (bool)isFirstTime;

// Get Smoothies Changes Like ingredients, health Condition, Goals, Prefrences
+ (bool)isSmoothiesChanges;

// Only First time hit all api
+ (bool)isHitAllApi;

// Get Campaign name Appsflyer
+ (NSString *)getCampaignName;

// Get group created List from server
+ (NSArray *)getGroupList;
@end
