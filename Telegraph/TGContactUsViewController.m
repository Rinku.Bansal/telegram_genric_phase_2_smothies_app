//
//  TGContactUsViewController.m
//  Telegraph
//
//  Created by Rinku on 21/02/19.
//

#import "TGContactUsViewController.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"
#import "TelegramServiceClass.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"

@interface TGContactUsViewController ()<UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate>{
    
    __weak IBOutlet NSLayoutConstraint *lblNameTopConstraints;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UITextField *nameTextField;
    __weak IBOutlet UILabel *lblEmail;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UILabel *lblMessage;
    __weak IBOutlet UITextView *messageTextView;
    __weak IBOutlet UIScrollView *scrollerView;
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIButton *btnSubmit;
    __weak IBOutlet NSLayoutConstraint *containerViewTopConstraint;
    __weak IBOutlet UIActivityIndicatorView *activityController;
    
    UIView *allContainerView;
    UITextView *activeField;
}

@end

@implementation TGContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGContactUsViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [allContainerView addSubview:controllerView];
    
    [TGMixPanelEventsConstants trackMixpanelEvents:CONTACT_US properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:CONTACT_US]];

    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"contact_us" withInfoParam:@"title" withDefaultStr:TGLocalized(@"Contact_Us_Header_Title")];//@"Contact Us";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    [activityController setHidden:true];
    [self initialiseTheView];
}

- (void)initialiseTheView {
    int topConstant = 0;
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        topConstant = 100;
    }else {
        topConstant = 70;
    }
    lblNameTopConstraints.constant = topConstant;
    
    nameTextField.placeholder = TGLocalized(@"Contact_Us_Your_Name");
    lblName.text = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"contact_us" withInfoParam:@"name_hint" withDefaultStr:TGLocalized(@"Contact_Us_Your_Name")];
    
    emailTextField.placeholder = @"Your Email";
    lblEmail.text = TGLocalized(@"Contact_Us_Email");
    
    lblMessage.text = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"contact_us" withInfoParam:@"message_hint" withDefaultStr:TGLocalized(@"Contact_Us_Message")];
    
    [btnSubmit setTitle:[[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"contact_us" withInfoParam:@"button_submit_text" withDefaultStr:TGLocalized(@"Contact_Us_Submit")] forState:UIControlStateNormal];
    
    [self addDoneButtonOnTextView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:CONTACT_US properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:CONTACT_US]];
    [self.navigationController popViewControllerAnimated:true];
}

// To validate the credential locally.
- (BOOL)isCredentialValid{
    if (nameTextField.text.length<=0) {
        [self showMessage:TGLocalized(@"Contact_Us_Please_Enter_Name")];
        return NO;
    }
    if (emailTextField.text.length<=0) {
        [self showMessage:TGLocalized(@"Contact_Us_Please_Enter_Email")];
        return NO;
    }
    if (![self validateEmailWithString:emailTextField.text]) {
        [self showMessage:TGLocalized(@"Contact_Us_Please_Enter_Email")];
        return NO;
    }
    if (messageTextView.text.length<=0) {
        [self showMessage:TGLocalized(@"Contact_Us_Please_Enter_Message")];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)showMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)submitClicked:(id)sender {
    if (![self isCredentialValid]) {
        return;
    }
    
    [activityController setHidden:false];
    [activityController startAnimating];
    [[TelegramServiceClass shareInstance] saveContactUsInformationFromServer:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] name:nameTextField.text emailId:emailTextField.text message:messageTextView.text with:^(BOOL success, NSError *error, NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [activityController setHidden:true];
            [activityController stopAnimating];
            if (success) {
                nameTextField.text = @"";
                emailTextField.text = @"";
                messageTextView.text = @"";
                [self showMessage:TGLocalized(@"Contact_Us_Save_Successfully")];
            }else {
                NSLog(@"error is ====>>>%@",error);
            }
        });
    }];
}

- (void)addDoneButtonOnTextView {
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    messageTextView.inputAccessoryView = keyboardToolbar;

}

-(void)yourTextViewDoneButtonPressed
{
    [messageTextView resignFirstResponder];
    containerViewTopConstraint.constant = 0;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 568) {
        containerViewTopConstraint.constant = -140;
    }else if (max == 667) {
        containerViewTopConstraint.constant = -50;
    }else {
        
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    containerViewTopConstraint.constant = 0;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (nameTextField == textField) {
        [emailTextField becomeFirstResponder];
    } else if (emailTextField == textField) {
        [emailTextField resignFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return NO;
}

- (IBAction)dismissKeyboardTap:(id)sender {
    [nameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [messageTextView resignFirstResponder];
    containerViewTopConstraint.constant = 0;
}


@end
