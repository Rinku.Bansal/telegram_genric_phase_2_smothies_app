//
//  TGHealthConditionTableViewCell.m
//  Telegraph
//
//  Created by vinove on 1/8/19.
//

#import "TGHealthConditionTableViewCell.h"

@implementation TGHealthConditionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)checkClick:(id)sender {
    if(_selectDelegate != nil && [_selectDelegate respondsToSelector:@selector(selectionClickedAtIndex:withIndex:)])
        [_selectDelegate selectionClickedAtIndex:_section withIndex:_cellIndex];
}


@end
