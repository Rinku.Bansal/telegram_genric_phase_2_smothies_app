//
//  TGGenricAppInformation.m
//  Telegraph
//
//  Created by Rinku on 19/11/18.
//

#import "TGGenricAppInformation.h"
#import "TGFetchSavedPrefrenceInfoGenric.h"

@implementation TGGenricAppInformation

// Mark:- Shared instanc
+ (TGGenricAppInformation *)shareInstance{
    // Creating shared instance
    static TGGenricAppInformation * TGInfoClass = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        TGInfoClass = [[self alloc] init];
    });
    return TGInfoClass;
}

- (NSString *)getTextDetailsFromInfoPlist:(NSString *)dicParam withInfoParam:(NSString *)param withDefaultStr:(NSString *)tempStr{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
//    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"GenricAppInfo.plist"];
//
//    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
//    {
//        plistPath = [[NSBundle mainBundle] pathForResource:@"GenricAppInfo" ofType:@"plist"];
//    }
//    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
//    NSString *infoparam = @"";
//    if (dict != nil) {
//        NSDictionary *screenParamDic = [dict objectForKey:dicParam];
//        infoparam = [NSString stringWithFormat:@"%@", [screenParamDic objectForKey:param]];
//        return infoparam;
//    }else {
//        NSDictionary *dict = [TGFetchSavedPrefrenceInfoGenric getAllApplicationSavedText];
//        NSString *infoparam = @"";
//        if (dict != nil) {
//            NSDictionary *screenParamDic = [dict objectForKey:dicParam];
//            infoparam = [NSString stringWithFormat:@"%@", [screenParamDic objectForKey:param]];
//            return infoparam;
//        }
//    }
//    return infoparam;
    return tempStr;
}

- (NSString *)getFontDetailsFromPlist:(NSString *)dicParam {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"GenricAppInfo.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"GenricAppInfo" ofType:@"plist"];
    }
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSString *fontParam = @"";
    if (dict != nil) {
        NSDictionary *screenParamDic = [dict objectForKey:dicParam];
        fontParam = [NSString stringWithFormat:@"%@", [screenParamDic objectForKey:@"font_family"]];
        return fontParam;
    }
    return fontParam;
}

@end
