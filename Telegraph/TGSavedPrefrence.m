//
//  TGSavedPrefrence.m
//  Telegraph
//
//  Created by vinove on 12/12/18.
//

#import "TGSavedPrefrence.h"

#define ADMIN_CHANNEL_LIST @"ADMIN_CHANNEL_LIST"
#define ADMIN_BOT_LIST @"ADMIN_BOT_LIST"

#define USER_CHANNEL_LIST @"USER_CHANNEL_LIST"
#define USER_BOT_LIST @"USER_BOT_LIST"

#define USER_LIST_SAVE @"USER_LIST_SAVE"

#define USER_FIRST_NAME @"USER_FIRST_NAME"
#define USER_LAST_NAME @"USER_LAST_NAME"
#define USER_MOBILE_NUMBER @"USER_MOBILE_NUMBER"

#define USER_BLOCK_STATUS @"USER_BLOCK_STATUS"

#define ALL_APPLICATION_TEXT_INFO @"ALL_APPLICATION_TEXT_INFO"

#define IS_GENRIC_BOT_ON_ADMIN @"IS_GENRIC_BOT_ON_ADMIN"

#define IS_CONTROLLER_SELECTED_TYPE @"IS_CONTROLLER_SELECTED_TYPE"

#define SEARCH_ADD_JOIN_CHANNEL_ID @"SEARCH_ADD_JOIN_CHANNEL_ID"
#define SEARCH_ADD_JOIN_CHANNEL_NAME @"SEARCH_ADD_JOIN_CHANNEL_NAME"

#define SEARCH_ADD_BOT_ID @"SEARCH_ADD_BOT_ID"
#define SEARCH_ADD_BOT_USER_NAME @"SEARCH_ADD_BOT_USER_NAME"

#define DELETE_CHANNEL_ID @"DELETE_CHANNEL_ID"
#define DELETE_CHANNEL_NAME @"DELETE_CHANNEL_NAME"

#define DELETE_BOT_ID @"DELETE_BOT_ID"
#define DELETE_BOT_USER_NAME @"DELETE_BOT_USER_NAME"

#define DELETE_USER_ID @"DELETE_USER_ID"

#define GENRIC_APP_ID @"GENRIC_APP_ID"

#define TELEGRAM_UID_TELEGRAM_SERVER @"TELEGRAM_UID_TELEGRAM_SERVER"

#define CHAT_USER_NAME @"CHAT_USER_NAME"

#define DAYS_INSATLL_COUNT @"DAYS_INSATLL_COUNT"

#define DAYS_APP_LAUNCH_COUNT @"DAYS_APP_LAUNCH_COUNT"

#define CURRENT_DATE_SAVE @"CURRENT_DATE_SAVE"

#define REFFRAL_NAME @"REFFRAL_NAME"


#define SAVE_SELECTED_GOALS_BEFORE_LOGIN @"SAVE_SELECTED_GOALS_BEFORE_LOGIN"

#define SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN @"SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN"

#define SAVE_FAVORATE_RECIPE_ID @"SAVE_FAVORATE_RECIPE_ID"

#define SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE @"SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE"

#define SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS @"SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS"

#define SAVE_SELECTED_INGREDIENTS_LIST_LIKE @"SAVE_SELECTED_INGREDIENTS_LIST_LIKE"

#define SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE @"SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE"

#define SAVE_SELECTED_HEALTH_CONDITION @"SAVE_SELECTED_HEALTH_CONDITION"

#define SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL @"SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL"

#define SAVE_USER_CREATE_USER_NAME_CHANNEL @"SAVE_USER_CREATE_USER_NAME_CHANNEL"

#define SAVE_USER_FIRST_TIME_LOGIN @"SAVE_USER_FIRST_TIME_LOGIN"

#define SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS @"SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS"

#define SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API @"SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API"

#define SAVE_CAMPAIGN_NAME_APPSFLYER @"SAVE_CAMPAIGN_NAME_APPSFLYER"

#define SAVE_GROUP_CREATION_LIST @"SAVE_GROUP_CREATION_LIST"

@implementation TGSavedPrefrence

// ****************************************** Change ***********************************************

// Save Admin channels and Admin bots getting from our server
+ (void)saveAdminChannels:(NSArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:ADMIN_CHANNEL_LIST];
}

+ (void)saveAdminBots:(NSArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:ADMIN_BOT_LIST];
}

// Save User Channels and User Bots when user Add
+ (void)saveUserChannels:(NSMutableArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:USER_CHANNEL_LIST];
}

+ (void)saveUserBots:(NSMutableArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:USER_BOT_LIST];
}

// ******************************************** Complete ************************************************

// Save User List
+ (void)saveUserList:(NSMutableArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:USER_LIST_SAVE];
}

// Telegram user information
+ (void)saveFirstName:(NSString *)firstName {
    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:USER_FIRST_NAME];
}

+ (void)saveLastName:(NSString *)lastName {
    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:USER_LAST_NAME];
}

+ (void)saveMobile:(NSString *)mobileNumber {
    [[NSUserDefaults standardUserDefaults] setObject:mobileNumber forKey:USER_MOBILE_NUMBER];
}

// Save all application Text from
+ (void)saveAllApplicationText:(NSDictionary *)dic {
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:ALL_APPLICATION_TEXT_INFO];

}

// Save is Genric Bot or not
+ (void)saveIsGenricBot:(bool)isGenricBot {
    [[NSUserDefaults standardUserDefaults] setBool:isGenricBot forKey:IS_GENRIC_BOT_ON_ADMIN];

}

// add channels and bots add first not change screen
+ (void)saveControllerSelectedType:(bool)selectedType {
    [[NSUserDefaults standardUserDefaults] setBool:selectedType forKey:IS_CONTROLLER_SELECTED_TYPE];

}

// User search channels and add channels and save channel info
+ (void)saveSearchAddChannelsId:(NSString *)channelId {
    [[NSUserDefaults standardUserDefaults] setObject:channelId forKey:SEARCH_ADD_JOIN_CHANNEL_ID];

}

+ (void)saveSearchAddChannelsName:(NSString *)channelName {
    [[NSUserDefaults standardUserDefaults] setObject:channelName forKey:SEARCH_ADD_JOIN_CHANNEL_NAME];

}

// User search Bots and save bot info
+ (void)saveSearchAddBotId:(NSString *)botId {
    [[NSUserDefaults standardUserDefaults] setObject:botId forKey:SEARCH_ADD_BOT_ID];

}

+ (void)saveSearchAddBotName:(NSString *)botName {
    [[NSUserDefaults standardUserDefaults] setObject:botName forKey:SEARCH_ADD_BOT_USER_NAME];

}

// User delete channel info save
+ (void)saveDeleteChannelId:(NSString *)channelId {
    [[NSUserDefaults standardUserDefaults] setObject:channelId forKey:DELETE_CHANNEL_ID];
}

+ (void)saveDeleteChannelName:(NSString *)channelName {
    [[NSUserDefaults standardUserDefaults] setObject:channelName forKey:DELETE_CHANNEL_NAME];
}

// User delete Bot info save
+ (void)saveDeleteBotId:(NSString *)botId {
    [[NSUserDefaults standardUserDefaults] setObject:botId forKey:DELETE_BOT_ID];
}

+ (void)saveDeleteBotName:(NSString *)botName {
    [[NSUserDefaults standardUserDefaults] setObject:botName forKey:DELETE_BOT_USER_NAME];
}

// User delete
+ (void)saveDeleteUser:(NSString *)userId {
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:DELETE_USER_ID];
}

// save Genric app id getting from our server
+ (void)saveGenricAppId:(NSString *)appId {
    [[NSUserDefaults standardUserDefaults] setObject:appId forKey:GENRIC_APP_ID];
}

// save telegram uid from telegram
+ (void)saveTelegramUIDInfo:(int)telegramUid {
    [[NSUserDefaults standardUserDefaults] setInteger:telegramUid forKey:TELEGRAM_UID_TELEGRAM_SERVER];
}

// Save user block status
+ (void)saveUserBlockedStatus:(bool)blockStatus {
    [[NSUserDefaults standardUserDefaults] setBool:blockStatus forKey:USER_BLOCK_STATUS];
}

// ****************************************** Change ***********************************************

// Save Chatting User, Channel, Bot Name
+ (void)saveChatUserName:(NSString *)chatUser {
    [[NSUserDefaults standardUserDefaults] setObject:chatUser forKey:CHAT_USER_NAME];
}

// Save Days install count
+ (void)saveDaysInstallCount:(int)count {
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:DAYS_INSATLL_COUNT];
}

// Save Daily App launch count
+ (void)saveDailyAppLaunchCount:(int)count {
    [[NSUserDefaults standardUserDefaults] setInteger:count forKey:DAYS_APP_LAUNCH_COUNT];
}

// Save Current Date
+ (void)saveCurrentDate:(NSString *)date {
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:CURRENT_DATE_SAVE];
}

// Save Referral Name
+ (void)saveRefferalNameGettingAppsflyer:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:REFFRAL_NAME];
}

// Save selected goals before login
+ (void)saveSelectedGoalsName:(NSString *)goalsStr {
    [[NSUserDefaults standardUserDefaults] setObject:goalsStr forKey:SAVE_SELECTED_GOALS_BEFORE_LOGIN];
}

// Save selected prefrences before login
+ (void)saveSelectedPrefrencesName:(NSString *)prefrenceStr {
    [[NSUserDefaults standardUserDefaults] setObject:prefrenceStr forKey:SAVE_SELECTED_PREFRENCES_BEFORE_LOGIN];
}

// Save Seleted Ingredients List
+ (void)saveSelectedIngredientsList:(NSString *)ingredientsLikeStr dislike:(NSString *)ingredientsDislikeStr {
    [[NSUserDefaults standardUserDefaults] setObject:ingredientsLikeStr forKey:SAVE_SELECTED_INGREDIENTS_LIST_LIKE];
    [[NSUserDefaults standardUserDefaults] setObject:ingredientsDislikeStr forKey:SAVE_SELECTED_INGREDIENTS_LIST_DISLIKE];
}

// Save selected Health Condition
+ (void)saveSeletedHealthCondition:(NSString *)healthStr {
    [[NSUserDefaults standardUserDefaults] setObject:healthStr forKey:SAVE_SELECTED_HEALTH_CONDITION];
}

// Save Favorate Recipe Id Saves
+ (void)saveFavorateRecipesIdFromArray:(NSString *)favorateArr {
    [[NSUserDefaults standardUserDefaults] setObject:favorateArr forKey:SAVE_FAVORATE_RECIPE_ID];
}

// Save Channel Share Recipe Id
+ (void)saveRecipeIdShareToChannelFromArray:(NSMutableArray *)channelRecipeIdArr {
    [[NSUserDefaults standardUserDefaults] setObject:channelRecipeIdArr forKey:SAVE_CHANNEL_RECIPE_ID_VIEW_RECIPE];
}

// Save Unread count
+ (void)saveUnreadCountOnlyShowCells:(int)unreadCount {
    [[NSUserDefaults standardUserDefaults] setInteger:unreadCount forKey:SAVE_UNREAD_COUNT_ONLY_DISPLAY_CELLS];
}

// Save User Create Favorate Smoothies Channel
+ (void)saveUserCreateFavorateSmoothiesChannel:(NSString *)channelId {
    [[NSUserDefaults standardUserDefaults] setObject:channelId forKey:SAVE_USER_CREATE_FAVORATE_SMOOTHIES_CHANNEL];
}

// Save User Create User Name Channel
+ (void)saveUserCreateUserNameChannel:(NSString *)channelId {
    [[NSUserDefaults standardUserDefaults] setObject:channelId forKey:SAVE_USER_CREATE_USER_NAME_CHANNEL];
}

// Save User First Time Login Call ViewWillAppear Method On dialogList
+ (void)saveLoginFirstTime:(bool)isFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:isFirstTime forKey:SAVE_USER_FIRST_TIME_LOGIN];
}

// Save Smoothies Changes Like ingredients, health Condition, Goals, Prefrences
+ (void)saveChangeInSmoothiesBot:(bool)isChange {
    [[NSUserDefaults standardUserDefaults] setBool:isChange forKey:SAVE_USER_CHANGE_IN_GOALS_PREFRENCE_HEALTHCONDITION_INGREDIENTS];
}

// Save After login Hit Api Only First Time
+ (void)saveAfterLoginFirstTimeHitAllApi:(bool)isFirst {
    [[NSUserDefaults standardUserDefaults] setBool:isFirst forKey:SAVE_USER_FIRST_TIME_LOGIN_HIT_ALL_API];
}

// Save Campaign name in Appsflyer
+ (void)saveCampaignName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:SAVE_CAMPAIGN_NAME_APPSFLYER];
}

// Save Group List
+ (void)saveGroupIdList:(NSArray *)arr {
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:SAVE_GROUP_CREATION_LIST];
}
@end
