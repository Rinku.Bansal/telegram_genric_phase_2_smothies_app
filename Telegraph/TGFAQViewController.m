//
//  TGFAQViewController.m
//  Telegraph
//
//  Created by vinove on 06/09/18.
//

#import "TGFAQViewController.h"
#import "TGFAQTableViewCell.h"
#import "TGFAQDetailsViewController.h"
#import "TGGenricAppInformation.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"

@interface TGFAQViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    __weak IBOutlet UITableView *faqTableView;
    __weak IBOutlet UILabel *lblUpperText;
    UIView *containerView;
    NSArray *questionArr;
    NSArray *answerArr;
}

@end

@implementation TGFAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGFAQViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    [TGMixPanelEventsConstants trackMixpanelEvents:FAQ properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:FAQ]];

    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"faq" withInfoParam:@"title" withDefaultStr:TGLocalized(@"FAQ_Header_Title")];//@"FAQ";
    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    questionArr = [[NSArray alloc] init];
    answerArr = [[NSArray alloc] init];
    [self setLocallyDataInArr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = TGThemeColor();
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:FAQ properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:FAQ]];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)setLocallyDataInArr {
    faqTableView.rowHeight = UITableViewAutomaticDimension;
    faqTableView.estimatedRowHeight = UITableViewAutomaticDimension;
    lblUpperText.text = TGLocalized(@"FAQ_Here_Is_My_List_Frequently_Question_Answer");
    questionArr = [TGLocalized(@"FAQ_Question_List") componentsSeparatedByString:@"#"];
    answerArr = [TGLocalized(@"FAQ_Answer_List") componentsSeparatedByString:@"#"];
    [faqTableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return questionArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TGFAQTableViewCell *cell = (TGFAQTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TGFAQTableViewCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TGFAQTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblQuestion.text = [NSString stringWithFormat:@"%@",[questionArr objectAtIndex:indexPath.row]];
    cell.lblAnswer.text = [NSString stringWithFormat:@"%@", [answerArr objectAtIndex:indexPath.row]];
  //  cell.lblAnswer.frame = CGRectMake(15, cell.lblQuestion.frame.origin.y + cell.lblQuestion.frame.size.height + 10, self.view.frame.size.width-50, 240);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = TGThemeColor();
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TGFAQDetailsViewController *controller = [[TGFAQDetailsViewController alloc] initWithNibName:@"TGFAQDetailsViewController" bundle:nil];
    controller.question = [NSString stringWithFormat:@"%@",[questionArr objectAtIndex:indexPath.row]];
    controller.answer = [NSString stringWithFormat:@"%@",[answerArr objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:controller animated:true];
}

//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    return 44;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

@end
