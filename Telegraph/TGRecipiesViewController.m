//
//  TGRecipiesViewController.m
//  Telegraph
//
//  Created by vinove on 12/19/18.
//
#define Share_Url @"http://app.ezsmoothie.recipes/m/recipe?r="//@"http://beta.shakensmoothie.recipes/m/recipe?r="

#import "TGRecipiesViewController.h"
#import "TelegramServiceClass.h"
#import "TGRecipeModel.h"
#import "TGIngrediantsCollectionViewCell.h"
#import "TGRecipiesCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "TGIngradiantsShowCollectionViewCell.h"
#import "TGIngradiantsImageCollectionViewCell.h"
#import <LegacyComponents/TGProgressWindow.h>
#import "TGFetchSavedPrefrenceInfoGenric.h"
#import "TGSavedPrefrence.h"
#import "TGRecipeNutritionFactsZoomImageView.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "TGIngredientsDetailsViewController.h"
#import "TGGenericModernConversationCompanion.h"
#import "TGPrivateModernConversationCompanion.h"
#import "TGTelegraph.h"
#import "TGDatabase.h"
#import "TGChannelConversationCompanion.h"
#import "TGMixPanelEventsConstants.h"

@interface TGRecipiesViewController ()<UICollectionViewDataSource, UIScrollViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegateLeftAlignedLayout, selectIngredientsImageProtocol>{
    
    __weak IBOutlet NSLayoutConstraint *upperViewTopConstraints;
    __weak IBOutlet NSLayoutConstraint *topImageTopConstraints;
    __weak IBOutlet NSLayoutConstraint *ingredientsTopConstraints;
    
    __weak IBOutlet NSLayoutConstraint *recipeCollectionViewHeigtConstraints;
    __weak IBOutlet NSLayoutConstraint *btnCloseTopConstraints;
    
    __weak IBOutlet NSLayoutConstraint *bodySystemCollectionViewHeightConstraints;
    
    __weak IBOutlet NSLayoutConstraint *nutritionProgramCollectionViewHeightConstraints;
    
    __weak IBOutlet NSLayoutConstraint *lblRecipeHeightConstraints;
    __weak IBOutlet NSLayoutConstraint *lblRecipeTopConstraints;
    
    __weak IBOutlet NSLayoutConstraint *lblBodySystemHeightConstraints;
    __weak IBOutlet NSLayoutConstraint *lblBodySystemTopConstraints;
    
    __weak IBOutlet NSLayoutConstraint *lblNutritionProgramHeightConstraints;
    __weak IBOutlet NSLayoutConstraint *lblNutritionProgramTopConstriants;
    
    __weak IBOutlet NSLayoutConstraint *ingradiantsCollectionViewHeightConstriants;
    
    __weak IBOutlet UIView *shadowView;
    __weak IBOutlet UIView *pageControlView;
    
    __weak IBOutlet UIView *secondView;
    
    __weak IBOutlet UIScrollView *mainScrollView;
    
    __weak IBOutlet UIImageView *nutritionTopImageView;
    
    __weak IBOutlet UICollectionView *gradiantsCollectionView;
    
    // __weak IBOutlet UIPageControl *pageControl;
    
    __weak IBOutlet UICollectionView *recipeCollectionView;
    
    __weak IBOutlet UICollectionView *bodySystemCollectionView;
    
    __weak IBOutlet UICollectionView *nutritionProgramCollectionView;
    
    __weak IBOutlet UIImageView *nutritionFactsImageView;
    
    __weak IBOutlet UIButton *btnFavrote;
    
    __weak IBOutlet UIButton *btnShare;
    
    __weak IBOutlet UICollectionViewLeftAlignedLayout *recipeLayout;
    __weak IBOutlet UICollectionViewLeftAlignedLayout *bodyLayout;
    __weak IBOutlet UICollectionViewLeftAlignedLayout *nutritionLayout;
    
  //  __weak IBOutlet UIButton *btnChannel;
    
    __weak IBOutlet UIImageView *imgFavorate;
    __weak IBOutlet UIImageView *imgShare;
 //   __weak IBOutlet UIImageView *imgChannel;
    __weak IBOutlet UILabel *lblFavorate;
    
    __weak IBOutlet UICollectionView *gradiantShowCollectionView;
    
    __weak IBOutlet UIButton *btnEnergy;
    __weak IBOutlet UIButton *btnGlymicIndex;
    __weak IBOutlet UIButton *btnMicroNutrients;
    __weak IBOutlet UIButton *btnFats;
    __weak IBOutlet UIButton *btnProtein;
    __weak IBOutlet UIButton *btnSalt;
    
    __weak IBOutlet UILabel *lblWhyThisRecipe;
    __weak IBOutlet UILabel *lblMaySupport;
    __weak IBOutlet UILabel *lblMayAssist;
    __weak IBOutlet UILabel *lblNutritionMeter;
    __weak IBOutlet UILabel *lblEnergy;
    __weak IBOutlet UILabel *lblGlycemicIndec;
    __weak IBOutlet UILabel *lblMicroNutrients;
    __weak IBOutlet UILabel *lblFats;
    __weak IBOutlet UILabel *lblprotein;
    __weak IBOutlet UILabel *lblSalt;
    __weak IBOutlet UILabel *lblFavorite;
    __weak IBOutlet UILabel *lblShare;
  //  __weak IBOutlet UILabel *lblChannel;
    
    
    NSArray *gradiantsArr;
    NSArray *bodySystemArr;
    NSArray *tagArr;
    NSArray *healthConditionArr;
    NSMutableArray *gradiantsScrollingArr;
    int currentPageIndex;
    UIView *containerView;
    NSDictionary *recipeDic;
    NSArray *ingradiantsDetailsArr;
    NSInteger currentIndex;
    TGRecipeNutritionFactsZoomImageView *zoomImageView;
    BOOL isFav;
    BOOL isShareChannel;
}
@property (nonatomic, strong) TGProgressWindow *progressWindow;

@end

@implementation TGRecipiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isFav = false;
    isShareChannel = false;
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGRecipiesViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    [containerView addSubview:controllerView];
    [self.view addSubview:containerView];
    recipeDic = [[NSDictionary alloc] init];
    self.navigationController.navigationBarHidden = true;
    [TGMixPanelEventsConstants trackMixpanelEvents:FULL_RECIPE properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:FULL_RECIPE]];

    int imageConstant = 0;
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        imageConstant = 35;
    }else if (max == 568) {
        imageConstant = 0;
        btnCloseTopConstraints.constant = 30;
        topImageTopConstraints.constant = -20;
        ingredientsTopConstraints.constant = -10;
    }else {
        imageConstant = 20;
    }
    
    upperViewTopConstraints.constant = imageConstant;
    [self initialiseTheArr];
    [self setTextToLabels];
    [self getFullRecipeDetailsFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Method

- (void)initialiseTheArr {
    _progressWindow = [[TGProgressWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    gradiantsArr = [[NSArray alloc] init];
    bodySystemArr = [[NSArray alloc] init];
    tagArr = [[NSArray alloc] init];
    healthConditionArr = [[NSArray alloc] init];
    gradiantsScrollingArr = [[NSMutableArray alloc] init];
    ingradiantsDetailsArr = [[NSArray alloc] init];
    
    shadowView.layer.shadowRadius  = 5.0f;
    shadowView.layer.shadowColor   = TGThemeColor().CGColor;
    shadowView.layer.shadowOffset  = CGSizeZero;
    shadowView.layer.shadowOpacity = 1.0f;
    shadowView.layer.masksToBounds = NO;
    
    [gradiantsCollectionView registerNib:[UINib nibWithNibName:@"TGIngrediantsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGIngrediantsCollectionViewCell"];
    
    [gradiantShowCollectionView registerNib:[UINib nibWithNibName:@"TGIngradiantsImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGIngradiantsImageCollectionViewCell"];
    
    [gradiantShowCollectionView registerNib:[UINib nibWithNibName:@"TGIngradiantsShowCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGIngradiantsShowCollectionViewCell"];
    
    [recipeCollectionView registerNib:[UINib nibWithNibName:@"TGRecipiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell"];
    
    [bodySystemCollectionView registerNib:[UINib nibWithNibName:@"TGRecipiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell"];
    
    [nutritionProgramCollectionView registerNib:[UINib nibWithNibName:@"TGRecipiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell"];
    
}

- (void)setTextToLabels {
    lblWhyThisRecipe.text = TGLocalized(@"View_Recipe_Why_This_Recipe");
    lblMaySupport.text = TGLocalized(@"View_Recipe_Support_These_Body_System");
    lblMayAssist.text = TGLocalized(@"View_Recipe_May_Assist_Nutrition_Program");
    lblNutritionMeter.text = TGLocalized(@"View_Recipe_Nutrition_Meter");
    lblEnergy.text = TGLocalized(@"View_Recipe_Energy");
    lblGlycemicIndec.text = TGLocalized(@"View_Recipe_Glycemic_Index");
    lblMicroNutrients.text = TGLocalized(@"View_Recipe_Micro_Nutrients");
    lblFats.text = TGLocalized(@"View_Recipe_Fats");
    lblprotein.text = TGLocalized(@"View_Recipe_Protein");
    lblSalt.text = TGLocalized(@"View_Recipe_Salt");
    lblFavorite.text = TGLocalized(@"View_Recipe_Favorites");
    lblShare.text = TGLocalized(@"View_Recipe_Share");
    //lblChannel.text = TGLocalized(@"View_Recipe_Channel");
}

// Get full recipe details from server
- (void)getFullRecipeDetailsFromServer {
    [_progressWindow show:true];
    [[TelegramServiceClass shareInstance] getFullRecipeDetailsFromServer:_recipeId :^(BOOL success, NSError *error, NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_progressWindow dismiss:true];
            if (success) {
                recipeDic = [dic objectForKey:@"recipe"];
                
                gradiantsArr = [TGRecipeModel getIngredientsDetails:[recipeDic objectForKey:@"ingredients"]];
                gradiantsScrollingArr = [TGRecipeModel getIngredientsDetailsScrolling:[recipeDic objectForKey:@"ingredients"] withDic:recipeDic];
                
                bodySystemArr = [recipeDic objectForKey:@"body_sys"];
                tagArr = [recipeDic objectForKey:@"tags"];
                healthConditionArr = [recipeDic objectForKey:@"health_cond"];
                [self initialiseTheNutritionMeter];
                [self setEstimatedSizeIfNeeded];
                [self isRecipeFavorate];
                [gradiantsCollectionView reloadData];
                [recipeCollectionView reloadData];
                [bodySystemCollectionView reloadData];
                [nutritionProgramCollectionView reloadData];
                [gradiantShowCollectionView reloadData];
                [self setCollectionViewHeight];
                [self setUpPageControlView];
                NSLog(@"dic is ===%@", recipeDic);
                
            }else {
                NSLog(@"error is ===%@", error);
            }
        });
    }];
}

- (void)getIngradiantsDetailsFromServer:(NSString *)name {
    ingradiantsDetailsArr = [[NSArray alloc] init];
    [[TelegramServiceClass shareInstance] getIngradiantsDetailsFromServer:name :^(BOOL success, NSError *error, NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                NSDictionary *dataDic = [dic objectForKey:@"ingredient"];
                NSArray *dataArr = [dataDic objectForKey:@"did_you_know"];
                NSString *dataStr = [NSString stringWithFormat:@"%@", dataArr];
                if (![dataStr isEqualToString:@"<null>"] && ![dataStr isEqualToString:@"(null)"]) {
                    if (dataArr.count > 0) {
                        ingradiantsDetailsArr = [[NSArray alloc] init];
                        ingradiantsDetailsArr = [dataDic objectForKey:@"did_you_know"];
                        [gradiantShowCollectionView reloadData];
                    }else {
                        ingradiantsDetailsArr = [[NSArray alloc] init];
                        [gradiantShowCollectionView reloadData];
                    }
                }else {
                    ingradiantsDetailsArr = [[NSArray alloc] init];
                    [gradiantShowCollectionView reloadData];
                }
                
                NSLog(@"ingradiant dic is =====<>>>>>%@", dic);
            }else {
                NSLog(@"error is ===>>>%@", error);
            }
        });
    }] ;
}

- (void)setEstimatedSizeIfNeeded {
    recipeLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    bodyLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    nutritionLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
}
//i * 20 + i * 5
- (void)setUpPageControlView {
    CGFloat totalWidth = gradiantsScrollingArr.count * 15 + gradiantsScrollingArr.count * 5;
    CGFloat x = self.view.frame.size.width/2 - totalWidth/2;
    for (NSUInteger i = 0; i < gradiantsScrollingArr.count; i++) {
        UIView *pageControl = [[UIView alloc] init];
        pageControl.frame = CGRectMake(i * 15 + i * 5 + x, 5, 15, 15);
        if (i == 0) {
            pageControl.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:150.0/255.0 blue:28.0/255.0 alpha:1];
        }else {
            pageControl.backgroundColor = [UIColor lightGrayColor];
        }
        pageControl.layer.borderColor = [UIColor blackColor].CGColor;
        pageControl.layer.borderWidth = 1;
        pageControl.layer.cornerRadius = pageControl.frame.size.width/2;
        pageControl.tag = i;
        [pageControlView addSubview:pageControl];
    }
}

- (void)setCollectionViewHeight {
    [self.view layoutIfNeeded];
    CGFloat recipeHeight = recipeCollectionView.collectionViewLayout.collectionViewContentSize.height;
    CGFloat bodySystemHeight = bodySystemCollectionView.collectionViewLayout.collectionViewContentSize.height;
    CGFloat nutritionProgramHeight = nutritionProgramCollectionView.collectionViewLayout.collectionViewContentSize.height;
    
    if (tagArr.count > 0) {
        if (recipeHeight == 30) {
            recipeCollectionViewHeigtConstraints.constant = 30;
        }else {
            recipeCollectionViewHeigtConstraints.constant = recipeHeight;
        }
    }else {
        recipeCollectionViewHeigtConstraints.constant = 0;
        lblRecipeHeightConstraints.constant = 0;
        lblRecipeTopConstraints.constant = 0;
    }
    
    if (bodySystemArr.count > 0) {
        if (bodySystemHeight == 30) {
            bodySystemCollectionViewHeightConstraints.constant = 30;
        }else {
            bodySystemCollectionViewHeightConstraints.constant = bodySystemHeight;
        }
    }else {
        bodySystemCollectionViewHeightConstraints.constant = 0;
        lblBodySystemHeightConstraints.constant = 0;
        lblBodySystemTopConstraints.constant = 0;
    }
    
    if (healthConditionArr.count > 0) {
        if (nutritionProgramHeight == 30) {
            nutritionProgramCollectionViewHeightConstraints.constant = 30;
        }else {
            nutritionProgramCollectionViewHeightConstraints.constant = nutritionProgramHeight;
            
        }
    }else {
        nutritionProgramCollectionViewHeightConstraints.constant = 0;
        lblNutritionProgramHeightConstraints.constant = 0;
        lblNutritionProgramTopConstriants.constant = 0;
    }
    
    
    [nutritionFactsImageView sd_setImageWithURL:[NSURL URLWithString:[[TelegramServiceClass shareInstance] NutritionFactImageDownloadWithUrl:_recipeId]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSString *imageStr = [NSString stringWithFormat:@"%@", [recipeDic objectForKey:@"img"]];
    imageStr = [imageStr stringByReplacingOccurrencesOfString:@"image" withString:@"image_iv"];
    [nutritionTopImageView sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
}

- (void)isRecipeFavorate {
    if ([[TGFetchSavedPrefrenceInfoGenric getFavorateRecipeIdArr] containsObject:_recipeId]) {
        isFav = true;
        imgFavorate.image = [imgFavorate.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgFavorate setTintColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
        [lblFavorate setTextColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
    }
    
//    if ([[TGFetchSavedPrefrenceInfoGenric getRecipeIdShareChannnel] containsObject:_recipeId]) {
//        isShareChannel = true;
//        imgChannel.image = [imgShare.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        [imgChannel setTintColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
//        [lblChannel setTextColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
//    }
    
}

- (void)initialiseTheNutritionMeter {
    
    NSDictionary *dic = [recipeDic objectForKey:@"meter"];
    //Energy Intensity Nutrition Meter
    if ([[[dic objectForKey:@"energy"] lowercaseString] isEqualToString:@"high"]) {
        [btnEnergy setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnEnergy setTitle:[[dic objectForKey:@"energy"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"energy"] lowercaseString] isEqualToString:@"low"]) {
        [btnEnergy setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnEnergy setTitle:[[dic objectForKey:@"energy"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"energy"] lowercaseString] isEqualToString:@"med"]) {
        [btnEnergy setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnEnergy setTitle:[[dic objectForKey:@"energy"] uppercaseString] forState:UIControlStateNormal];
    }
    
    //Glymic Index Intensity Nutrition Meter
    if ([[[dic objectForKey:@"gl_index"] lowercaseString] isEqualToString:@"high"]) {
        [btnGlymicIndex setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnGlymicIndex setTitle:[[dic objectForKey:@"gl_index"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"gl_index"] lowercaseString] isEqualToString:@"low"]) {
        [btnGlymicIndex setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnGlymicIndex setTitle:[[dic objectForKey:@"gl_index"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"gl_index"] lowercaseString] isEqualToString:@"med"]) {
        [btnGlymicIndex setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnGlymicIndex setTitle:[[dic objectForKey:@"gl_index"] uppercaseString] forState:UIControlStateNormal];
    }
    
    // Micro Nutritients Nutrition Meter
    if ([[[dic objectForKey:@"micr"] lowercaseString] isEqualToString:@"high"]) {
        [btnMicroNutrients setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnMicroNutrients setTitle:[[dic objectForKey:@"micr"] uppercaseString] forState:UIControlStateNormal];
        
    }
    if ([[[dic objectForKey:@"micr"] lowercaseString] isEqualToString:@"low"]) {
        [btnMicroNutrients setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnMicroNutrients setTitle:[[dic objectForKey:@"micr"] uppercaseString] forState:UIControlStateNormal];
        
    }
    if ([[[dic objectForKey:@"micr"] lowercaseString] isEqualToString:@"med"]) {
        [btnMicroNutrients setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnMicroNutrients setTitle:[[dic objectForKey:@"micr"] uppercaseString] forState:UIControlStateNormal];
    }
    
    // Fats Nutrition Meter
    if ([[[dic objectForKey:@"fats"] lowercaseString] isEqualToString:@"high"]) {
        [btnFats setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnFats setTitle:[[dic objectForKey:@"fats"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"fats"] lowercaseString] isEqualToString:@"low"]) {
        [btnFats setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnFats setTitle:[[dic objectForKey:@"fats"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"fats"] lowercaseString] isEqualToString:@"med"]) {
        [btnFats setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnFats setTitle:[[dic objectForKey:@"fats"] uppercaseString] forState:UIControlStateNormal];
    }
    
    // Protien Nutritio Meter
    if ([[[dic objectForKey:@"protein"] lowercaseString] isEqualToString:@"high"]) {
        [btnProtein setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnProtein setTitle:[[dic objectForKey:@"protein"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"protein"] lowercaseString] isEqualToString:@"low"]) {
        [btnProtein setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnProtein setTitle:[[dic objectForKey:@"protein"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"protein"] lowercaseString] isEqualToString:@"med"]) {
        [btnProtein setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnProtein setTitle:[[dic objectForKey:@"protein"] uppercaseString] forState:UIControlStateNormal];
    }
    
    // Salt Nutritio Meter
    if ([[[dic objectForKey:@"salt"] lowercaseString] isEqualToString:@"high"]) {
        [btnSalt setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:0.0/255.0 blue:5.0/255.0 alpha:1]];
        [btnSalt setTitle:[[dic objectForKey:@"salt"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"salt"] lowercaseString] isEqualToString:@"low"]) {
        [btnSalt setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:63.0/255.0 blue:16.0/255.0 alpha:1]];
        [btnSalt setTitle:[[dic objectForKey:@"salt"] uppercaseString] forState:UIControlStateNormal];
    }
    if ([[[dic objectForKey:@"salt"] lowercaseString] isEqualToString:@"med"])  {
        [btnSalt setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:136.0/255.0 blue:43.0/255.0 alpha:1]];
        [btnSalt setTitle:[[dic objectForKey:@"salt"] uppercaseString] forState:UIControlStateNormal];
    }
    [self setNutritionMeterButtonCornerRadius];
}

- (void)setNutritionMeterButtonCornerRadius {
    btnEnergy.layer.cornerRadius = btnEnergy.frame.size.width/2;
    btnEnergy.layer.masksToBounds = true;
    
    btnGlymicIndex.layer.cornerRadius = btnGlymicIndex.frame.size.width/2;
    btnGlymicIndex.layer.masksToBounds = true;
    
    btnMicroNutrients.layer.cornerRadius = btnMicroNutrients.frame.size.width/2;
    btnMicroNutrients.layer.masksToBounds = true;
    
    btnFats.layer.cornerRadius = btnFats.frame.size.width/2;
    btnFats.layer.masksToBounds = true;
    
    btnProtein.layer.cornerRadius = btnProtein.frame.size.width/2;
    btnProtein.layer.masksToBounds = true;
    
    btnSalt.layer.cornerRadius = btnSalt.frame.size.width/2;
    btnSalt.layer.masksToBounds = true;
}

#pragma mark - IBAction

- (IBAction)btnClose:(id)sender {
    [TGMixPanelEventsConstants trackMixpanelEvents:FULL_RECIPE properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:FULL_RECIPE]];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnFavorate:(id)sender {
    [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:FULL_RECIPE back:FAVORITE] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:FULL_RECIPE]];
    int64_t conversationId = [[TGFetchSavedPrefrenceInfoGenric getFavorateSmoothiesChannelId] longLongValue];
    int64_t converID = conversationId - ((int64_t)INT32_MIN) * 2;
    NSString *encrypthId = [NSString stringWithFormat:@"-%lld",converID];
    [self shareUrlToChannelwithConversationId:[encrypthId longLongValue]];
    NSMutableArray *saveArr = [[NSMutableArray alloc] init];;
    if (isFav) {
        isFav = false;
        imgFavorate.image = [imgFavorate.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgFavorate setTintColor:[UIColor whiteColor]];
        [lblFavorate setTextColor:[UIColor whiteColor]];
        NSArray *favrateArr = [TGFetchSavedPrefrenceInfoGenric getFavorateRecipeIdArr];
        for (NSUInteger i = 0; i < favrateArr.count; i++) {
            NSString *favRecipeId = [NSString stringWithFormat:@"%@", [favrateArr objectAtIndex:i]];
            if (![favRecipeId isEqualToString:_recipeId]) {
                [saveArr addObject:favRecipeId];
            }
        }
    }else {
        isFav = true;
        imgFavorate.image = [imgFavorate.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgFavorate setTintColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
        [lblFavorate setTextColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
        NSArray *favrateArr = [TGFetchSavedPrefrenceInfoGenric getFavorateRecipeIdArr];
        if (favrateArr.count > 0) {
            for (NSUInteger i = 0; i < favrateArr.count; i++) {
                NSString *favRecipeId = [NSString stringWithFormat:@"%@", [favrateArr objectAtIndex:i]];
                if (![favRecipeId isEqualToString:_recipeId]) {
                    [saveArr addObject:favRecipeId];
                }
            }
            [saveArr addObject:_recipeId];
        }else {
            [saveArr addObject:_recipeId];
        }
    }
    [TGSavedPrefrence saveFavorateRecipesIdFromArray:saveArr];
    NSString *seprateStr = [saveArr componentsJoinedByString:@","];
    [[TelegramServiceClass shareInstance] saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:seprateStr type:@"favorite_smoothie" like:@"" dislike:@"" phnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] with:^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"data dic is ====>>>%@",dic);
        }else {
            NSLog(@"error is ====>>>%@",error);
        }
    }];
   // [self saveAllPrefrencesListFromSmoothiesServer];
}

- (void)saveAllPrefrencesListFromSmoothiesServer {
    TGUser *selfUser = [TGDatabaseInstance() loadUser:(int)[TGFetchSavedPrefrenceInfoGenric getTelegramUid]];
    NSString *chatUserId = [NSString stringWithFormat:@"%d", selfUser.uid];
    [[TelegramServiceClass shareInstance] saveUserAllPrefrencestoTheSmoothiesServerchatUserId:chatUserId :^(BOOL success, NSError *error, NSDictionary *dic) {
        if (success) {
            NSLog(@"prefrences dic is ====>>>>>%@", dic);
        }else {
            NSLog(@"error is ====>>>>%@",error);
        }
    }];
}

- (void)shareUrlToChannelwithConversationId:(int64_t)conversationID {
    NSString *text = _urlStr;
    TGConversation *conversation = [TGDatabaseInstance() loadConversationWithIdCached:conversationID];
    TGChannelConversationCompanion *companion = [[TGChannelConversationCompanion alloc] initWithConversation:conversation userActivities:nil];
    [companion controllerWantsToSendTextMessage:text entities:@[] asReplyToMessageId:0 withAttachedMessages:@[] completeGroups:nil disableLinkPreviews:nil botContextResult:nil botReplyMarkup:nil withConveresationId:conversationID];
}

- (IBAction)btnShare:(id)sender {
    [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:FULL_RECIPE back:SHARE] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:CLICK value:@"" element:BUTTON screenName:FULL_RECIPE]];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",Share_Url,_recipeId];
    NSURL *url = [[NSURL alloc] initWithString:urlStr];
    NSArray *objectsToShare = @[url];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)NutritionFactsImageTap:(id)sender {
    [self openZoomImageView:[[TelegramServiceClass shareInstance] NutritionFactImageDownloadWithUrl:_recipeId]];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return zoomImageView.nutritionFactsImageView;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}
#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // zoom in
    float newScale = [zoomImageView.scrollerView zoomScale] * 1.5;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [zoomImageView.scrollerView zoomToRect:zoomRect animated:YES];
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [zoomImageView.scrollerView zoomScale] / 1.5;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [zoomImageView.scrollerView zoomToRect:zoomRect animated:YES];
}

#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [zoomImageView.scrollerView frame].size.height / scale;
    zoomRect.size.width  = [zoomImageView.scrollerView frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}


//- (IBAction)btnChannel:(id)sender {
//    [TGMixPanelEventsConstants trackMixpanelEvents:[TGMixPanelEventsConstants screenNameWith:FULL_RECIPE back:CHANNEL] properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:@"" value:@"" element:BUTTON screenName:FULL_RECIPE]];
//    [self shareUrlToChannelwithConversationId:[[TGFetchSavedPrefrenceInfoGenric getUserNameCreatedChannelId] longLongValue]];
//    NSMutableArray *saveArr = [[NSMutableArray alloc] init];;
//    if (isShareChannel) {
//        isShareChannel = false;
//        imgChannel.image = [imgChannel.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        [imgChannel setTintColor:[UIColor whiteColor]];
//        [lblChannel setTextColor:[UIColor whiteColor]];
//        NSArray *idArr = [TGFetchSavedPrefrenceInfoGenric getRecipeIdShareChannnel];
//        for (NSUInteger i = 0; i < idArr.count; i++) {
//            NSString *channelRecipeId = [NSString stringWithFormat:@"%@", [idArr objectAtIndex:i]];
//            if (![channelRecipeId isEqualToString:_recipeId]) {
//                [saveArr addObject:channelRecipeId];
//            }
//        }
//    }else {
//        isShareChannel = true;
//        imgChannel.image = [imgChannel.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        [imgChannel setTintColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
//        [lblChannel setTextColor:[UIColor colorWithRed:232.0/255.0 green:168.0/255.0 blue:70.0/255.0 alpha:1]];
//        NSArray *favrateArr = [TGFetchSavedPrefrenceInfoGenric getRecipeIdShareChannnel];
//        if (favrateArr.count > 0) {
//            for (NSUInteger i = 0; i < favrateArr.count; i++) {
//                NSString *favRecipeId = [NSString stringWithFormat:@"%@", [favrateArr objectAtIndex:i]];
//                if (![favRecipeId isEqualToString:_recipeId]) {
//                    [saveArr addObject:favRecipeId];
//                }
//            }
//            [saveArr addObject:_recipeId];
//        }else {
//            [saveArr addObject:_recipeId];
//        }
//    }
////    [TGSavedPrefrence saveRecipeIdShareToChannelFromArray:saveArr];
////    NSString *seprateStr = [saveArr componentsJoinedByString:@","];
////    [[TelegramServiceClass shareInstance] saveGoalsPrefrencesIngredientsHealthConditionsInformationFromServer:seprateStr type:@"favorite_smoothie" like:@"" dislike:@"" phnNumber:[TGFetchSavedPrefrenceInfoGenric getMobileNumber] with:^(BOOL success, NSError *error, NSDictionary *dic) {
////        if (success) {
////            NSLog(@"data dic is ====>>>%@",dic);
////        }else {
////            NSLog(@"error is ====>>>%@",error);
////        }
////    }];
////    [self saveAllPrefrencesListFromSmoothiesServer];
//}

- (IBAction)btnMeter:(id)sender {
    
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (bool)collectionView:(UICollectionView *)__unused collectionView canMoveItemAtIndexPath:(NSIndexPath *)__unused indexPath
{
    return false;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == gradiantsCollectionView) {
        return 0;
    }
    if (collectionView == gradiantShowCollectionView) {
        return 0;
    }
    if (collectionView == recipeCollectionView) {
        return 8;
    }
    if (collectionView == bodySystemCollectionView) {
        return 8;
    }
    if (collectionView == nutritionProgramCollectionView) {
        return 8;
    }
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == gradiantsCollectionView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    if (collectionView == gradiantShowCollectionView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    if (collectionView == recipeCollectionView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    if (collectionView == bodySystemCollectionView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    if (collectionView == nutritionProgramCollectionView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == gradiantsCollectionView) {
        CGFloat width = self.view.frame.size.width/gradiantsArr.count;
        return CGSizeMake(width, 100);
    }
    if (collectionView == gradiantShowCollectionView) {
        ingradiantsCollectionViewHeightConstriants.constant = 232;
        return CGSizeMake(self.view.frame.size.width, 232);
    }
    if (collectionView == recipeCollectionView) {
        CGFloat randomWidth = (arc4random() % 120) + 30;
        return CGSizeMake(randomWidth+16, 30);
    }
    if (collectionView == bodySystemCollectionView) {
        CGFloat randomWidth = (arc4random() % 120) + 30;
        return CGSizeMake(randomWidth+16, 30);
    }
    if (collectionView == nutritionProgramCollectionView) {
        CGFloat randomWidth = (arc4random() % 120) + 30;
        return CGSizeMake(randomWidth+16, 30);
    }
    return CGSizeZero;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == gradiantsCollectionView) {
        return gradiantsArr.count;
    }else if (collectionView == gradiantShowCollectionView) {
        return gradiantsScrollingArr.count;
    }else if (collectionView == recipeCollectionView) {
        return tagArr.count;
    }else if (collectionView == bodySystemCollectionView) {
        return bodySystemArr.count;
    }else {
        return healthConditionArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == gradiantsCollectionView) {
        TGIngrediantsCollectionViewCell *cell = (TGIngrediantsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGIngrediantsCollectionViewCell" forIndexPath:indexPath];
        TGRecipeModel *model = [gradiantsArr objectAtIndex:indexPath.row];
        [cell.ingrediantsImageView sd_setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        if (model.is_Selected) {
            cell.selectedView.backgroundColor = [UIColor colorWithRed:63.0/255.0 green:102.0/255.0 blue:108.0/255.0 alpha:1];
        }else {
            cell.selectedView.backgroundColor = [UIColor whiteColor];
        }
        return cell;
    }else if (collectionView == gradiantShowCollectionView) {
        if (indexPath.item == 0) {
            TGIngradiantsImageCollectionViewCell *cell = (TGIngradiantsImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGIngradiantsImageCollectionViewCell" forIndexPath:indexPath];
            TGRecipeModel *model = [gradiantsScrollingArr objectAtIndex:indexPath.row];
            [cell.ingradiantsImageView sd_setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            ingradiantsCollectionViewHeightConstriants.constant = cell.frame.size.height;
            return cell;
        }else {
            TGIngradiantsShowCollectionViewCell *cell = (TGIngradiantsShowCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGIngradiantsShowCollectionViewCell" forIndexPath:indexPath];
            TGRecipeModel *model = [gradiantsScrollingArr objectAtIndex:indexPath.row];
            if ([model.name isEqualToString:@""] && [model.title isEqualToString:@""]) {
                cell.ingradiantImageView.image = [UIImage imageNamed:model.url];
                cell.lblSecond.text = TGLocalized(@"View_Recipe_Blend_It_For");
                cell.lblDiscription.text = TGLocalized(@"View_Recipe_Use_Powerfull_Blender");
                cell.lblSecond.backgroundColor = [UIColor whiteColor];
                cell.lblSecond.textColor = [UIColor blackColor];
                cell.lblFirst.text = @"";
                cell.lblThird.text = @"";
            }else {
                [cell.ingradiantImageView sd_setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                if (model.titleArr.count == 1) {
                    cell.lblFirst.text = @"";
                    if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]] isEqualToString:[model.name lowercaseString]]) {
                        cell.lblSecond.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:0]];
                        cell.lblSecond.backgroundColor = [UIColor orangeColor];
                        cell.lblSecond.layer.cornerRadius = 14;
                        cell.lblSecond.layer.masksToBounds = true;
                        cell.lblSecond.textColor = [UIColor whiteColor];
                        
                    }
                    cell.lblThird.text = @"";
                }else if (model.titleArr.count == 2) {
                    if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]] isEqualToString:[model.name lowercaseString]]) {
                        cell.lblFirst.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:0]];
                        cell.lblFirst.backgroundColor = [UIColor orangeColor];
                        cell.lblFirst.layer.cornerRadius = 14;
                        cell.lblFirst.layer.masksToBounds = true;
                        cell.lblFirst.textColor = [UIColor whiteColor];

                        cell.lblSecond.text = @"";
                        cell.lblThird.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:1]];
                        cell.lblThird.textColor = [UIColor blackColor];
                        cell.lblThird.backgroundColor = [UIColor whiteColor];
                    }
                    if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:1]] isEqualToString:[model.name lowercaseString]]) {
                        cell.lblFirst.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]];
                        cell.lblFirst.backgroundColor = [UIColor whiteColor];
                        cell.lblFirst.textColor = [UIColor blackColor];
                        cell.lblSecond.text = @"";
                        cell.lblThird.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:1]];
                        cell.lblThird.backgroundColor = [UIColor orangeColor];
                        cell.lblThird.layer.cornerRadius = 14;
                        cell.lblThird.layer.masksToBounds = true;
                        cell.lblThird.textColor = [UIColor whiteColor];
                    }
                }else {
                    
                    cell.lblFirst.text = @"";
                    cell.lblSecond.text = @"";
                    cell.lblThird.text = @"";
                    if (model.titleArr.count > 0) {
                        if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]] isEqualToString:[model.name lowercaseString]]) {
                            cell.lblFirst.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:0]];
                            cell.lblFirst.backgroundColor = [UIColor orangeColor];
                            cell.lblFirst.layer.cornerRadius = 14;
                            cell.lblFirst.layer.masksToBounds = true;
                            cell.lblFirst.textColor = [UIColor whiteColor];
                            
                            cell.lblSecond.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:1]];
                            cell.lblSecond.backgroundColor = [UIColor whiteColor];
                            cell.lblSecond.textColor = [UIColor blackColor];
                            
                            cell.lblThird.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:2]];
                            cell.lblThird.backgroundColor = [UIColor whiteColor];
                            cell.lblThird.textColor = [UIColor blackColor];
                            
                        }
                        if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:1]] isEqualToString:[model.name lowercaseString]]) {
                            
                            cell.lblFirst.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]];
                            cell.lblFirst.backgroundColor = [UIColor whiteColor];
                            cell.lblFirst.textColor = [UIColor blackColor];
                            
                            cell.lblSecond.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:1]];
                            cell.lblSecond.backgroundColor = [UIColor orangeColor];
                            cell.lblSecond.layer.cornerRadius = 14;
                            cell.lblSecond.layer.masksToBounds = true;
                            cell.lblSecond.textColor = [UIColor whiteColor];

                            cell.lblThird.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:2]];
                            cell.lblThird.backgroundColor = [UIColor whiteColor];
                            cell.lblThird.textColor = [UIColor blackColor];
                            
                        }
                        if ([[NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:2]] isEqualToString:[model.name lowercaseString]]) {
                            
                            cell.lblFirst.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:0]];
                            cell.lblFirst.backgroundColor = [UIColor whiteColor];
                            cell.lblFirst.textColor = [UIColor blackColor];
                            
                            cell.lblSecond.text = [NSString stringWithFormat:@"%@",[model.titleArr objectAtIndex:1]];
                            cell.lblSecond.backgroundColor = [UIColor whiteColor];
                            cell.lblSecond.textColor = [UIColor blackColor];
                            
                            cell.lblThird.text = [NSString stringWithFormat:@" %@ ",[model.titleArr objectAtIndex:3]];
                            cell.lblThird.backgroundColor = [UIColor orangeColor];
                            cell.lblThird.layer.cornerRadius = 14;
                            cell.lblThird.layer.masksToBounds = true;
                            cell.lblThird.textColor = [UIColor whiteColor];
                        }
                    }
                }
                if (ingradiantsDetailsArr.count > 0) {
                    cell.lblDiscription.text = [NSString stringWithFormat:@"%@",[ingradiantsDetailsArr objectAtIndex:0]];
                }else {
                    cell.lblDiscription.text = @"";
                }
            }

            cell.cellIndex = indexPath.row;
            cell.selectDelegate = self;
            ingradiantsCollectionViewHeightConstriants.constant = cell.frame.size.height;
            return cell;
        }
    }else {
        TGRecipiesCollectionViewCell *cell = (TGRecipiesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TGRecipiesCollectionViewCell" forIndexPath:indexPath];
        if (collectionView == recipeCollectionView) {
            cell.lblText.text = [NSString stringWithFormat:@"%@",[tagArr objectAtIndex:indexPath.row]];
            cell.bgView.backgroundColor = [self getTextBackgroundColor:cell.lblText.text];
            cell.bgView.layer.cornerRadius = 14.0;
            cell.bgView.layer.masksToBounds = true;
            return cell;
        }else if (collectionView == bodySystemCollectionView) {
            cell.lblText.text = [NSString stringWithFormat:@"%@",[bodySystemArr objectAtIndex:indexPath.row]];
            cell.bgView.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:97.0/255.0 blue:156.0/255.0 alpha:1];
            cell.bgView.layer.cornerRadius = 14.0;
            cell.bgView.layer.masksToBounds = true;
            return cell;
        }else {
            cell.lblText.text = [NSString stringWithFormat:@"%@",[healthConditionArr objectAtIndex:indexPath.row]];
            cell.bgView.backgroundColor = [UIColor colorWithRed:19.0/255.0 green:54.0/255.0 blue:86.0/255.0 alpha:1];
            cell.bgView.layer.cornerRadius = 14.0;
            cell.bgView.layer.masksToBounds = true;
            return cell;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == gradiantsCollectionView) {
        for (NSUInteger i = 0; i<gradiantsArr.count; i++) {
            TGRecipeModel *model = [gradiantsArr objectAtIndex:i];
            if (i == (NSUInteger)indexPath.row) {
                model.is_Selected = true;
                [self getIngradiantsDetailsFromServer:model.name];
            }else {
                model.is_Selected = false;
            }
        }
        [gradiantsCollectionView reloadData];
        NSUInteger index = 0;
        for (NSUInteger i = 0; i < gradiantsScrollingArr.count; i++) {
            TGRecipeModel *model = [gradiantsArr objectAtIndex:indexPath.row];
            TGRecipeModel *model1 = [gradiantsScrollingArr objectAtIndex:i];
            if ([model.name isEqualToString:model1.name]) {
                //  pageControl.currentPage = i;
                index = i;
                [gradiantShowCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]
                                                   atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                                           animated:NO];
            }
        }
        for (NSUInteger i = 0; i < [pageControlView subviews].count; i++) {
            if ((NSUInteger)pageControlView.subviews[i].tag == index) {
                pageControlView.subviews[i].backgroundColor = [UIColor colorWithRed:241.0/255.0 green:150.0/255.0 blue:28.0/255.0 alpha:1];
            }else {
                pageControlView.subviews[i].backgroundColor = [UIColor colorWithRed:207.0/255.0 green:206.0/255.0 blue:207.0/255.0 alpha:1];
            }
        }
    }
}

- (UIColor *)getTextBackgroundColor:(NSString *)text {
    UIColor *color = [[UIColor alloc] init];
    if ([text isEqualToString:@"Weight Loss"] || [text isEqualToString:@"Immune Booster"]) {
        color = [UIColor colorWithRed:23.0/255.0 green:178.0/255.0 blue:139.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Muscle Building" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Anti-Inflammatory" lowercaseString]]) {
        color = [UIColor colorWithRed:26.0/255.0 green:201.0/255.0 blue:96.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Shaping" lowercaseString]]) {
        color = [UIColor colorWithRed:29.0/255.0 green:129.0/255.0 blue:210.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Health improvement" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Purple Shake" lowercaseString]]  || [[text lowercaseString] isEqualToString:[@"Antioxidant Rich" lowercaseString]]) {
        color = [UIColor colorWithRed:138.0/255.0 green:58.0/255.0 blue:166.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Good For Skin" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Better Vision" lowercaseString]]) {
        color = [UIColor colorWithRed:38.0/255.0 green:56.0/255.0 blue:75.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Better Sleep" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Breakfast" lowercaseString]]) {
        color = [UIColor colorWithRed:240.0/255.0 green:188.0/255.0 blue:38.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Post Workout" lowercaseString]]) {
        color = [UIColor colorWithRed:21.0/255.0 green:164.0/255.0 blue:79.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Protein Shake" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Omega3 Rich" lowercaseString]]) {
        color = [UIColor colorWithRed:228.0/255.0 green:106.0/255.0 blue:28.0/255.0 alpha:1];
    }else if ([[text lowercaseString] isEqualToString:[@"Green Shake" lowercaseString]] || [[text lowercaseString] isEqualToString:[@"Energy Booster" lowercaseString]]) {
        color = [UIColor colorWithRed:19.0/255.0 green:147.0/255.0 blue:115.0/255.0 alpha:1];
    }else {
        color = [UIColor colorWithRed:218.0/255.0 green:49.0/255.0 blue:43.0/255.0 alpha:1];
    }
    return color;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.tag == 1) {
        CGFloat pageWidth = gradiantShowCollectionView.frame.size.width;
        CGFloat currentPage = gradiantShowCollectionView.contentOffset.x / pageWidth;
        // pageControl.currentPage = (int)currentPage;
        ingradiantsDetailsArr = [[NSArray alloc] init];
        TGRecipeModel *model = [gradiantsScrollingArr objectAtIndex:(NSUInteger)currentPage];//pageControl.currentPage];
        for (NSUInteger i = 0; i < gradiantsArr.count; i++) {
            TGRecipeModel *model1 = [gradiantsArr objectAtIndex:i];
            if ([model.name isEqualToString:model1.name]) {
                model1.is_Selected = true;
                [self getIngradiantsDetailsFromServer:model.name];
            }else {
                model1.is_Selected = false;
            }
        }
        [gradiantsCollectionView reloadData];
        
        for (NSUInteger i = 0; i <[pageControlView subviews].count; i++) {
            if (pageControlView.subviews[i].tag == currentPage) {
                pageControlView.subviews[i].backgroundColor = [UIColor colorWithRed:241.0/255.0 green:150.0/255.0 blue:28.0/255.0 alpha:1];
            }else {
                pageControlView.subviews[i].backgroundColor = [UIColor lightGrayColor];
            }
        }
    }
}

- (void)selectionClickedAtIndex:(NSUInteger)index {
    for (NSUInteger i = 0; i < gradiantsScrollingArr.count; i++) {
        if (index == i) {
            TGRecipeModel *model1 = [gradiantsScrollingArr objectAtIndex:i];
            if (![model1.name isEqualToString:@""] && ![model1.title isEqualToString:@""]) {
                //[self openZoomImageView:model1.url];
                TGIngredientsDetailsViewController *controller = [[TGIngredientsDetailsViewController alloc] initWithNibName:@"TGIngredientsDetailsViewController" bundle:nil];
                controller.ingredientsName = model1.name;
                [self.navigationController pushViewController:controller animated:true];
            }
        }
    }
}

- (void)openZoomImageView:(NSString *)url {
    int imageConstant = 0;
    int max = (int)[[UIScreen mainScreen] bounds].size.height;
    if (max == 812) {
        imageConstant = 50;
    }else {
        imageConstant = 35;
    }
    zoomImageView = [[[NSBundle mainBundle] loadNibNamed:@"TGRecipeNutritionFactsZoomImageView" owner:self options:nil] objectAtIndex:0];
    zoomImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    zoomImageView.nutritionFactsImageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth );
    [zoomImageView.nutritionFactsImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    zoomImageView.buttonTopConstraints.constant = imageConstant;
    zoomImageView.scrollerView.contentSize = CGSizeMake(zoomImageView.nutritionFactsImageView.frame.size.width, zoomImageView.nutritionFactsImageView.frame.size.height);
    zoomImageView.scrollerView.clipsToBounds = YES;
    zoomImageView.scrollerView.delegate = self;
    zoomImageView.doubleTap.delegate = self;
    zoomImageView.nutritionFactsImageView.userInteractionEnabled = YES;
    
    zoomImageView.scrollerView.minimumZoomScale = 1.0;
    zoomImageView.scrollerView.maximumZoomScale = 5.0;
    
    // add gesture recognizers to the image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:singleTap];
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:doubleTap];
    [zoomImageView.nutritionFactsImageView addGestureRecognizer:twoFingerTap];
    [self.view addSubview:zoomImageView];
}

@end

