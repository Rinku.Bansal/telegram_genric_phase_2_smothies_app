//
//  TGFAQDetailsViewController.m
//  Telegraph
//
//  Created by vinove on 11/2/18.
//

#import "TGFAQDetailsViewController.h"
#import "TGColor.h"
#import "TGMixPanelEventsConstants.h"
#import "TGGenricAppInformation.h"

@interface TGFAQDetailsViewController ()<UITextViewDelegate>{
    
    __weak IBOutlet UITextView *lblAnswerTextView;
    __weak IBOutlet UILabel *lblQuestion;
    UIView *containerView;
}

@end

@implementation TGFAQDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *controllerView = [[[NSBundle mainBundle] loadNibNamed:@"TGFAQDetailsViewController" owner:self options:nil] firstObject];
    containerView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    containerView.backgroundColor = TGThemeColor();
    [containerView addSubview:controllerView];
    self.titleText = [[TGGenricAppInformation shareInstance] getTextDetailsFromInfoPlist:@"faq" withInfoParam:@"title" withDefaultStr:TGLocalized(@"FAQ_Header_Title")];//@"FAQ";
    [TGMixPanelEventsConstants trackMixpanelEvents:FAQ_DETAILS properties:[TGMixPanelEventsConstants mixpanelProperties:@"" withAction:ENTRY value:@"" element:SCREEN screenName:FAQ_DETAILS]];

    UIBarButtonItem *btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NavigationBackArrowLightChange.png"] style:UIBarButtonItemStylePlain target:self action:@selector(ButtonBackClicked)];
    self.navigationItem.leftBarButtonItem=btn;
    [self initialiseTheView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = TGThemeColor();
}

- (void)ButtonBackClicked {
    [TGMixPanelEventsConstants trackMixpanelEvents:FAQ_DETAILS properties:[TGMixPanelEventsConstants mixpanelProperties:BACK_BUTTON withAction:@"" value:@"" element:BACK_BUTTON screenName:FAQ_DETAILS]];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)initialiseTheView {
    lblQuestion.text = _question;
    lblAnswerTextView.text = _answer;
}

@end
