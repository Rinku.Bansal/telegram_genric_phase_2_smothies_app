//
//  TGIngrediantsCollectionViewCell.h
//  Telegraph
//
//  Created by vinove on 12/27/18.
//

#import <UIKit/UIKit.h>

@interface TGIngrediantsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ingrediantsImageView;
@property (weak, nonatomic) IBOutlet UIView *selectedView;

@end
