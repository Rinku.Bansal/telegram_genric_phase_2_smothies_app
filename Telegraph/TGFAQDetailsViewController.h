//
//  TGFAQDetailsViewController.h
//  Telegraph
//
//  Created by vinove on 11/2/18.
//

#import <UIKit/UIKit.h>
#import <LegacyComponents/TGViewController.h>

@interface TGFAQDetailsViewController : TGViewController

@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSString *answer;
@end
