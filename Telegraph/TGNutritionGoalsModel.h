//
//  TGNutritionGoalsModel.h
//  Telegraph
//
//  Created by vinove on 12/18/18.
//

#import <Foundation/Foundation.h>

@interface TGNutritionGoalsModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) BOOL is_Selected;

+ (NSMutableArray *)getNutritionGoalsDetails:(NSArray *)dataArr;

@end
