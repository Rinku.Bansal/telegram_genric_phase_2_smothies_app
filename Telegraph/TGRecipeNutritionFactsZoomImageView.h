//
//  TGRecipeNutritionFactsZoomImageView.h
//  Telegraph
//
//  Created by Rinku on 06/01/19.
//

#import <UIKit/UIKit.h>

@interface TGRecipeNutritionFactsZoomImageView : UIView<UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *nutritionFactsImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraints;

//@property (weak, nonatomic) IBOutlet UIGestureRecognizer *doubleTap;
//@property (weak, nonatomic) IBOutlet UIGestureRecognizer *twoFingerTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *doubleTap;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *twoFingerTap;

@end
