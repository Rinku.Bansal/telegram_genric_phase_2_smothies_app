//
//  TGGenricAppInformation.h
//  Telegraph
//
//  Created by Rinku on 19/11/18.
//

#import <Foundation/Foundation.h>

@interface TGGenricAppInformation : NSObject

+ (TGGenricAppInformation *)shareInstance;

- (NSString *)getTextDetailsFromInfoPlist:(NSString *)dicParam withInfoParam:(NSString *)param withDefaultStr:(NSString *)tempStr;

- (NSString *)getFontDetailsFromPlist:(NSString *)dicParam;

//+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
