//
//  TGIngredientsDiscriptionTableViewCell.h
//  Telegraph
//
//  Created by vinove on 1/10/19.
//

#import <UIKit/UIKit.h>

@interface TGIngredientsDiscriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDiscription;
@end
